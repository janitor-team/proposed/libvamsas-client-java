/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client;

/**
 * Methods implemented by a Vamsas Application's Object Update handler
 * 
 * @author vamsas Introduced November 2006 Vamsas Meeting TODO: verify this is
 *         sufficient for the per-object update event mechanism
 */
public interface IObjectUpdate {
  /**
   * Called by the library to find out which vamsas document object this update
   * handler is interested in
   * 
   * @return class that extends org.vamsas.Vobject
   */
  Class getRootVobject();

  /**
   * Called to test if this handler is to be called for updates to any Vobjects
   * below the Root Vobject in the vamsas document.
   * 
   * @return false means IObjectUpdate.update(updated, cdoc) will only be called
   *         with instances of type getRootVobject().
   */
  boolean handlesSubtreeUpdates();

  /**
   * Method called by Vamsas Client Library for all updated objects that the
   * handler is registered for.
   * 
   * @param updated
   * @param cdoc
   */
  void update(uk.ac.vamsas.client.Vobject updated,
      uk.ac.vamsas.client.IClientDocument cdoc);
}
