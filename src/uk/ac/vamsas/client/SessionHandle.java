/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client;

import java.io.Serializable;

/**
 * Uniquely locates a particular VAMSAS session.
 * 
 * @author jimp
 * 
 */
public class SessionHandle implements Serializable {

  /**
   * @param sessionUrn
   */
  public SessionHandle(String _sessionUrn) {
    super();
    this.sessionUrn = _sessionUrn;
  }

  /**
   * @return Returns the sessionUrn.
   */
  public String getSessionUrn() {
    return this.sessionUrn;
  }

  /**
   * @param sessionUrn
   *          The sessionUrn to set.
   */
  public void setSessionUrn(String _sessionUrn) {
    this.sessionUrn = _sessionUrn;
  }

  /**
   * The path to the vamsas session file.
   */
  String sessionUrn = null;

  /**
   * @see java.lang.Object#equals(java.lang.Object)
   */
  public boolean equals(Object obj) {

    if (obj instanceof SessionHandle)
      return this.equals((SessionHandle) obj);
    return false;
  }

  public boolean equals(SessionHandle that) {
    return (this.sessionUrn.equals(that.getSessionUrn()));
  }

}
