/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client.simpleclient;

import java.io.File;
import java.net.URI;

import uk.ac.vamsas.client.InvalidSessionUrnException;

/**
 * SessionUrn for simpleclient sessions: simpleclient://{Absolute path to
 * session directory}
 * 
 * For simpleclient urn, it consideres as on the same machine, using the path to
 * the session directory to generate the session URN
 * 
 * @author jimp
 * 
 */
public class SessionUrn extends uk.ac.vamsas.client.SessionUrn {
  /**
   * a simple client session urn prefix
   */
  public static final String SIMPLECLIENT = "simpleclient";

  public static String VAMSASDOCUMENT = "vdoc";
  static {
    TYPES.put(SIMPLECLIENT, SessionUrn.class);
    TYPES.put(SessionUrn.VAMSASDOCUMENT, SessionUrn.class);
  }

  /**
   * Creates a SessionUrn object from a String. The string must be a string
   * representation of a URI
   * 
   * @param urnString
   * @throws InvalidSessionUrnException
   */
  public SessionUrn(String urnString) throws InvalidSessionUrnException {
    super();
    this.setURN(urnString);
  }

  /**
   * Generates a sessionURN from a file with a type of SIMPLECLIENT if it is a directory or VAMSASDOCUMENT if it is a file. 
   * 
   * Note: No validation is performed, either for uniqueness of resultant URI or validity of the location.
   * 
   * @param sessionLocation
   *          the file object to create the sessionURN from.
   * 
   */
  public SessionUrn(File sessionLocation) {
    // TODO: refactor this and use a Urn factory - this inline switch is pretty ugly!
    super((sessionLocation.isFile() ? VAMSASDOCUMENT : SIMPLECLIENT)
        ,sessionLocation.getAbsoluteFile().toURI());
  }

  /**
   * Generates a sessionURN bases on a vamsas session
   * 
   * @param session
   *          a VamsasSession
   * 
   *          Should return the same URN string than the creation with the
   *          session directory
   */
  public SessionUrn(VamsasSession session) {
    // throws MalformedURLException {
    // use URI instead of URL
    super(SIMPLECLIENT, session.sessionDir.getAbsoluteFile().toURI());
  }

  /**
   * TODO: LATER: think about this again.
   * 
   * Retrieves the file associated to the current sessionURN. The sessionURN
   * (URI) is based on the session directory absolute path. Use the raw path of
   * the URN and change the scheme to file to generate a new file URI. Then,
   * from the URI create the File object (a File object can be created from an
   * uri)
   * 
   * @return File object representing the session URN // File(urn.getPath())
   */
  public File asFile() {
    String path = this.urn.getRawPath();

    return new File(URI.create("file://" + path));
  }
  // TODO: add abstract 'handler' methods for resolving the URN to a particular
  // class
}
