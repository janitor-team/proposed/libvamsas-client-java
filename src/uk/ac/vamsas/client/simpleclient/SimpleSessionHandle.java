/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client.simpleclient;

import java.io.File;
import java.io.Serializable;
/**
 * SimpleSessionHandle maintains mapping between external vamsas SessionHandles and simpleclient session directory locations.
 * @author JimP
 *
 */
public class SimpleSessionHandle extends uk.ac.vamsas.client.SessionHandle implements Serializable {
  /**
   * SimpleClient SessionHandle v0.1
   */
  private static final long serialVersionUID = 1L;
  /**
   * Construct urn from logical vamsas session urn for this session
   * @param urn
   */
  public SimpleSessionHandle(String urn) {
    super(urn);
  }
  /**
   * where the session is actually stored
   */
  private String physLoc = null;
  /**
   * Vamsas Session URN and physical location of simplesession vamsas session
   * @param urn
   * @param origDoc
   */
  public SimpleSessionHandle(String urn, File origDoc)
  {
   this(urn);
   setPhysLoc(origDoc.getAbsoluteFile().getAbsolutePath());
  }
  public void setPhysLoc(String physLoc) {
    this.physLoc = physLoc;
  }
  public String getPhysLoc() {
    return physLoc;
  }

}
