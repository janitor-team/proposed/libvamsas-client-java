/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client.simpleclient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Timer;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;

/**
 * low level vamsas document management routines analogous to ClientsFile Grew
 * out of io tests on VamsasArchive class in
 * uk.ac.vamsas.test.simpleclient.VamsasArchive This class is not thread safe.
 * 
 * @author jimp
 * 
 */
public class VamsasFile extends SessionFile {
  /**
   * 
   * Connect to an existing Vamsas document in a given sessionDir or create a
   * new one.
   * 
   * @param sessionDir
   * @throws java.io.IOException
   */
  public VamsasFile(File sessionFile) throws java.io.IOException {
    super(sessionFile);
  }

  /**
   * 
   * @return the VamsasFile
   */
  public File getVamsasFile() {
    return sessionFile;
  }

  /**
   * Expand a previously stored session into the sessionDir
   * 
   * @param sessionDir
   * @param storedSession
   *          public VamsasFile(File sessionDir, JarFile storedSession) throws
   *          IOException { // check if sessionDir is live or not if
   *          (!sessionDir.exists()) { sessionDir.mkdir(); }
   * 
   *          { // check its actually a writable directory }
   * 
   *          File sfile = new File(sessionDir, "vamsas.jar");
   *          VamsasFile(sfile); // if live - try to merge storedSession with
   *          sessionDir // - will probably fail through duplicate Vobject
   *          references needing to be dereferenced. // TODO: think of a way of
   *          specifying vorba_id scope for an application's references to allow
   *          merging of one vamsasDocument with another.
   * 
   *          }
   */
  /**
   * public interface for getting a lock. The lock object is internally
   * referenced so the lock will persist even after the return value of the
   * method goes out of scope.
   * 
   * @return null if lock couldn't be got or a valid Lock object.
   */
  public Lock getLock() {
    if (lockFile())
      return fileLock;
    return null;
  }

  /**
   * 
   * @param extantLock
   * @return null, extantLock or new Lock.
   */
  public Lock getLock(Lock extantLock) {
    if (lockFile(extantLock))
      return fileLock;
    return null;
  }

  /**
   * explicitly unlocks vamsas file. if you have called getLock() you *must*
   * call this to release the lock.
   */
  public void unLock() {
    this.unlockFile();
  }

}
