/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client.simpleclient;

/**
 * @author jimp
 * 
 */
public class LockTimeoutException extends Exception {
  static final long serialVersionUID = 1;

  static final String defaultMessage = "Timeout whilst waiting for lock on VamsasDocument";

  public LockTimeoutException() {
    super(defaultMessage);
  }

  public LockTimeoutException(String arg0, Throwable arg1) {
    super(arg0 + " (timeout whilst waiting for lock)", arg1);
    // TODO Auto-generated constructor stub
  }

  public LockTimeoutException(String arg0) {
    super(arg0 + " (timeout whilst waiting for lock)");
    // TODO Auto-generated constructor stub
  }

  public LockTimeoutException(Throwable arg0) {
    super(defaultMessage, arg0);
    // TODO Auto-generated constructor stub
  }

}
