/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client.simpleclient;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * methods for setting and checking binary flags in a vamsas session directory.
 * all methods apart from the constructor will throw a fatal error if the
 * flagFile is not a valid java.io.File object. LATER: extract SessionFlag
 * interface for generalizing the vamsas session code
 * 
 * @author jimp
 * 
 */
public class SessionFlagFile {
  private static Log log = LogFactory.getLog(SessionFlagFile.class);

  protected File flagFile = null;

  private void checkFlagFile() {
    if (flagFile == null) {
      log.fatal("Implementation error - uninitialized SessionFlagFile",
          new Error("Implementation error - uninitialized SessionFlagFile"));
    }
  }

  /**
   * will log a warning if exceptions occur during flag creation.
   * 
   * @return true if flag was set successfully
   */
  public boolean setFlag() {
    checkFlagFile();
    try {
      if (flagFile.createNewFile()) {
        log.debug("Set session flag " + flagFile);
      } else {
        log.debug("Session flag already set " + flagFile);
      }
      return true;
    } catch (Exception e) {
      log.warn("Couldn't set session flag " + flagFile, e);
    }
    return false;
  }

  /**
   * 
   * @return true if flag was cleared successfully
   */
  public boolean clearFlag() {
    checkFlagFile();
    if (flagFile.exists()) {
      log.debug("clearing session flag " + flagFile);
      if (!flagFile.delete()) {
        log.warn("failed to clear session flag " + flagFile);
        return false;
      }
    } else {
      log.debug("clearFlag called for already cleared flag " + flagFile);
    }
    return true;
  }

  /**
   * 
   * @return state of session flag
   */
  public boolean checkFlag() {
    checkFlagFile();
    if (flagFile.exists()) {
      if (log.isDebugEnabled())
        log.debug("Flag '" + flagFile + "' is set.");
      return true;
    }
    if (log.isDebugEnabled())
      log.debug("Flag '" + flagFile + "' is not set.");
    return false;
  }

  /**
   * @param flagFile
   */
  public SessionFlagFile(File flagFile) {
    super();
    this.flagFile = flagFile;
  };
}
