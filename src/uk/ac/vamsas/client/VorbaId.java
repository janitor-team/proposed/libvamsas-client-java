/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client;

import java.io.Serializable;

/**
 * The unique reference id for a Vamsas document Vobject, used by applications
 * to refer to the vamsas Vobject within their own data space in the vamsas
 * document. This is serializable (thanks to Dominik Lindner) so an application
 * can store it easily.
 * 
 * @author jimp
 */
public class VorbaId implements Serializable {
  /**
   * 1 is first vamsas release ID version.
   */
  private static final long serialVersionUID = 1L;

  protected String id;

  protected VorbaId() {
    super();
  }

  private VorbaId(String Id) {
    super();
    id = Id;
  }

  /**
   * 
   * @param vorbaObject
   *          the source of vorba Ids
   * @param vobject
   *          the Vobject to be registered with a new vorba id
   * @return
   */
  protected static VorbaId newId(IVorbaIdFactory vorbaObject, Vobject vobject) {
    // Make unique id from appSpace info in vorbaObject
    synchronized (vorbaObject) {
      vobject.vorbaId = vorbaObject.makeVorbaId(vobject);
      return vobject.vorbaId;
    }
  }

  /**
   * protected VorbaId constructor used when turning XML ID strings into vorba
   * IDs
   * 
   * @param id
   * @return VorbaId object or null if string was null.
   */
  protected static VorbaId newId(String id) {
    return (id == null) ? null : new VorbaId(id);
  }

  /**
   * @return Returns the id.
   */
  public String getId() {
    return id;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  public boolean equals(Object obj) {
    if (obj instanceof String)
      return id.equals(obj);
    else if (obj instanceof VorbaId)
      return id.equals(((VorbaId) obj).id);
    return false;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  public int hashCode() {
    return id.hashCode();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString() {
    return id;
  }

}
