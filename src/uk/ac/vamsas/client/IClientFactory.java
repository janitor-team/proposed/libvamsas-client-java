/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client;

import java.io.File;

/**
 * Defines methods for instantiating Vorba client application agents
 * 
 * @author jimp
 * 
 * @note  (it's VORBA, not CORBA!)
 * @history added additional sessionName argument for openAsNewSessionIClient method (v0.2)
 */

public interface IClientFactory {

  /**
   * Create a new Vorba Session
   * 
   * @param applicationHandle
   *          is the application's VAMSAS handle string
   * @throws NoDefaultSessionException
   *           if more than one session exists that the client may connect to
   */
  IClient getIClient(ClientHandle applicationHandle)
      throws NoDefaultSessionException;

  /**
   * returns new Vorba for a given session.
   * 
   * @param applicationHandle
   * @param sessionUrn
   *          session to connect to (or null to create a new session)
   * @return
   */
  IClient getIClient(ClientHandle applicationHandle, String sessionUrn);

  /**
   * returns new vorba for a given session acting as a particular identity
   * 
   * @param applicationHandle
   * @param userId
   * @param sessionUrn
   *          session to connect to (or null to create a new session)
   * @return
   */
  IClient getIClient(ClientHandle applicationHandle, UserHandle userId,
      String sessionUrn);

  /**
   * New session for application and specific user
   * 
   * @param applicationHandle
   * @param userId
   * @return
   * @throws NoDefaultSessionException
   *           if more than one session exists that the client may connect to
   */
  IClient getIClient(ClientHandle applicationHandle, UserHandle userId)
      throws NoDefaultSessionException;

  /**
   * Create a new sesssion for the application with the current user
   * 
   * @param applicationHandle
   * @return
   */
  IClient getNewSessionIClient(ClientHandle applicationHandle);

  /**
   * Create a new session for the application using a particular user identity
   * 
   * @param applicationHandle
   * @param userId
   * @return
   */
  IClient getNewSessionIClient(ClientHandle applicationHandle, UserHandle userId);

  /**
   * Create a new session and import an existing vamsas document into it.
   * Session name will inherit from vamsasDocument path.
   * @param applicationHandle
   * @param vamsasDocument
   * @return
   */
  IClient openAsNewSessionIClient(ClientHandle applicationHandle,
      File vamsasDocument) throws InvalidSessionDocumentException;

  /**
   * Create a new session and import an existing vamsas document into it.
   * 
   * @param applicationHandle
   * @param vamsasDocument
   * @param sessionName - preferred session name (may be null)  
   * @return
   */
  IClient openAsNewSessionIClient(ClientHandle applicationHandle, 
      File vamsasDocument, String sessionName) throws InvalidSessionDocumentException;

  /**
   * Create a new session as a particular user and import an existing vamsas
   * document into it.
   * 
   * @param applicationHandle
   * @param userId
   * @param vamsasDocument
   * @return
   */
  IClient openAsNewSessionIClient(ClientHandle applicationHandle,
      UserHandle userId, File vamsasDocument)
      throws InvalidSessionDocumentException;  
  /**
   * Create a new session as a particular user and import an existing vamsas
   * document into it.
   * 
   * @param applicationHandle
   * @param userId
   * @param vamsasDocument
   * @param sessionName - preferred session name (may be null)  
   * @return
   */
  IClient openAsNewSessionIClient(ClientHandle applicationHandle,
      UserHandle userId, File vamsasDocument, String sessionName)
  throws InvalidSessionDocumentException;
  /**
   * enumerate the active sessions this IClientFactory instance knows about. Can
   * be used by caller to pick a session on catching a
   * NoDefaultSessionException. LATER: Define interface for discovering more
   * information about a session (so it can be presented to a user in a
   * meaningful way)
   * 
   * @return possibly empty array of sessionUrn strings
   */
  public String[] getCurrentSessions();

}
