/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client.picking;

/**
 * Message class that can be used to send mouse over events.
 */
public class MouseOverMessage extends Message {
  private String vorbaID;

  private int position;

  /**
   * Constructs a new mouse over message.
   * 
   * @param vorbaID
   *          the VAMSAS object ID of the event's source (usually an alignment
   *          or alignment sequence)
   * @param position
   *          a position on the source in its coordinate system (ie a column or
   *          nucleotide/residue position)
   */
  public MouseOverMessage(String vorbaID, int position) {
    this.vorbaID = vorbaID;
    this.position = position;

    message = "MOUSEOVER\t" + "vorbaID=" + vorbaID + "\t" + "position="
        + position;
  }

  /**
   * Constructs a new mouse over message from its underlying string format.
   * 
   * @param str
   *          the string representation of an instance of this object
   * @throws java.lang.Exception
   *           if the message cannot be reconstructed
   */
  MouseOverMessage(String str) throws Exception {
    message = str;

    String[] elements = str.split("\t");

    for (int i = 0; i < elements.length; i++) {
      if (elements[i].startsWith("vorbaID="))
        vorbaID = elements[i].substring(8);
      else if (elements[i].startsWith("position="))
        position = Integer.parseInt(elements[i].substring(9));
    }
  }

  /**
   * Returns the VAMSAS object ID associated with this message.
   * 
   * @return the VAMSAS object ID associated with this message
   */
  public String getVorbaID() {
    return vorbaID;
  }

  /**
   * Returns the position value associated with this message.
   * 
   * @return the position value associated with this message
   */
  public int getPosition() {
    return position;
  }
}
