/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client.picking;

import java.util.logging.*;

import uk.ac.vamsas.objects.core.Input;
import uk.ac.vamsas.objects.core.Pos;
import uk.ac.vamsas.objects.core.Seg;

/**
 * Simple example of a (runnable) class that shows how to use the picking API.
 * use this to test new messages. Bear in mind that any active pick server only
 * relays messages that could be parsed at compile time - that means that you
 * must make sure that you close all currently running vamsas pick servers and
 * start a new one which copes with the new message in order to be able to
 * receive any of the new message classes.
 */
public class TestApp implements IMessageHandler {
  public static void main(String[] args) throws Exception {
    TestApp app = new TestApp();
  }

  public TestApp() {
    IPickManager manager = new SocketManager();
    manager.registerMessageHandler(this);
    while (true) {
      try {
        Thread.sleep((int) (Math.random() * 5000));
      } catch (InterruptedException e) {
      }

      int rnd = (int) (Math.random() * 100);
      CustomMessage msg = new CustomMessage("" + rnd);

      manager.sendMessage(msg);

      try {
        Thread.sleep((int) (Math.random() * 5000));
      } catch (InterruptedException e) {
      }

      MouseOverMessage mom = new MouseOverMessage("wibble", 10);
      manager.sendMessage(mom);

      try {
        Thread.sleep((int) (Math.random() * 5000));
      } catch (InterruptedException e) {
      }

      try {
        Input range = new Input();
        for (int in = 0, inL = (2 + ((int) (Math.random() * 10f))); in < inL; in++) {
          if ((inL % 2) == 1) {
            Seg sg = new Seg();
            sg.setStart((int) (Math.random() * 1000f));
            sg.setEnd((int) (Math.random() * 1000f));
            sg.setInclusive((((int) (Math.random() * 10f)) % 2) == 0);
            range.addSeg(sg);
          } else {
            Pos p = new Pos();
            p.setI((int) (Math.random() * 1000f));
            range.addPos(p);
          }
        }
        String[] ids = new String[(int) (Math.random() * 10)];
        for (int id = 0; id < ids.length; id++) {
          ids[id] = "object" + id;
        }

        SelectionMessage sel = new SelectionMessage("mysel", ids,
            (ids.length > 0) ? range : null, (ids.length == 0) ? true : false);
        sel.validate();
        manager.sendMessage(sel);
      } catch (Exception e) {
        System.err.println("Failed to construct and send selection message.");
        e.printStackTrace();
      }
    }
  }

  public void handleMessage(Message message) {
    System.out.println("Handler received " + message.getRawMessage());
    if (message instanceof MouseOverMessage) {
      MouseOverMessage mm = (MouseOverMessage) message;
      System.out.println("MouseOver : " + mm.getPosition() + " on id "
          + mm.getVorbaID());
    }
    if (message instanceof SelectionMessage) {
      SelectionMessage sm = (SelectionMessage) message;

      System.out.println("Selection "
          + ((sm.getSelectionID() == null) ? "on " : "'" + sm.getSelectionID()
              + "' on ")
          + sm.getVorbaIDs().length
          + ((sm.getRanges() == null) ? "." : " over "
              + ((sm.getRanges().getPosCount() > 0) ? ""
                  + sm.getRanges().getPosCount() + " positions" : ""
                  + sm.getRanges().getSegCount() + "intervals.")));
    }
  }
}
