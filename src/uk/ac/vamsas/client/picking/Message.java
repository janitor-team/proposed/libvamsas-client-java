/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client.picking;

/**
 * Abstract base class for all message types supported by the picking API.
 */
public abstract class Message {
  protected String message;

  /**
   * Constructs a new message.
   */
  protected Message() {

  }

  /**
   * Returns the raw message content as a string.
   * 
   * @return the raw message content as a string
   */
  public String getRawMessage() {
    return message;
  }

  /**
   * compare the on-wire message content of the given message to this.
   * 
   * @param msg
   * @return true if message content is equal
   */
  public boolean equals(Message msg) {
    return message.equals(msg.getRawMessage());
  }

  /**
   * Test consistence of a Message class implementation. This method throws an
   * error if the message object cannot be parsed into another instance of the
   * same object by invoking the MessageImpl(String this.getRawMessage())
   * constructor or that newinstance.getRawMessage != this.getRawMessage
   */
  public void validate() {
    try {
      java.lang.reflect.Constructor msgcons = this.getClass().getConstructor(
          new Class[] { String.class });
      if (msgcons == null) {
        throw new Exception("No " + this.getClass().getName()
            + "(String rawmessage) constructor.");
      }
      Message instance = (Message) msgcons.newInstance(new Object[] { this
          .getRawMessage() });
      if (!instance.getRawMessage().equals(getRawMessage())) {
        throw new Error(
            "Raw Message Content does not match :\nInitial Message:"
                + getRawMessage() + "\nParsed and regnerated as :\n"
                + instance.getRawMessage() + "\n");
      }
    } catch (Exception e) {
      throw new Error("Message implementation broken for " + this.getClass(), e);
    }
  }

}
