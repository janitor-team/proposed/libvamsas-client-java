/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client.picking;

/**
 * Interface that defines the methods required for a pick manager.
 */
public interface IPickManager {
  /**
   * Sends a message.
   * 
   * @param message
   *          the message to send
   */
  public void sendMessage(Message message);

  /**
   * Registers a message handler with the manager that allows the manager to
   * perform a method callback on that object whenever a message is received.
   * 
   * @param handler
   *          the message handler to register
   */
  public void registerMessageHandler(IMessageHandler handler);

  /**
   * Shutsdown the pick manager processes, terminating any connections to other
   * clients.
   */
  public void shutdown();
}
