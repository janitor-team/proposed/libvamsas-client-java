/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.JarInputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author jimp LATER: this may not be a necessary or useful class to return
 *         from IClientAppdata get*InputStream() methods
 */
public class AppDataInputStream extends DataInputStream implements DataInput {
  private Log log = LogFactory.getLog(AppDataInputStream.class);

  private boolean isOpen = false;

  /**
   * Wrapper for writing to/from AppData Entries in a Vamsas Document.
   */
  public AppDataInputStream(InputStream inputstream) {
    super(inputstream);
    isOpen = true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.io.FilterInputStream#close()
   */
  public void close() throws IOException {
    if (!isOpen) {
      log.debug("close() called on closed AppDataInputStream.");
      // throw new
      // IOException("Attempt to close an already closed AppDataInputStream");
    } else {
      isOpen = false;
    }
  }

  /**
   * Will return zero if stream has been closed.
   * 
   * @see java.io.FilterInputStream#available()
   */
  public int available() throws IOException {
    if (isOpen)
      return super.available();
    else
      return 0;
  }

}
