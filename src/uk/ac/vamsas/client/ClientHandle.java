/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client;

import java.io.Serializable;

/**
 * Uniquely describes a vamsas client application.
 * 
 * @author jimp
 */
public class ClientHandle implements Serializable {
  static final long serialVersionUID = 0;

  /**
   * @param clientName
   * @param version
   */
  public ClientHandle(String _clientName, String _version) {
    super();
    this.clientName = _clientName;
    this.version = _version;
    // this.setClientUrn("vamsas://"+clientName+":"+version+"/"); // TODO:
    // decide on application handle ornthing (used to prefix new ids made by a
    // particular application)
    this.setClientUrn(this.generateClientUrn(this.clientName, this.version));
  }

  /**
   * (non-unique) human readable vamsas client name
   */
  String clientName;

  /**
   * the unambiguous client identifier This may be rewritten by the Vorba object
   * if other clients with the same name, version and user are involved in a
   * session.
   * 
   */
  String clientUrn;

  /**
   * version modifier to tag application space
   */
  String version;

  /**
   * Generates the client Urn from the clientName and version
   * 
   * @param clientName
   *          (non-unique) human readable vamsas client name
   * @param version
   *          version modifier
   * @return a String corresponding to the clientUrn
   */
  private String generateClientUrn(String _clientName, String _version) {
    return ("vamsas://" + _clientName + ":" + _version + "/").intern();
  }

  /**
   * @return Returns the clientUrn.
   */
  public String getClientUrn() {
    return this.clientUrn;
  }

  /**
   * May become protected - should only be set by a Vorba object.
   * 
   * @param clientUrn
   *          The clientUrn to set.
   */
  public void setClientUrn(String _clientUrn) {
    this.clientUrn = _clientUrn;
  }

  /**
   * @return Returns the version.
   */
  public String getVersion() {
    return this.version;
  }

  /**
   * @param version
   *          The version to set.
   */
  public void setVersion(String _version) {
    this.version = _version;
    this.setClientUrn(this.generateClientUrn(this.clientName, this.version));
  }

  /**
   * @return Returns the clientName.
   */
  public String getClientName() {
    return this.clientName;
  }

  /**
   * @param clientName
   *          The clientName to set.
   */
  public void setClientName(String _clientName) {
    this.clientName = _clientName;
    this.setClientUrn(this.generateClientUrn(this.clientName, this.version));
  }

  public boolean equals(Object that) {
    if (that instanceof ClientHandle)
      return this.equals((ClientHandle) that);
    return false;
  }

  public boolean equals(ClientHandle that) {
    return ((this.clientName == null || this.clientName.equals(that.clientName))
        && (this.version == null || this.version.equals(that.version)) && (this.clientUrn == null || this.clientUrn
        .equals(that.clientUrn)));
  }

  public String getClientNCname() {

    String ncname = clientName.replace(':', '_');
    ncname = ncname.replace('@', '.');
    return ncname;
  }
}
