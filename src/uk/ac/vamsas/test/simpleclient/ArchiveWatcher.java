/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.test.simpleclient;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.vamsas.client.ClientHandle;
import uk.ac.vamsas.client.simpleclient.FileWatcher;
import uk.ac.vamsas.client.simpleclient.Lock;
import uk.ac.vamsas.client.simpleclient.SimpleDocument;
import uk.ac.vamsas.client.simpleclient.VamsasArchiveReader;
import uk.ac.vamsas.client.simpleclient.VamsasFile;
import uk.ac.vamsas.objects.core.VamsasDocument;

/**
 * demo of archive watching process - should mimic the clientsfileTest
 * watcher/monitor process.
 * 
 * @author jimp
 * 
 */
public class ArchiveWatcher {
  private static Log log = LogFactory.getLog(ArchiveWatcher.class);

  private static CommandProcessor cproc = new CommandProcessor();
  static {
    cproc.addCommand("new", 0, "no args");
    cproc.addCommand("delete", 0, "no args");
    cproc.addCommand("watch", 0, "no args");
    cproc.addCommand("file", 1, "Need vamsas archive as argument.");
  }

  public static void main(String[] args) {
    try {

      if (args != null && args.length > 0) {
        File archive = new File(args[0]);
        log.info("Watching file " + args[0]);
        int argc = 1;
        while (argc < args.length) {
          // vars needed for operations
          ClientHandle ch;
          int com = cproc.getCommand(args, argc);
          argc++;
          switch (com) {
          case 0:
            // new
            log.info("Doing locked deletion and new-file creation.");
            {
              if (!archive.exists())
                archive.createNewFile();
              VamsasFile sf = new VamsasFile(archive);
              Lock l = sf.getLock();
              archive.delete();
              archive.createNewFile();
              sf.unLock();
            }
            break;
          case 1:
            // delete
            log.info("Deleting " + archive + " without locking it first.");
            archive.delete();
            break;
          case 2:
            // watch
            log.info("Endlessly Watching file " + archive);
            /*
             * if (!archive.exists()) archive.createNewFile();
             */// watch the new file... - taken straight from ClientsFileTest
            FileWatcher w = new FileWatcher(archive);
            while (true) {
              // get watcher's lock to ensure state change is fixed for
              // retrieval
              Lock chlock = w.getChangedState();
              if (chlock != null) {
                log.info("Got lock on "
                    + archive
                    + (archive.exists() ? " exists l=" + archive.length()
                        : "(non existant)"));
                if (archive.length() > 0) {
                  VamsasArchiveReader vreader = new VamsasArchiveReader(archive);
                  SimpleDocument sdoc = new SimpleDocument(
                      "testing vamsas watcher");
                  try {
                    VamsasDocument d = sdoc.getVamsasDocument(vreader);
                    if (d != null) {
                      ArchiveReports.reportDocument(d, vreader, false,
                          System.out);
                    }
                    System.out
                        .println("Update at "
                            + System.currentTimeMillis()
                            + "\n\n********************************************************\n");
                  } catch (Exception e) {
                    log.error("Unmarshalling failed.", e);
                  }
                  vreader.close();
                  w.setState();
                }
              }
            }
            // break;
          case 3: // set file
            archive = new File(args[argc++]);
            break;
          case 4:
            break;
          default:
            log.warn("Unknown command  + " + args[argc++]);
          }
        }
      }
    } catch (Exception e) {
      log.error(e);
    }

  }
}
