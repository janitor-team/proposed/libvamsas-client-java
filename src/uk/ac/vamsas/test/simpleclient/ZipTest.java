/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.test.simpleclient;

import java.io.File;
import java.util.jar.JarFile;

/**
 * really simple test to see if we can open an archive and test for the
 * existence of an entry called vamssDocument.xml.
 * 
 * 'Pathological' archives fail this test (due to things like funny characters
 * or lots of '.' in the entry name.
 * 
 * @author JimP
 * 
 */
public class ZipTest {

  /**
   * @param args
   *          single filename as an argument to open as a Jar file.
   */
  public static void main(String[] args) {
    File av = new File(args[0]);
    boolean jfailed = false;
    try {
      JarFile jf = new JarFile(av, false, JarFile.OPEN_READ);
      if (jf.getEntry("vamsasDocument.xml") != null) {
        System.out.println("Valid archive " + av);
      }
      jf.close();
      return;
    } catch (Exception f) {
      System.out.println("Couldn't access jar archive " + av);
      f.printStackTrace(System.out);
    }
    try {
      System.out.println("Trying the Apache Zip Package:");
      org.apache.tools.zip.ZipFile jf = new org.apache.tools.zip.ZipFile(av);
      if (jf.getEntry("vamsasDocument.xml") != null) {
        System.out.println("Valid archive " + av);
      }
      jf.close();
    } catch (Exception f) {
      System.out.println("Couldn't access jar archive " + av);
      f.printStackTrace(System.out);
    }
  }
}
