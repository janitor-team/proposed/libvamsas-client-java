/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.test.simpleclient;

import java.util.Iterator;
import java.util.Vector;

public class CommandProcessor {
  /**
   * this is not getOPT!!!! - processes a *series* of space separated commands -
   * some of which take arguments.
   */
  private Vector commands;

  /*
   * static { ClientsFileTest.commands=new Vector();
   * ClientsFileTest.commands.add(new String("add"));
   * ClientsFileTest.commands.add(new String("remove"));
   * ClientsFileTest.commands.add(new String("list"));
   * ClientsFileTest.commands.add(new String("clear"));
   * ClientsFileTest.commands.add(new String("watch"));
   * ClientsFileTest.commands.add(new String("monitor")); }
   */

  public int addCommand(String cmd, int argneed, String complainString) {
    int cnum = 0;
    if (commands == null)
      commands = new Vector();
    else
      cnum = commands.size();
    Vector cv = new Vector();
    cv.add(new String(cmd));
    cv.add(new Integer(argneed));
    cv.add(new String(complainString));
    commands.add(cv);
    return cnum;
  }

  /**
   * Integer argl, Integer argpos, String cmd, Integer argneed, String msg in
   * vector
   */
  public void complainArgs(int argl, int argpos, Vector ca) {
    int argneed = ((Integer) ca.get(1)).intValue();
    if (argl - argpos < argneed)
      throw new Error(((String) ca.get(0)) + " at position " + argpos
          + " needs " + argneed + " arguments : " + (String) ca.get(2));
  }

  /**
   * find and verify a command
   * 
   * @param args
   *          argstring
   * @param argpos
   *          position to check for command
   * @return matching command or -1
   */
  public int getCommand(String[] args, int argpos) {
    Iterator coms = commands.iterator();
    int com = -1, argc;
    argc = argpos;
    while ((coms != null) && coms.hasNext()) {
      com++;
      Vector comnext = (Vector) coms.next();
      if (args[argc].toLowerCase().equals((String) comnext.get(0))) {
        if (comnext.size() > 2)
          complainArgs(args.length, argc + 1, comnext);
        return com;
      }
    }
    return -1;

  }
}
