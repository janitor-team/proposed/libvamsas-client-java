/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.test.simpleclient;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

import uk.ac.vamsas.client.simpleclient.SessionFile;
import uk.ac.vamsas.client.simpleclient.VamsasArchiveReader;
import uk.ac.vamsas.client.simpleclient.VamsasFile;
import uk.ac.vamsas.objects.core.VAMSAS;
import uk.ac.vamsas.objects.core.VamsasDocument;

public class ArchiveStreamReader {
  /**
   * tests VamsasArchiveReader archive reader on a vamsas jar file opened as a
   * stream
   * 
   * @param args
   */
  public static void main(String args[]) {

    try {
      VamsasFile av = new VamsasFile(new File(args[0]));
      VamsasArchiveReader var = new VamsasArchiveReader(av.getLock());
      VAMSAS roots[] = null;
      if (var.isValid()) {
        InputStreamReader vdoc = new InputStreamReader(var
            .getVamsasDocumentStream());
        VamsasDocument doc = VamsasDocument.unmarshal(vdoc);
        if (ArchiveReports.reportDocument(doc, var, true, System.out)) {
          roots = doc.getVAMSAS();
        }
      } else {
        InputStream vxmlis = var.getVamsasXmlStream();

        if (vxmlis != null) { // Might be an old vamsas file.
          BufferedInputStream ixml = new BufferedInputStream(var
              .getVamsasXmlStream());
          InputStreamReader vxml = new InputStreamReader(ixml);
          VAMSAS root;
          // unmarshal seems to always close the stream (should check this)
          if ((root = VAMSAS.unmarshal(vxml)) != null) {
            System.out.println("Read a root.");
            roots = new VAMSAS[1];
            roots[0] = root;
          }
        }
      }
      if (!ArchiveReports.rootReport(roots, true, System.out))
        System.err.print(args[0] + " is not a valid vamsas archive.");
    } catch (Exception e) {
      e.printStackTrace(System.err);
    }
  }
}
