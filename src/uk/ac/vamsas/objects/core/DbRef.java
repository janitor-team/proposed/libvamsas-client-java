/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Store a list of database references for this sequence record - with optional
 * mapping from database sequence to the given sequence record
 * 
 * @version $Revision$ $Date: 2007-06-29 13:23:12 +0100 (Fri, 29 Jun 2007)
 *          $
 */
public class DbRef extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * TODO Database Naming Convention: either start using LSID (so change type to
   * URI) or leave this as an uncontrolled/unspecified string ID
   * 
   */
  private java.lang.String _source;

  /**
   * Version must be specified -
   * 
   */
  private java.lang.String _version;

  /**
   * TODO: make some specification of the database field from which this
   * accessionId is taken from - should that be a special property of the dbRef
   * object ?
   * 
   */
  private java.lang.String _accessionId;

  /**
   * Primary Key for vamsas object referencing
   */
  private java.lang.String _id;

  /**
   * the local mapType maps from the parent sequence coordinate frame to the
   * reference frame defined by the dbRef element. The mapped mapType is the
   * mapped range defined on the dbRef element's reference frame.
   * Conventionally, the unit attribute defaults to 1, or will be inferred from
   * the local sequence's dictionary type and any dictionary type associated
   * with the database being mapped to. However, it may be used to avoid
   * ambiguity.
   * 
   */
  private java.util.Vector _mapList;

  /**
   * Field _linkList.
   */
  private java.util.Vector _linkList;

  /**
   * Field _propertyList.
   */
  private java.util.Vector _propertyList;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public DbRef() {
    super();
    this._mapList = new java.util.Vector();
    this._linkList = new java.util.Vector();
    this._propertyList = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vLink
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addLink(final uk.ac.vamsas.objects.core.Link vLink)
      throws java.lang.IndexOutOfBoundsException {
    this._linkList.addElement(vLink);
  }

  /**
   * 
   * 
   * @param index
   * @param vLink
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addLink(final int index,
      final uk.ac.vamsas.objects.core.Link vLink)
      throws java.lang.IndexOutOfBoundsException {
    this._linkList.add(index, vLink);
  }

  /**
   * 
   * 
   * @param vMap
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addMap(final uk.ac.vamsas.objects.core.Map vMap)
      throws java.lang.IndexOutOfBoundsException {
    this._mapList.addElement(vMap);
  }

  /**
   * 
   * 
   * @param index
   * @param vMap
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addMap(final int index, final uk.ac.vamsas.objects.core.Map vMap)
      throws java.lang.IndexOutOfBoundsException {
    this._mapList.add(index, vMap);
  }

  /**
   * 
   * 
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addProperty(final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    this._propertyList.addElement(vProperty);
  }

  /**
   * 
   * 
   * @param index
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addProperty(final int index,
      final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    this._propertyList.add(index, vProperty);
  }

  /**
   * Method enumerateLink.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Link elements
   */
  public java.util.Enumeration enumerateLink() {
    return this._linkList.elements();
  }

  /**
   * Method enumerateMap.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Map elements
   */
  public java.util.Enumeration enumerateMap() {
    return this._mapList.elements();
  }

  /**
   * Method enumerateProperty.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Property elements
   */
  public java.util.Enumeration enumerateProperty() {
    return this._propertyList.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof DbRef) {

      DbRef temp = (DbRef) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._source != null) {
        if (temp._source == null)
          return false;
        if (this._source != temp._source) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._source);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._source);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._source);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._source);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._source.equals(temp._source)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._source);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._source);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._source);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._source);
          }
        }
      } else if (temp._source != null)
        return false;
      if (this._version != null) {
        if (temp._version == null)
          return false;
        if (this._version != temp._version) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._version);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._version);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._version);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._version);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._version.equals(temp._version)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._version);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._version);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._version);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._version);
          }
        }
      } else if (temp._version != null)
        return false;
      if (this._accessionId != null) {
        if (temp._accessionId == null)
          return false;
        if (this._accessionId != temp._accessionId) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._accessionId);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._accessionId);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._accessionId);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._accessionId);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._accessionId.equals(temp._accessionId)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._accessionId);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._accessionId);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._accessionId);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._accessionId);
          }
        }
      } else if (temp._accessionId != null)
        return false;
      if (this._id != null) {
        if (temp._id == null)
          return false;
        if (this._id != temp._id) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._id);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._id);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._id.equals(temp._id)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
          }
        }
      } else if (temp._id != null)
        return false;
      if (this._mapList != null) {
        if (temp._mapList == null)
          return false;
        if (this._mapList != temp._mapList) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._mapList);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._mapList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._mapList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._mapList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._mapList.equals(temp._mapList)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._mapList);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._mapList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._mapList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._mapList);
          }
        }
      } else if (temp._mapList != null)
        return false;
      if (this._linkList != null) {
        if (temp._linkList == null)
          return false;
        if (this._linkList != temp._linkList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._linkList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._linkList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._linkList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._linkList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._linkList.equals(temp._linkList)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._linkList);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._linkList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._linkList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._linkList);
          }
        }
      } else if (temp._linkList != null)
        return false;
      if (this._propertyList != null) {
        if (temp._propertyList == null)
          return false;
        if (this._propertyList != temp._propertyList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._propertyList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._propertyList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._propertyList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._propertyList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._propertyList.equals(temp._propertyList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._propertyList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._propertyList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._propertyList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._propertyList);
          }
        }
      } else if (temp._propertyList != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'accessionId'. The field 'accessionId' has the
   * following description: TODO: make some specification of the database field
   * from which this accessionId is taken from - should that be a special
   * property of the dbRef object ?
   * 
   * 
   * @return the value of field 'AccessionId'.
   */
  public java.lang.String getAccessionId() {
    return this._accessionId;
  }

  /**
   * Returns the value of field 'id'. The field 'id' has the following
   * description: Primary Key for vamsas object referencing
   * 
   * @return the value of field 'Id'.
   */
  public java.lang.String getId() {
    return this._id;
  }

  /**
   * Method getLink.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Link at the given index
   */
  public uk.ac.vamsas.objects.core.Link getLink(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._linkList.size()) {
      throw new IndexOutOfBoundsException("getLink: Index value '" + index
          + "' not in range [0.." + (this._linkList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Link) _linkList.get(index);
  }

  /**
   * Method getLink.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Link[] getLink() {
    uk.ac.vamsas.objects.core.Link[] array = new uk.ac.vamsas.objects.core.Link[0];
    return (uk.ac.vamsas.objects.core.Link[]) this._linkList.toArray(array);
  }

  /**
   * Method getLinkAsReference.Returns a reference to '_linkList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getLinkAsReference() {
    return this._linkList;
  }

  /**
   * Method getLinkCount.
   * 
   * @return the size of this collection
   */
  public int getLinkCount() {
    return this._linkList.size();
  }

  /**
   * Method getMap.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Map at the given index
   */
  public uk.ac.vamsas.objects.core.Map getMap(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._mapList.size()) {
      throw new IndexOutOfBoundsException("getMap: Index value '" + index
          + "' not in range [0.." + (this._mapList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Map) _mapList.get(index);
  }

  /**
   * Method getMap.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Map[] getMap() {
    uk.ac.vamsas.objects.core.Map[] array = new uk.ac.vamsas.objects.core.Map[0];
    return (uk.ac.vamsas.objects.core.Map[]) this._mapList.toArray(array);
  }

  /**
   * Method getMapAsReference.Returns a reference to '_mapList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getMapAsReference() {
    return this._mapList;
  }

  /**
   * Method getMapCount.
   * 
   * @return the size of this collection
   */
  public int getMapCount() {
    return this._mapList.size();
  }

  /**
   * Method getProperty.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Property at the given
   *         index
   */
  public uk.ac.vamsas.objects.core.Property getProperty(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._propertyList.size()) {
      throw new IndexOutOfBoundsException("getProperty: Index value '" + index
          + "' not in range [0.." + (this._propertyList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Property) _propertyList.get(index);
  }

  /**
   * Method getProperty.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Property[] getProperty() {
    uk.ac.vamsas.objects.core.Property[] array = new uk.ac.vamsas.objects.core.Property[0];
    return (uk.ac.vamsas.objects.core.Property[]) this._propertyList
        .toArray(array);
  }

  /**
   * Method getPropertyAsReference.Returns a reference to '_propertyList'. No
   * type checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getPropertyAsReference() {
    return this._propertyList;
  }

  /**
   * Method getPropertyCount.
   * 
   * @return the size of this collection
   */
  public int getPropertyCount() {
    return this._propertyList.size();
  }

  /**
   * Returns the value of field 'source'. The field 'source' has the following
   * description: TODO Database Naming Convention: either start using LSID (so
   * change type to URI) or leave this as an uncontrolled/unspecified string ID
   * 
   * 
   * @return the value of field 'Source'.
   */
  public java.lang.String getSource() {
    return this._source;
  }

  /**
   * Returns the value of field 'version'. The field 'version' has the following
   * description: Version must be specified -
   * 
   * 
   * @return the value of field 'Version'.
   */
  public java.lang.String getVersion() {
    return this._version;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_source != null
        && !org.castor.util.CycleBreaker.startingToCycle(_source)) {
      result = 37 * result + _source.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_source);
    }
    if (_version != null
        && !org.castor.util.CycleBreaker.startingToCycle(_version)) {
      result = 37 * result + _version.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_version);
    }
    if (_accessionId != null
        && !org.castor.util.CycleBreaker.startingToCycle(_accessionId)) {
      result = 37 * result + _accessionId.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_accessionId);
    }
    if (_id != null && !org.castor.util.CycleBreaker.startingToCycle(_id)) {
      result = 37 * result + _id.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_id);
    }
    if (_mapList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_mapList)) {
      result = 37 * result + _mapList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_mapList);
    }
    if (_linkList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_linkList)) {
      result = 37 * result + _linkList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_linkList);
    }
    if (_propertyList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_propertyList)) {
      result = 37 * result + _propertyList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_propertyList);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
     */
  public void removeAllLink() {
    this._linkList.clear();
  }

  /**
     */
  public void removeAllMap() {
    this._mapList.clear();
  }

  /**
     */
  public void removeAllProperty() {
    this._propertyList.clear();
  }

  /**
   * Method removeLink.
   * 
   * @param vLink
   * @return true if the object was removed from the collection.
   */
  public boolean removeLink(final uk.ac.vamsas.objects.core.Link vLink) {
    boolean removed = _linkList.remove(vLink);
    return removed;
  }

  /**
   * Method removeLinkAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Link removeLinkAt(final int index) {
    java.lang.Object obj = this._linkList.remove(index);
    return (uk.ac.vamsas.objects.core.Link) obj;
  }

  /**
   * Method removeMap.
   * 
   * @param vMap
   * @return true if the object was removed from the collection.
   */
  public boolean removeMap(final uk.ac.vamsas.objects.core.Map vMap) {
    boolean removed = _mapList.remove(vMap);
    return removed;
  }

  /**
   * Method removeMapAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Map removeMapAt(final int index) {
    java.lang.Object obj = this._mapList.remove(index);
    return (uk.ac.vamsas.objects.core.Map) obj;
  }

  /**
   * Method removeProperty.
   * 
   * @param vProperty
   * @return true if the object was removed from the collection.
   */
  public boolean removeProperty(
      final uk.ac.vamsas.objects.core.Property vProperty) {
    boolean removed = _propertyList.remove(vProperty);
    return removed;
  }

  /**
   * Method removePropertyAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Property removePropertyAt(final int index) {
    java.lang.Object obj = this._propertyList.remove(index);
    return (uk.ac.vamsas.objects.core.Property) obj;
  }

  /**
   * Sets the value of field 'accessionId'. The field 'accessionId' has the
   * following description: TODO: make some specification of the database field
   * from which this accessionId is taken from - should that be a special
   * property of the dbRef object ?
   * 
   * 
   * @param accessionId
   *          the value of field 'accessionId'.
   */
  public void setAccessionId(final java.lang.String accessionId) {
    this._accessionId = accessionId;
  }

  /**
   * Sets the value of field 'id'. The field 'id' has the following description:
   * Primary Key for vamsas object referencing
   * 
   * @param id
   *          the value of field 'id'.
   */
  public void setId(final java.lang.String id) {
    this._id = id;
  }

  /**
   * 
   * 
   * @param index
   * @param vLink
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setLink(final int index,
      final uk.ac.vamsas.objects.core.Link vLink)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._linkList.size()) {
      throw new IndexOutOfBoundsException("setLink: Index value '" + index
          + "' not in range [0.." + (this._linkList.size() - 1) + "]");
    }

    this._linkList.set(index, vLink);
  }

  /**
   * 
   * 
   * @param vLinkArray
   */
  public void setLink(final uk.ac.vamsas.objects.core.Link[] vLinkArray) {
    // -- copy array
    _linkList.clear();

    for (int i = 0; i < vLinkArray.length; i++) {
      this._linkList.add(vLinkArray[i]);
    }
  }

  /**
   * Sets the value of '_linkList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vLinkList
   *          the Vector to copy.
   */
  public void setLink(final java.util.Vector vLinkList) {
    // copy vector
    this._linkList.clear();

    this._linkList.addAll(vLinkList);
  }

  /**
   * Sets the value of '_linkList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param linkVector
   *          the Vector to set.
   */
  public void setLinkAsReference(final java.util.Vector linkVector) {
    this._linkList = linkVector;
  }

  /**
   * 
   * 
   * @param index
   * @param vMap
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setMap(final int index, final uk.ac.vamsas.objects.core.Map vMap)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._mapList.size()) {
      throw new IndexOutOfBoundsException("setMap: Index value '" + index
          + "' not in range [0.." + (this._mapList.size() - 1) + "]");
    }

    this._mapList.set(index, vMap);
  }

  /**
   * 
   * 
   * @param vMapArray
   */
  public void setMap(final uk.ac.vamsas.objects.core.Map[] vMapArray) {
    // -- copy array
    _mapList.clear();

    for (int i = 0; i < vMapArray.length; i++) {
      this._mapList.add(vMapArray[i]);
    }
  }

  /**
   * Sets the value of '_mapList' by copying the given Vector. All elements will
   * be checked for type safety.
   * 
   * @param vMapList
   *          the Vector to copy.
   */
  public void setMap(final java.util.Vector vMapList) {
    // copy vector
    this._mapList.clear();

    this._mapList.addAll(vMapList);
  }

  /**
   * Sets the value of '_mapList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param mapVector
   *          the Vector to set.
   */
  public void setMapAsReference(final java.util.Vector mapVector) {
    this._mapList = mapVector;
  }

  /**
   * 
   * 
   * @param index
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setProperty(final int index,
      final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._propertyList.size()) {
      throw new IndexOutOfBoundsException("setProperty: Index value '" + index
          + "' not in range [0.." + (this._propertyList.size() - 1) + "]");
    }

    this._propertyList.set(index, vProperty);
  }

  /**
   * 
   * 
   * @param vPropertyArray
   */
  public void setProperty(
      final uk.ac.vamsas.objects.core.Property[] vPropertyArray) {
    // -- copy array
    _propertyList.clear();

    for (int i = 0; i < vPropertyArray.length; i++) {
      this._propertyList.add(vPropertyArray[i]);
    }
  }

  /**
   * Sets the value of '_propertyList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vPropertyList
   *          the Vector to copy.
   */
  public void setProperty(final java.util.Vector vPropertyList) {
    // copy vector
    this._propertyList.clear();

    this._propertyList.addAll(vPropertyList);
  }

  /**
   * Sets the value of '_propertyList' by setting it to the given Vector. No
   * type checking is performed.
   * 
   * @deprecated
   * 
   * @param propertyVector
   *          the Vector to set.
   */
  public void setPropertyAsReference(final java.util.Vector propertyVector) {
    this._propertyList = propertyVector;
  }

  /**
   * Sets the value of field 'source'. The field 'source' has the following
   * description: TODO Database Naming Convention: either start using LSID (so
   * change type to URI) or leave this as an uncontrolled/unspecified string ID
   * 
   * 
   * @param source
   *          the value of field 'source'.
   */
  public void setSource(final java.lang.String source) {
    this._source = source;
  }

  /**
   * Sets the value of field 'version'. The field 'version' has the following
   * description: Version must be specified -
   * 
   * 
   * @param version
   *          the value of field 'version'.
   */
  public void setVersion(final java.lang.String version) {
    this._version = version;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.DbRef
   */
  public static uk.ac.vamsas.objects.core.DbRef unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.DbRef) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.DbRef.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
