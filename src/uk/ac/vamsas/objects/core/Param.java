/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Param.
 * 
 * @version $Revision$ $Date$
 */
public class Param extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * internal content storage
   */
  private java.lang.String _content = "";

  /**
   * Field _name.
   */
  private java.lang.String _name;

  /**
   * The type specifies how the property will be parsed. Empty property strings
   * are allowed, and can be used to prototype the input to a document. TODO:
   * specify allowed types
   */
  private java.lang.String _type;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public Param() {
    super();
    setContent("");
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof Param) {

      Param temp = (Param) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._content != null) {
        if (temp._content == null)
          return false;
        if (this._content != temp._content) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._content);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._content);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._content);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._content);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._content.equals(temp._content)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._content);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._content);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._content);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._content);
          }
        }
      } else if (temp._content != null)
        return false;
      if (this._name != null) {
        if (temp._name == null)
          return false;
        if (this._name != temp._name) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._name);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._name);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._name);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._name);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._name.equals(temp._name)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._name);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._name);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._name);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._name);
          }
        }
      } else if (temp._name != null)
        return false;
      if (this._type != null) {
        if (temp._type == null)
          return false;
        if (this._type != temp._type) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._type);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._type);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._type);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._type);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._type.equals(temp._type)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._type);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._type);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._type);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._type);
          }
        }
      } else if (temp._type != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'content'. The field 'content' has the following
   * description: internal content storage
   * 
   * @return the value of field 'Content'.
   */
  public java.lang.String getContent() {
    return this._content;
  }

  /**
   * Returns the value of field 'name'.
   * 
   * @return the value of field 'Name'.
   */
  public java.lang.String getName() {
    return this._name;
  }

  /**
   * Returns the value of field 'type'. The field 'type' has the following
   * description: The type specifies how the property will be parsed. Empty
   * property strings are allowed, and can be used to prototype the input to a
   * document. TODO: specify allowed types
   * 
   * @return the value of field 'Type'.
   */
  public java.lang.String getType() {
    return this._type;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_content != null
        && !org.castor.util.CycleBreaker.startingToCycle(_content)) {
      result = 37 * result + _content.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_content);
    }
    if (_name != null && !org.castor.util.CycleBreaker.startingToCycle(_name)) {
      result = 37 * result + _name.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_name);
    }
    if (_type != null && !org.castor.util.CycleBreaker.startingToCycle(_type)) {
      result = 37 * result + _type.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_type);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
   * Sets the value of field 'content'. The field 'content' has the following
   * description: internal content storage
   * 
   * @param content
   *          the value of field 'content'.
   */
  public void setContent(final java.lang.String content) {
    this._content = content;
  }

  /**
   * Sets the value of field 'name'.
   * 
   * @param name
   *          the value of field 'name'.
   */
  public void setName(final java.lang.String name) {
    this._name = name;
  }

  /**
   * Sets the value of field 'type'. The field 'type' has the following
   * description: The type specifies how the property will be parsed. Empty
   * property strings are allowed, and can be used to prototype the input to a
   * document. TODO: specify allowed types
   * 
   * @param type
   *          the value of field 'type'.
   */
  public void setType(final java.lang.String type) {
    this._type = type;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.Param
   */
  public static uk.ac.vamsas.objects.core.Param unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.Param) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.Param.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
