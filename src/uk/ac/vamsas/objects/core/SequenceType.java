/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class SequenceType.
 * 
 * @version $Revision$ $Date: 2007-06-28 14:51:44 +0100 (Thu, 28 Jun 2007)
 *          $
 */
public class SequenceType extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Field _start.
   */
  private long _start;

  /**
   * keeps track of state for field: _start
   */
  private boolean _has_start;

  /**
   * Field _end.
   */
  private long _end;

  /**
   * keeps track of state for field: _end
   */
  private boolean _has_end;

  /**
   * Field _sequence.
   */
  private java.lang.String _sequence;

  /**
   * Field _name.
   */
  private java.lang.String _name;

  /**
   * Field _description.
   */
  private java.lang.String _description;

  /**
   * additional typed properties
   */
  private java.util.Vector _propertyList;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public SequenceType() {
    super();
    this._propertyList = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addProperty(final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    this._propertyList.addElement(vProperty);
  }

  /**
   * 
   * 
   * @param index
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addProperty(final int index,
      final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    this._propertyList.add(index, vProperty);
  }

  /**
     */
  public void deleteEnd() {
    this._has_end = false;
  }

  /**
     */
  public void deleteStart() {
    this._has_start = false;
  }

  /**
   * Method enumerateProperty.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Property elements
   */
  public java.util.Enumeration enumerateProperty() {
    return this._propertyList.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof SequenceType) {

      SequenceType temp = (SequenceType) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._start != temp._start)
        return false;
      if (this._has_start != temp._has_start)
        return false;
      if (this._end != temp._end)
        return false;
      if (this._has_end != temp._has_end)
        return false;
      if (this._sequence != null) {
        if (temp._sequence == null)
          return false;
        if (this._sequence != temp._sequence) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._sequence);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._sequence);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._sequence);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._sequence);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._sequence.equals(temp._sequence)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._sequence);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._sequence);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._sequence);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._sequence);
          }
        }
      } else if (temp._sequence != null)
        return false;
      if (this._name != null) {
        if (temp._name == null)
          return false;
        if (this._name != temp._name) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._name);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._name);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._name);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._name);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._name.equals(temp._name)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._name);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._name);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._name);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._name);
          }
        }
      } else if (temp._name != null)
        return false;
      if (this._description != null) {
        if (temp._description == null)
          return false;
        if (this._description != temp._description) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._description);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._description);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._description);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._description);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._description.equals(temp._description)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._description);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._description);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._description);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._description);
          }
        }
      } else if (temp._description != null)
        return false;
      if (this._propertyList != null) {
        if (temp._propertyList == null)
          return false;
        if (this._propertyList != temp._propertyList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._propertyList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._propertyList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._propertyList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._propertyList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._propertyList.equals(temp._propertyList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._propertyList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._propertyList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._propertyList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._propertyList);
          }
        }
      } else if (temp._propertyList != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'description'.
   * 
   * @return the value of field 'Description'.
   */
  public java.lang.String getDescription() {
    return this._description;
  }

  /**
   * Returns the value of field 'end'.
   * 
   * @return the value of field 'End'.
   */
  public long getEnd() {
    return this._end;
  }

  /**
   * Returns the value of field 'name'.
   * 
   * @return the value of field 'Name'.
   */
  public java.lang.String getName() {
    return this._name;
  }

  /**
   * Method getProperty.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Property at the given
   *         index
   */
  public uk.ac.vamsas.objects.core.Property getProperty(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._propertyList.size()) {
      throw new IndexOutOfBoundsException("getProperty: Index value '" + index
          + "' not in range [0.." + (this._propertyList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Property) _propertyList.get(index);
  }

  /**
   * Method getProperty.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Property[] getProperty() {
    uk.ac.vamsas.objects.core.Property[] array = new uk.ac.vamsas.objects.core.Property[0];
    return (uk.ac.vamsas.objects.core.Property[]) this._propertyList
        .toArray(array);
  }

  /**
   * Method getPropertyAsReference.Returns a reference to '_propertyList'. No
   * type checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getPropertyAsReference() {
    return this._propertyList;
  }

  /**
   * Method getPropertyCount.
   * 
   * @return the size of this collection
   */
  public int getPropertyCount() {
    return this._propertyList.size();
  }

  /**
   * Returns the value of field 'sequence'.
   * 
   * @return the value of field 'Sequence'.
   */
  public java.lang.String getSequence() {
    return this._sequence;
  }

  /**
   * Returns the value of field 'start'.
   * 
   * @return the value of field 'Start'.
   */
  public long getStart() {
    return this._start;
  }

  /**
   * Method hasEnd.
   * 
   * @return true if at least one End has been added
   */
  public boolean hasEnd() {
    return this._has_end;
  }

  /**
   * Method hasStart.
   * 
   * @return true if at least one Start has been added
   */
  public boolean hasStart() {
    return this._has_start;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    result = 37 * result + (int) (_start ^ (_start >>> 32));
    result = 37 * result + (int) (_end ^ (_end >>> 32));
    if (_sequence != null
        && !org.castor.util.CycleBreaker.startingToCycle(_sequence)) {
      result = 37 * result + _sequence.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_sequence);
    }
    if (_name != null && !org.castor.util.CycleBreaker.startingToCycle(_name)) {
      result = 37 * result + _name.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_name);
    }
    if (_description != null
        && !org.castor.util.CycleBreaker.startingToCycle(_description)) {
      result = 37 * result + _description.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_description);
    }
    if (_propertyList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_propertyList)) {
      result = 37 * result + _propertyList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_propertyList);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
     */
  public void removeAllProperty() {
    this._propertyList.clear();
  }

  /**
   * Method removeProperty.
   * 
   * @param vProperty
   * @return true if the object was removed from the collection.
   */
  public boolean removeProperty(
      final uk.ac.vamsas.objects.core.Property vProperty) {
    boolean removed = _propertyList.remove(vProperty);
    return removed;
  }

  /**
   * Method removePropertyAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Property removePropertyAt(final int index) {
    java.lang.Object obj = this._propertyList.remove(index);
    return (uk.ac.vamsas.objects.core.Property) obj;
  }

  /**
   * Sets the value of field 'description'.
   * 
   * @param description
   *          the value of field 'description'.
   */
  public void setDescription(final java.lang.String description) {
    this._description = description;
  }

  /**
   * Sets the value of field 'end'.
   * 
   * @param end
   *          the value of field 'end'.
   */
  public void setEnd(final long end) {
    this._end = end;
    this._has_end = true;
  }

  /**
   * Sets the value of field 'name'.
   * 
   * @param name
   *          the value of field 'name'.
   */
  public void setName(final java.lang.String name) {
    this._name = name;
  }

  /**
   * 
   * 
   * @param index
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setProperty(final int index,
      final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._propertyList.size()) {
      throw new IndexOutOfBoundsException("setProperty: Index value '" + index
          + "' not in range [0.." + (this._propertyList.size() - 1) + "]");
    }

    this._propertyList.set(index, vProperty);
  }

  /**
   * 
   * 
   * @param vPropertyArray
   */
  public void setProperty(
      final uk.ac.vamsas.objects.core.Property[] vPropertyArray) {
    // -- copy array
    _propertyList.clear();

    for (int i = 0; i < vPropertyArray.length; i++) {
      this._propertyList.add(vPropertyArray[i]);
    }
  }

  /**
   * Sets the value of '_propertyList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vPropertyList
   *          the Vector to copy.
   */
  public void setProperty(final java.util.Vector vPropertyList) {
    // copy vector
    this._propertyList.clear();

    this._propertyList.addAll(vPropertyList);
  }

  /**
   * Sets the value of '_propertyList' by setting it to the given Vector. No
   * type checking is performed.
   * 
   * @deprecated
   * 
   * @param propertyVector
   *          the Vector to set.
   */
  public void setPropertyAsReference(final java.util.Vector propertyVector) {
    this._propertyList = propertyVector;
  }

  /**
   * Sets the value of field 'sequence'.
   * 
   * @param sequence
   *          the value of field 'sequence'.
   */
  public void setSequence(final java.lang.String sequence) {
    this._sequence = sequence;
  }

  /**
   * Sets the value of field 'start'.
   * 
   * @param start
   *          the value of field 'start'.
   */
  public void setStart(final long start) {
    this._start = start;
    this._has_start = true;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.SequenceTyp
   */
  public static uk.ac.vamsas.objects.core.SequenceType unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.SequenceType) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.SequenceType.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
