/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class AnnotationElement.
 * 
 * @version $Revision$ $Date: 2007-06-28 14:51:44 +0100 (Thu, 28 Jun 2007)
 *          $
 */
public class AnnotationElement extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * position with respect to the coordinate frame defined by a rangeType
   * specification
   */
  private long _position;

  /**
   * keeps track of state for field: _position
   */
  private boolean _has_position;

  /**
   * true means the annotation element appears between the specified position
   * and the next
   */
  private boolean _after = false;

  /**
   * keeps track of state for field: _after
   */
  private boolean _has_after;

  /**
   * Primary Key for vamsas object referencing
   */
  private java.lang.String _id;

  /**
   * Free text at this position
   */
  private java.lang.String _description;

  /**
   * Discrete symbol - possibly graphically represented
   * 
   */
  private java.util.Vector _glyphList;

  /**
   * Ordered set of float values - an application may treat the values together
   * as a vector with common support for a set of annotation elements - but this
   * is, again, not validated so applications should deal gracefully with
   * varying numbers of dimensions
   * 
   */
  private java.util.Vector _valueList;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public AnnotationElement() {
    super();
    this._glyphList = new java.util.Vector();
    this._valueList = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vGlyph
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addGlyph(final uk.ac.vamsas.objects.core.Glyph vGlyph)
      throws java.lang.IndexOutOfBoundsException {
    this._glyphList.addElement(vGlyph);
  }

  /**
   * 
   * 
   * @param index
   * @param vGlyph
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addGlyph(final int index,
      final uk.ac.vamsas.objects.core.Glyph vGlyph)
      throws java.lang.IndexOutOfBoundsException {
    this._glyphList.add(index, vGlyph);
  }

  /**
   * 
   * 
   * @param vValue
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addValue(final float vValue)
      throws java.lang.IndexOutOfBoundsException {
    this._valueList.addElement(new java.lang.Float(vValue));
  }

  /**
   * 
   * 
   * @param index
   * @param vValue
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addValue(final int index, final float vValue)
      throws java.lang.IndexOutOfBoundsException {
    this._valueList.add(index, new java.lang.Float(vValue));
  }

  /**
     */
  public void deleteAfter() {
    this._has_after = false;
  }

  /**
     */
  public void deletePosition() {
    this._has_position = false;
  }

  /**
   * Method enumerateGlyph.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Glyph elements
   */
  public java.util.Enumeration enumerateGlyph() {
    return this._glyphList.elements();
  }

  /**
   * Method enumerateValue.
   * 
   * @return an Enumeration over all float elements
   */
  public java.util.Enumeration enumerateValue() {
    return this._valueList.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof AnnotationElement) {

      AnnotationElement temp = (AnnotationElement) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._position != temp._position)
        return false;
      if (this._has_position != temp._has_position)
        return false;
      if (this._after != temp._after)
        return false;
      if (this._has_after != temp._has_after)
        return false;
      if (this._id != null) {
        if (temp._id == null)
          return false;
        if (this._id != temp._id) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._id);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._id);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._id.equals(temp._id)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
          }
        }
      } else if (temp._id != null)
        return false;
      if (this._description != null) {
        if (temp._description == null)
          return false;
        if (this._description != temp._description) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._description);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._description);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._description);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._description);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._description.equals(temp._description)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._description);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._description);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._description);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._description);
          }
        }
      } else if (temp._description != null)
        return false;
      if (this._glyphList != null) {
        if (temp._glyphList == null)
          return false;
        if (this._glyphList != temp._glyphList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._glyphList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._glyphList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._glyphList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._glyphList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._glyphList.equals(temp._glyphList)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._glyphList);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._glyphList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._glyphList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._glyphList);
          }
        }
      } else if (temp._glyphList != null)
        return false;
      if (this._valueList != null) {
        if (temp._valueList == null)
          return false;
        if (this._valueList != temp._valueList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._valueList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._valueList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._valueList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._valueList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._valueList.equals(temp._valueList)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._valueList);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._valueList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._valueList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._valueList);
          }
        }
      } else if (temp._valueList != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'after'. The field 'after' has the following
   * description: true means the annotation element appears between the
   * specified position and the next
   * 
   * @return the value of field 'After'.
   */
  public boolean getAfter() {
    return this._after;
  }

  /**
   * Returns the value of field 'description'. The field 'description' has the
   * following description: Free text at this position
   * 
   * @return the value of field 'Description'.
   */
  public java.lang.String getDescription() {
    return this._description;
  }

  /**
   * Method getGlyph.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Glyph at the given index
   */
  public uk.ac.vamsas.objects.core.Glyph getGlyph(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._glyphList.size()) {
      throw new IndexOutOfBoundsException("getGlyph: Index value '" + index
          + "' not in range [0.." + (this._glyphList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Glyph) _glyphList.get(index);
  }

  /**
   * Method getGlyph.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Glyph[] getGlyph() {
    uk.ac.vamsas.objects.core.Glyph[] array = new uk.ac.vamsas.objects.core.Glyph[0];
    return (uk.ac.vamsas.objects.core.Glyph[]) this._glyphList.toArray(array);
  }

  /**
   * Method getGlyphAsReference.Returns a reference to '_glyphList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getGlyphAsReference() {
    return this._glyphList;
  }

  /**
   * Method getGlyphCount.
   * 
   * @return the size of this collection
   */
  public int getGlyphCount() {
    return this._glyphList.size();
  }

  /**
   * Returns the value of field 'id'. The field 'id' has the following
   * description: Primary Key for vamsas object referencing
   * 
   * @return the value of field 'Id'.
   */
  public java.lang.String getId() {
    return this._id;
  }

  /**
   * Returns the value of field 'position'. The field 'position' has the
   * following description: position with respect to the coordinate frame
   * defined by a rangeType specification
   * 
   * @return the value of field 'Position'.
   */
  public long getPosition() {
    return this._position;
  }

  /**
   * Method getValue.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the float at the given index
   */
  public float getValue(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._valueList.size()) {
      throw new IndexOutOfBoundsException("getValue: Index value '" + index
          + "' not in range [0.." + (this._valueList.size() - 1) + "]");
    }

    return ((java.lang.Float) _valueList.get(index)).floatValue();
  }

  /**
   * Method getValue.Returns the contents of the collection in an Array.
   * 
   * @return this collection as an Array
   */
  public float[] getValue() {
    int size = this._valueList.size();
    float[] array = new float[size];
    java.util.Iterator iter = _valueList.iterator();
    for (int index = 0; index < size; index++) {
      array[index] = ((java.lang.Float) iter.next()).floatValue();
    }
    return array;
  }

  /**
   * Method getValueAsReference.Returns a reference to '_valueList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getValueAsReference() {
    return this._valueList;
  }

  /**
   * Method getValueCount.
   * 
   * @return the size of this collection
   */
  public int getValueCount() {
    return this._valueList.size();
  }

  /**
   * Method hasAfter.
   * 
   * @return true if at least one After has been added
   */
  public boolean hasAfter() {
    return this._has_after;
  }

  /**
   * Method hasPosition.
   * 
   * @return true if at least one Position has been added
   */
  public boolean hasPosition() {
    return this._has_position;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    result = 37 * result + (int) (_position ^ (_position >>> 32));
    result = 37 * result + (_after ? 0 : 1);
    if (_id != null && !org.castor.util.CycleBreaker.startingToCycle(_id)) {
      result = 37 * result + _id.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_id);
    }
    if (_description != null
        && !org.castor.util.CycleBreaker.startingToCycle(_description)) {
      result = 37 * result + _description.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_description);
    }
    if (_glyphList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_glyphList)) {
      result = 37 * result + _glyphList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_glyphList);
    }
    if (_valueList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_valueList)) {
      result = 37 * result + _valueList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_valueList);
    }

    return result;
  }

  /**
   * Returns the value of field 'after'. The field 'after' has the following
   * description: true means the annotation element appears between the
   * specified position and the next
   * 
   * @return the value of field 'After'.
   */
  public boolean isAfter() {
    return this._after;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
     */
  public void removeAllGlyph() {
    this._glyphList.clear();
  }

  /**
     */
  public void removeAllValue() {
    this._valueList.clear();
  }

  /**
   * Method removeGlyph.
   * 
   * @param vGlyph
   * @return true if the object was removed from the collection.
   */
  public boolean removeGlyph(final uk.ac.vamsas.objects.core.Glyph vGlyph) {
    boolean removed = _glyphList.remove(vGlyph);
    return removed;
  }

  /**
   * Method removeGlyphAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Glyph removeGlyphAt(final int index) {
    java.lang.Object obj = this._glyphList.remove(index);
    return (uk.ac.vamsas.objects.core.Glyph) obj;
  }

  /**
   * Method removeValue.
   * 
   * @param vValue
   * @return true if the object was removed from the collection.
   */
  public boolean removeValue(final float vValue) {
    boolean removed = _valueList.remove(new java.lang.Float(vValue));
    return removed;
  }

  /**
   * Method removeValueAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public float removeValueAt(final int index) {
    java.lang.Object obj = this._valueList.remove(index);
    return ((java.lang.Float) obj).floatValue();
  }

  /**
   * Sets the value of field 'after'. The field 'after' has the following
   * description: true means the annotation element appears between the
   * specified position and the next
   * 
   * @param after
   *          the value of field 'after'.
   */
  public void setAfter(final boolean after) {
    this._after = after;
    this._has_after = true;
  }

  /**
   * Sets the value of field 'description'. The field 'description' has the
   * following description: Free text at this position
   * 
   * @param description
   *          the value of field 'description'.
   */
  public void setDescription(final java.lang.String description) {
    this._description = description;
  }

  /**
   * 
   * 
   * @param index
   * @param vGlyph
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setGlyph(final int index,
      final uk.ac.vamsas.objects.core.Glyph vGlyph)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._glyphList.size()) {
      throw new IndexOutOfBoundsException("setGlyph: Index value '" + index
          + "' not in range [0.." + (this._glyphList.size() - 1) + "]");
    }

    this._glyphList.set(index, vGlyph);
  }

  /**
   * 
   * 
   * @param vGlyphArray
   */
  public void setGlyph(final uk.ac.vamsas.objects.core.Glyph[] vGlyphArray) {
    // -- copy array
    _glyphList.clear();

    for (int i = 0; i < vGlyphArray.length; i++) {
      this._glyphList.add(vGlyphArray[i]);
    }
  }

  /**
   * Sets the value of '_glyphList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vGlyphList
   *          the Vector to copy.
   */
  public void setGlyph(final java.util.Vector vGlyphList) {
    // copy vector
    this._glyphList.clear();

    this._glyphList.addAll(vGlyphList);
  }

  /**
   * Sets the value of '_glyphList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param glyphVector
   *          the Vector to set.
   */
  public void setGlyphAsReference(final java.util.Vector glyphVector) {
    this._glyphList = glyphVector;
  }

  /**
   * Sets the value of field 'id'. The field 'id' has the following description:
   * Primary Key for vamsas object referencing
   * 
   * @param id
   *          the value of field 'id'.
   */
  public void setId(final java.lang.String id) {
    this._id = id;
  }

  /**
   * Sets the value of field 'position'. The field 'position' has the following
   * description: position with respect to the coordinate frame defined by a
   * rangeType specification
   * 
   * @param position
   *          the value of field 'position'.
   */
  public void setPosition(final long position) {
    this._position = position;
    this._has_position = true;
  }

  /**
   * 
   * 
   * @param index
   * @param vValue
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setValue(final int index, final float vValue)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._valueList.size()) {
      throw new IndexOutOfBoundsException("setValue: Index value '" + index
          + "' not in range [0.." + (this._valueList.size() - 1) + "]");
    }

    this._valueList.set(index, new java.lang.Float(vValue));
  }

  /**
   * 
   * 
   * @param vValueArray
   */
  public void setValue(final float[] vValueArray) {
    // -- copy array
    _valueList.clear();

    for (int i = 0; i < vValueArray.length; i++) {
      this._valueList.add(new java.lang.Float(vValueArray[i]));
    }
  }

  /**
   * Sets the value of '_valueList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vValueList
   *          the Vector to copy.
   */
  public void setValue(final java.util.Vector vValueList) {
    // copy vector
    this._valueList.clear();

    this._valueList.addAll(vValueList);
  }

  /**
   * Sets the value of '_valueList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param valueVector
   *          the Vector to set.
   */
  public void setValueAsReference(final java.util.Vector valueVector) {
    this._valueList = valueVector;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.AnnotationElement
   */
  public static uk.ac.vamsas.objects.core.AnnotationElement unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.AnnotationElement) Unmarshaller
        .unmarshal(uk.ac.vamsas.objects.core.AnnotationElement.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
