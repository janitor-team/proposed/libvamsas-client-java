/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * node identity and mapping data between tree representations and vamsas
 * document objects
 * 
 * @version $Revision$ $Date$
 */
public class Treenode extends uk.ac.vamsas.objects.core.NodeType implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * reference to one or more trees containing the node being described.
   */
  private java.util.Vector _treeId;

  /**
   * String uniquely identifying a particular node in the referenced tree
   * according to the format of the tree representation that is referenced.
   * 
   */
  private java.lang.String _nodespec;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public Treenode() {
    super();
    this._treeId = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vTreeId
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addTreeId(final java.lang.Object vTreeId)
      throws java.lang.IndexOutOfBoundsException {
    this._treeId.addElement(vTreeId);
  }

  /**
   * 
   * 
   * @param index
   * @param vTreeId
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addTreeId(final int index, final java.lang.Object vTreeId)
      throws java.lang.IndexOutOfBoundsException {
    this._treeId.add(index, vTreeId);
  }

  /**
   * Method enumerateTreeId.
   * 
   * @return an Enumeration over all java.lang.Object elements
   */
  public java.util.Enumeration enumerateTreeId() {
    return this._treeId.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof Treenode) {

      Treenode temp = (Treenode) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._treeId != null) {
        if (temp._treeId == null)
          return false;
        if (this._treeId != temp._treeId) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._treeId);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._treeId);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._treeId);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._treeId);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._treeId.equals(temp._treeId)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._treeId);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._treeId);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._treeId);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._treeId);
          }
        }
      } else if (temp._treeId != null)
        return false;
      if (this._nodespec != null) {
        if (temp._nodespec == null)
          return false;
        if (this._nodespec != temp._nodespec) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._nodespec);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._nodespec);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._nodespec);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._nodespec);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._nodespec.equals(temp._nodespec)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._nodespec);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._nodespec);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._nodespec);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._nodespec);
          }
        }
      } else if (temp._nodespec != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'nodespec'. The field 'nodespec' has the
   * following description: String uniquely identifying a particular node in the
   * referenced tree according to the format of the tree representation that is
   * referenced.
   * 
   * 
   * @return the value of field 'Nodespec'.
   */
  public java.lang.String getNodespec() {
    return this._nodespec;
  }

  /**
   * Method getTreeId.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the java.lang.Object at the given index
   */
  public java.lang.Object getTreeId(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._treeId.size()) {
      throw new IndexOutOfBoundsException("getTreeId: Index value '" + index
          + "' not in range [0.." + (this._treeId.size() - 1) + "]");
    }

    return _treeId.get(index);
  }

  /**
   * Method getTreeId.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public java.lang.Object[] getTreeId() {
    java.lang.Object[] array = new java.lang.Object[0];
    return (java.lang.Object[]) this._treeId.toArray(array);
  }

  /**
   * Method getTreeIdAsReference.Returns a reference to '_treeId'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getTreeIdAsReference() {
    return this._treeId;
  }

  /**
   * Method getTreeIdCount.
   * 
   * @return the size of this collection
   */
  public int getTreeIdCount() {
    return this._treeId.size();
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_treeId != null
        && !org.castor.util.CycleBreaker.startingToCycle(_treeId)) {
      result = 37 * result + _treeId.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_treeId);
    }
    if (_nodespec != null
        && !org.castor.util.CycleBreaker.startingToCycle(_nodespec)) {
      result = 37 * result + _nodespec.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_nodespec);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
     */
  public void removeAllTreeId() {
    this._treeId.clear();
  }

  /**
   * Method removeTreeId.
   * 
   * @param vTreeId
   * @return true if the object was removed from the collection.
   */
  public boolean removeTreeId(final java.lang.Object vTreeId) {
    boolean removed = _treeId.remove(vTreeId);
    return removed;
  }

  /**
   * Method removeTreeIdAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public java.lang.Object removeTreeIdAt(final int index) {
    java.lang.Object obj = this._treeId.remove(index);
    return obj;
  }

  /**
   * Sets the value of field 'nodespec'. The field 'nodespec' has the following
   * description: String uniquely identifying a particular node in the
   * referenced tree according to the format of the tree representation that is
   * referenced.
   * 
   * 
   * @param nodespec
   *          the value of field 'nodespec'.
   */
  public void setNodespec(final java.lang.String nodespec) {
    this._nodespec = nodespec;
  }

  /**
   * 
   * 
   * @param index
   * @param vTreeId
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setTreeId(final int index, final java.lang.Object vTreeId)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._treeId.size()) {
      throw new IndexOutOfBoundsException("setTreeId: Index value '" + index
          + "' not in range [0.." + (this._treeId.size() - 1) + "]");
    }

    this._treeId.set(index, vTreeId);
  }

  /**
   * 
   * 
   * @param vTreeIdArray
   */
  public void setTreeId(final java.lang.Object[] vTreeIdArray) {
    // -- copy array
    _treeId.clear();

    for (int i = 0; i < vTreeIdArray.length; i++) {
      this._treeId.add(vTreeIdArray[i]);
    }
  }

  /**
   * Sets the value of '_treeId' by copying the given Vector. All elements will
   * be checked for type safety.
   * 
   * @param vTreeIdList
   *          the Vector to copy.
   */
  public void setTreeId(final java.util.Vector vTreeIdList) {
    // copy vector
    this._treeId.clear();

    this._treeId.addAll(vTreeIdList);
  }

  /**
   * Sets the value of '_treeId' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param treeIdVector
   *          the Vector to set.
   */
  public void setTreeIdAsReference(final java.util.Vector treeIdVector) {
    this._treeId = treeIdVector;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.NodeType
   */
  public static uk.ac.vamsas.objects.core.NodeType unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.NodeType) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.Treenode.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
