/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class ApplicationData.
 * 
 * @version $Revision$ $Date: 2007-06-28 14:51:44 +0100 (Thu, 28 Jun 2007)
 *          $
 */
public class ApplicationData extends uk.ac.vamsas.objects.core.AppData
    implements java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Version string describing the application specific data storage version
   * used
   */
  private java.lang.String _version;

  /**
   * Canonical name of application
   */
  private java.lang.String _name;

  /**
   * Field _userList.
   */
  private java.util.Vector _userList;

  /**
   * Field _common.
   */
  private uk.ac.vamsas.objects.core.Common _common;

  /**
   * Field _instanceList.
   */
  private java.util.Vector _instanceList;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public ApplicationData() {
    super();
    this._userList = new java.util.Vector();
    this._instanceList = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vInstance
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addInstance(final uk.ac.vamsas.objects.core.Instance vInstance)
      throws java.lang.IndexOutOfBoundsException {
    this._instanceList.addElement(vInstance);
  }

  /**
   * 
   * 
   * @param index
   * @param vInstance
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addInstance(final int index,
      final uk.ac.vamsas.objects.core.Instance vInstance)
      throws java.lang.IndexOutOfBoundsException {
    this._instanceList.add(index, vInstance);
  }

  /**
   * 
   * 
   * @param vUser
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addUser(final uk.ac.vamsas.objects.core.User vUser)
      throws java.lang.IndexOutOfBoundsException {
    this._userList.addElement(vUser);
  }

  /**
   * 
   * 
   * @param index
   * @param vUser
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addUser(final int index,
      final uk.ac.vamsas.objects.core.User vUser)
      throws java.lang.IndexOutOfBoundsException {
    this._userList.add(index, vUser);
  }

  /**
   * Method enumerateInstance.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Instance elements
   */
  public java.util.Enumeration enumerateInstance() {
    return this._instanceList.elements();
  }

  /**
   * Method enumerateUser.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.User elements
   */
  public java.util.Enumeration enumerateUser() {
    return this._userList.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof ApplicationData) {

      ApplicationData temp = (ApplicationData) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._version != null) {
        if (temp._version == null)
          return false;
        if (this._version != temp._version) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._version);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._version);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._version);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._version);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._version.equals(temp._version)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._version);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._version);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._version);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._version);
          }
        }
      } else if (temp._version != null)
        return false;
      if (this._name != null) {
        if (temp._name == null)
          return false;
        if (this._name != temp._name) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._name);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._name);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._name);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._name);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._name.equals(temp._name)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._name);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._name);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._name);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._name);
          }
        }
      } else if (temp._name != null)
        return false;
      if (this._userList != null) {
        if (temp._userList == null)
          return false;
        if (this._userList != temp._userList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._userList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._userList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._userList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._userList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._userList.equals(temp._userList)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._userList);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._userList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._userList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._userList);
          }
        }
      } else if (temp._userList != null)
        return false;
      if (this._common != null) {
        if (temp._common == null)
          return false;
        if (this._common != temp._common) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._common);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._common);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._common);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._common);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._common.equals(temp._common)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._common);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._common);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._common);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._common);
          }
        }
      } else if (temp._common != null)
        return false;
      if (this._instanceList != null) {
        if (temp._instanceList == null)
          return false;
        if (this._instanceList != temp._instanceList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._instanceList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._instanceList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._instanceList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._instanceList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._instanceList.equals(temp._instanceList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._instanceList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._instanceList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._instanceList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._instanceList);
          }
        }
      } else if (temp._instanceList != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'common'.
   * 
   * @return the value of field 'Common'.
   */
  public uk.ac.vamsas.objects.core.Common getCommon() {
    return this._common;
  }

  /**
   * Method getInstance.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Instance at the given
   *         index
   */
  public uk.ac.vamsas.objects.core.Instance getInstance(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._instanceList.size()) {
      throw new IndexOutOfBoundsException("getInstance: Index value '" + index
          + "' not in range [0.." + (this._instanceList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Instance) _instanceList.get(index);
  }

  /**
   * Method getInstance.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Instance[] getInstance() {
    uk.ac.vamsas.objects.core.Instance[] array = new uk.ac.vamsas.objects.core.Instance[0];
    return (uk.ac.vamsas.objects.core.Instance[]) this._instanceList
        .toArray(array);
  }

  /**
   * Method getInstanceAsReference.Returns a reference to '_instanceList'. No
   * type checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getInstanceAsReference() {
    return this._instanceList;
  }

  /**
   * Method getInstanceCount.
   * 
   * @return the size of this collection
   */
  public int getInstanceCount() {
    return this._instanceList.size();
  }

  /**
   * Returns the value of field 'name'. The field 'name' has the following
   * description: Canonical name of application
   * 
   * @return the value of field 'Name'.
   */
  public java.lang.String getName() {
    return this._name;
  }

  /**
   * Method getUser.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.User at the given index
   */
  public uk.ac.vamsas.objects.core.User getUser(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._userList.size()) {
      throw new IndexOutOfBoundsException("getUser: Index value '" + index
          + "' not in range [0.." + (this._userList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.User) _userList.get(index);
  }

  /**
   * Method getUser.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.User[] getUser() {
    uk.ac.vamsas.objects.core.User[] array = new uk.ac.vamsas.objects.core.User[0];
    return (uk.ac.vamsas.objects.core.User[]) this._userList.toArray(array);
  }

  /**
   * Method getUserAsReference.Returns a reference to '_userList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getUserAsReference() {
    return this._userList;
  }

  /**
   * Method getUserCount.
   * 
   * @return the size of this collection
   */
  public int getUserCount() {
    return this._userList.size();
  }

  /**
   * Returns the value of field 'version'. The field 'version' has the following
   * description: Version string describing the application specific data
   * storage version used
   * 
   * @return the value of field 'Version'.
   */
  public java.lang.String getVersion() {
    return this._version;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_version != null
        && !org.castor.util.CycleBreaker.startingToCycle(_version)) {
      result = 37 * result + _version.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_version);
    }
    if (_name != null && !org.castor.util.CycleBreaker.startingToCycle(_name)) {
      result = 37 * result + _name.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_name);
    }
    if (_userList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_userList)) {
      result = 37 * result + _userList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_userList);
    }
    if (_common != null
        && !org.castor.util.CycleBreaker.startingToCycle(_common)) {
      result = 37 * result + _common.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_common);
    }
    if (_instanceList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_instanceList)) {
      result = 37 * result + _instanceList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_instanceList);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
     */
  public void removeAllInstance() {
    this._instanceList.clear();
  }

  /**
     */
  public void removeAllUser() {
    this._userList.clear();
  }

  /**
   * Method removeInstance.
   * 
   * @param vInstance
   * @return true if the object was removed from the collection.
   */
  public boolean removeInstance(
      final uk.ac.vamsas.objects.core.Instance vInstance) {
    boolean removed = _instanceList.remove(vInstance);
    return removed;
  }

  /**
   * Method removeInstanceAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Instance removeInstanceAt(final int index) {
    java.lang.Object obj = this._instanceList.remove(index);
    return (uk.ac.vamsas.objects.core.Instance) obj;
  }

  /**
   * Method removeUser.
   * 
   * @param vUser
   * @return true if the object was removed from the collection.
   */
  public boolean removeUser(final uk.ac.vamsas.objects.core.User vUser) {
    boolean removed = _userList.remove(vUser);
    return removed;
  }

  /**
   * Method removeUserAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.User removeUserAt(final int index) {
    java.lang.Object obj = this._userList.remove(index);
    return (uk.ac.vamsas.objects.core.User) obj;
  }

  /**
   * Sets the value of field 'common'.
   * 
   * @param common
   *          the value of field 'common'.
   */
  public void setCommon(final uk.ac.vamsas.objects.core.Common common) {
    this._common = common;
  }

  /**
   * 
   * 
   * @param index
   * @param vInstance
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setInstance(final int index,
      final uk.ac.vamsas.objects.core.Instance vInstance)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._instanceList.size()) {
      throw new IndexOutOfBoundsException("setInstance: Index value '" + index
          + "' not in range [0.." + (this._instanceList.size() - 1) + "]");
    }

    this._instanceList.set(index, vInstance);
  }

  /**
   * 
   * 
   * @param vInstanceArray
   */
  public void setInstance(
      final uk.ac.vamsas.objects.core.Instance[] vInstanceArray) {
    // -- copy array
    _instanceList.clear();

    for (int i = 0; i < vInstanceArray.length; i++) {
      this._instanceList.add(vInstanceArray[i]);
    }
  }

  /**
   * Sets the value of '_instanceList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vInstanceList
   *          the Vector to copy.
   */
  public void setInstance(final java.util.Vector vInstanceList) {
    // copy vector
    this._instanceList.clear();

    this._instanceList.addAll(vInstanceList);
  }

  /**
   * Sets the value of '_instanceList' by setting it to the given Vector. No
   * type checking is performed.
   * 
   * @deprecated
   * 
   * @param instanceVector
   *          the Vector to set.
   */
  public void setInstanceAsReference(final java.util.Vector instanceVector) {
    this._instanceList = instanceVector;
  }

  /**
   * Sets the value of field 'name'. The field 'name' has the following
   * description: Canonical name of application
   * 
   * @param name
   *          the value of field 'name'.
   */
  public void setName(final java.lang.String name) {
    this._name = name;
  }

  /**
   * 
   * 
   * @param index
   * @param vUser
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setUser(final int index,
      final uk.ac.vamsas.objects.core.User vUser)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._userList.size()) {
      throw new IndexOutOfBoundsException("setUser: Index value '" + index
          + "' not in range [0.." + (this._userList.size() - 1) + "]");
    }

    this._userList.set(index, vUser);
  }

  /**
   * 
   * 
   * @param vUserArray
   */
  public void setUser(final uk.ac.vamsas.objects.core.User[] vUserArray) {
    // -- copy array
    _userList.clear();

    for (int i = 0; i < vUserArray.length; i++) {
      this._userList.add(vUserArray[i]);
    }
  }

  /**
   * Sets the value of '_userList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vUserList
   *          the Vector to copy.
   */
  public void setUser(final java.util.Vector vUserList) {
    // copy vector
    this._userList.clear();

    this._userList.addAll(vUserList);
  }

  /**
   * Sets the value of '_userList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param userVector
   *          the Vector to set.
   */
  public void setUserAsReference(final java.util.Vector userVector) {
    this._userList = userVector;
  }

  /**
   * Sets the value of field 'version'. The field 'version' has the following
   * description: Version string describing the application specific data
   * storage version used
   * 
   * @param version
   *          the value of field 'version'.
   */
  public void setVersion(final java.lang.String version) {
    this._version = version;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.AppData
   */
  public static uk.ac.vamsas.objects.core.AppData unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.AppData) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.ApplicationData.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
