/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Alignment.
 * 
 * @version $Revision$ $Date: 2007-06-28 14:51:44 +0100 (Thu, 28 Jun 2007)
 *          $
 */
public class Alignment extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Field _gapChar.
   */
  private java.lang.String _gapChar;

  /**
   * Field _aligned.
   */
  private boolean _aligned;

  /**
   * keeps track of state for field: _aligned
   */
  private boolean _has_aligned;

  /**
   * Primary Key for vamsas object referencing
   * 
   */
  private java.lang.String _id;

  /**
   * Field _modifiable.
   */
  private java.lang.String _modifiable;

  /**
   * This is annotation over the coordinate frame defined by all the columns in
   * the alignment.
   * 
   */
  private java.util.Vector _alignmentAnnotationList;

  /**
   * Field _treeList.
   */
  private java.util.Vector _treeList;

  /**
   * Field _alignmentSequenceList.
   */
  private java.util.Vector _alignmentSequenceList;

  /**
   * typical properties may be additional alignment score objects
   */
  private java.util.Vector _propertyList;

  /**
   * Field _provenance.
   */
  private uk.ac.vamsas.objects.core.Provenance _provenance;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public Alignment() {
    super();
    this._alignmentAnnotationList = new java.util.Vector();
    this._treeList = new java.util.Vector();
    this._alignmentSequenceList = new java.util.Vector();
    this._propertyList = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vAlignmentAnnotation
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addAlignmentAnnotation(
      final uk.ac.vamsas.objects.core.AlignmentAnnotation vAlignmentAnnotation)
      throws java.lang.IndexOutOfBoundsException {
    this._alignmentAnnotationList.addElement(vAlignmentAnnotation);
  }

  /**
   * 
   * 
   * @param index
   * @param vAlignmentAnnotation
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addAlignmentAnnotation(final int index,
      final uk.ac.vamsas.objects.core.AlignmentAnnotation vAlignmentAnnotation)
      throws java.lang.IndexOutOfBoundsException {
    this._alignmentAnnotationList.add(index, vAlignmentAnnotation);
  }

  /**
   * 
   * 
   * @param vAlignmentSequence
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addAlignmentSequence(
      final uk.ac.vamsas.objects.core.AlignmentSequence vAlignmentSequence)
      throws java.lang.IndexOutOfBoundsException {
    this._alignmentSequenceList.addElement(vAlignmentSequence);
  }

  /**
   * 
   * 
   * @param index
   * @param vAlignmentSequence
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addAlignmentSequence(final int index,
      final uk.ac.vamsas.objects.core.AlignmentSequence vAlignmentSequence)
      throws java.lang.IndexOutOfBoundsException {
    this._alignmentSequenceList.add(index, vAlignmentSequence);
  }

  /**
   * 
   * 
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addProperty(final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    this._propertyList.addElement(vProperty);
  }

  /**
   * 
   * 
   * @param index
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addProperty(final int index,
      final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    this._propertyList.add(index, vProperty);
  }

  /**
   * 
   * 
   * @param vTree
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addTree(final uk.ac.vamsas.objects.core.Tree vTree)
      throws java.lang.IndexOutOfBoundsException {
    this._treeList.addElement(vTree);
  }

  /**
   * 
   * 
   * @param index
   * @param vTree
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addTree(final int index,
      final uk.ac.vamsas.objects.core.Tree vTree)
      throws java.lang.IndexOutOfBoundsException {
    this._treeList.add(index, vTree);
  }

  /**
     */
  public void deleteAligned() {
    this._has_aligned = false;
  }

  /**
   * Method enumerateAlignmentAnnotation.
   * 
   * @return an Enumeration over all
   *         uk.ac.vamsas.objects.core.AlignmentAnnotation elements
   */
  public java.util.Enumeration enumerateAlignmentAnnotation() {
    return this._alignmentAnnotationList.elements();
  }

  /**
   * Method enumerateAlignmentSequence.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.AlignmentSequence
   *         elements
   */
  public java.util.Enumeration enumerateAlignmentSequence() {
    return this._alignmentSequenceList.elements();
  }

  /**
   * Method enumerateProperty.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Property elements
   */
  public java.util.Enumeration enumerateProperty() {
    return this._propertyList.elements();
  }

  /**
   * Method enumerateTree.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Tree elements
   */
  public java.util.Enumeration enumerateTree() {
    return this._treeList.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof Alignment) {

      Alignment temp = (Alignment) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._gapChar != null) {
        if (temp._gapChar == null)
          return false;
        if (this._gapChar != temp._gapChar) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._gapChar);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._gapChar);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._gapChar);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._gapChar);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._gapChar.equals(temp._gapChar)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._gapChar);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._gapChar);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._gapChar);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._gapChar);
          }
        }
      } else if (temp._gapChar != null)
        return false;
      if (this._aligned != temp._aligned)
        return false;
      if (this._has_aligned != temp._has_aligned)
        return false;
      if (this._id != null) {
        if (temp._id == null)
          return false;
        if (this._id != temp._id) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._id);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._id);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._id.equals(temp._id)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
          }
        }
      } else if (temp._id != null)
        return false;
      if (this._modifiable != null) {
        if (temp._modifiable == null)
          return false;
        if (this._modifiable != temp._modifiable) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._modifiable);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._modifiable);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._modifiable);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._modifiable);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._modifiable.equals(temp._modifiable)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._modifiable);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._modifiable);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._modifiable);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._modifiable);
          }
        }
      } else if (temp._modifiable != null)
        return false;
      if (this._alignmentAnnotationList != null) {
        if (temp._alignmentAnnotationList == null)
          return false;
        if (this._alignmentAnnotationList != temp._alignmentAnnotationList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._alignmentAnnotationList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._alignmentAnnotationList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._alignmentAnnotationList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._alignmentAnnotationList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._alignmentAnnotationList
                .equals(temp._alignmentAnnotationList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._alignmentAnnotationList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._alignmentAnnotationList);
              return false;
            }
            org.castor.util.CycleBreaker
                .releaseCycleHandle(this._alignmentAnnotationList);
            org.castor.util.CycleBreaker
                .releaseCycleHandle(temp._alignmentAnnotationList);
          }
        }
      } else if (temp._alignmentAnnotationList != null)
        return false;
      if (this._treeList != null) {
        if (temp._treeList == null)
          return false;
        if (this._treeList != temp._treeList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._treeList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._treeList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._treeList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._treeList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._treeList.equals(temp._treeList)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._treeList);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._treeList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._treeList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._treeList);
          }
        }
      } else if (temp._treeList != null)
        return false;
      if (this._alignmentSequenceList != null) {
        if (temp._alignmentSequenceList == null)
          return false;
        if (this._alignmentSequenceList != temp._alignmentSequenceList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._alignmentSequenceList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._alignmentSequenceList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._alignmentSequenceList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._alignmentSequenceList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._alignmentSequenceList
                .equals(temp._alignmentSequenceList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._alignmentSequenceList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._alignmentSequenceList);
              return false;
            }
            org.castor.util.CycleBreaker
                .releaseCycleHandle(this._alignmentSequenceList);
            org.castor.util.CycleBreaker
                .releaseCycleHandle(temp._alignmentSequenceList);
          }
        }
      } else if (temp._alignmentSequenceList != null)
        return false;
      if (this._propertyList != null) {
        if (temp._propertyList == null)
          return false;
        if (this._propertyList != temp._propertyList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._propertyList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._propertyList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._propertyList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._propertyList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._propertyList.equals(temp._propertyList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._propertyList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._propertyList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._propertyList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._propertyList);
          }
        }
      } else if (temp._propertyList != null)
        return false;
      if (this._provenance != null) {
        if (temp._provenance == null)
          return false;
        if (this._provenance != temp._provenance) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._provenance);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._provenance);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._provenance);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._provenance);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._provenance.equals(temp._provenance)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._provenance);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._provenance);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._provenance);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._provenance);
          }
        }
      } else if (temp._provenance != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'aligned'.
   * 
   * @return the value of field 'Aligned'.
   */
  public boolean getAligned() {
    return this._aligned;
  }

  /**
   * Method getAlignmentAnnotation.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.AlignmentAnnotation at
   *         the given index
   */
  public uk.ac.vamsas.objects.core.AlignmentAnnotation getAlignmentAnnotation(
      final int index) throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._alignmentAnnotationList.size()) {
      throw new IndexOutOfBoundsException(
          "getAlignmentAnnotation: Index value '" + index
              + "' not in range [0.."
              + (this._alignmentAnnotationList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.AlignmentAnnotation) _alignmentAnnotationList
        .get(index);
  }

  /**
   * Method getAlignmentAnnotation.Returns the contents of the collection in an
   * Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.AlignmentAnnotation[] getAlignmentAnnotation() {
    uk.ac.vamsas.objects.core.AlignmentAnnotation[] array = new uk.ac.vamsas.objects.core.AlignmentAnnotation[0];
    return (uk.ac.vamsas.objects.core.AlignmentAnnotation[]) this._alignmentAnnotationList
        .toArray(array);
  }

  /**
   * Method getAlignmentAnnotationAsReference.Returns a reference to
   * '_alignmentAnnotationList'. No type checking is performed on any
   * modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getAlignmentAnnotationAsReference() {
    return this._alignmentAnnotationList;
  }

  /**
   * Method getAlignmentAnnotationCount.
   * 
   * @return the size of this collection
   */
  public int getAlignmentAnnotationCount() {
    return this._alignmentAnnotationList.size();
  }

  /**
   * Method getAlignmentSequence.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.AlignmentSequence at the
   *         given inde
   */
  public uk.ac.vamsas.objects.core.AlignmentSequence getAlignmentSequence(
      final int index) throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._alignmentSequenceList.size()) {
      throw new IndexOutOfBoundsException("getAlignmentSequence: Index value '"
          + index + "' not in range [0.."
          + (this._alignmentSequenceList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.AlignmentSequence) _alignmentSequenceList
        .get(index);
  }

  /**
   * Method getAlignmentSequence.Returns the contents of the collection in an
   * Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.AlignmentSequence[] getAlignmentSequence() {
    uk.ac.vamsas.objects.core.AlignmentSequence[] array = new uk.ac.vamsas.objects.core.AlignmentSequence[0];
    return (uk.ac.vamsas.objects.core.AlignmentSequence[]) this._alignmentSequenceList
        .toArray(array);
  }

  /**
   * Method getAlignmentSequenceAsReference.Returns a reference to
   * '_alignmentSequenceList'. No type checking is performed on any
   * modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getAlignmentSequenceAsReference() {
    return this._alignmentSequenceList;
  }

  /**
   * Method getAlignmentSequenceCount.
   * 
   * @return the size of this collection
   */
  public int getAlignmentSequenceCount() {
    return this._alignmentSequenceList.size();
  }

  /**
   * Returns the value of field 'gapChar'.
   * 
   * @return the value of field 'GapChar'.
   */
  public java.lang.String getGapChar() {
    return this._gapChar;
  }

  /**
   * Returns the value of field 'id'. The field 'id' has the following
   * description: Primary Key for vamsas object referencing
   * 
   * 
   * @return the value of field 'Id'.
   */
  public java.lang.String getId() {
    return this._id;
  }

  /**
   * Returns the value of field 'modifiable'.
   * 
   * @return the value of field 'Modifiable'.
   */
  public java.lang.String getModifiable() {
    return this._modifiable;
  }

  /**
   * Method getProperty.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Property at the given
   *         index
   */
  public uk.ac.vamsas.objects.core.Property getProperty(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._propertyList.size()) {
      throw new IndexOutOfBoundsException("getProperty: Index value '" + index
          + "' not in range [0.." + (this._propertyList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Property) _propertyList.get(index);
  }

  /**
   * Method getProperty.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Property[] getProperty() {
    uk.ac.vamsas.objects.core.Property[] array = new uk.ac.vamsas.objects.core.Property[0];
    return (uk.ac.vamsas.objects.core.Property[]) this._propertyList
        .toArray(array);
  }

  /**
   * Method getPropertyAsReference.Returns a reference to '_propertyList'. No
   * type checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getPropertyAsReference() {
    return this._propertyList;
  }

  /**
   * Method getPropertyCount.
   * 
   * @return the size of this collection
   */
  public int getPropertyCount() {
    return this._propertyList.size();
  }

  /**
   * Returns the value of field 'provenance'.
   * 
   * @return the value of field 'Provenance'.
   */
  public uk.ac.vamsas.objects.core.Provenance getProvenance() {
    return this._provenance;
  }

  /**
   * Method getTree.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Tree at the given index
   */
  public uk.ac.vamsas.objects.core.Tree getTree(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._treeList.size()) {
      throw new IndexOutOfBoundsException("getTree: Index value '" + index
          + "' not in range [0.." + (this._treeList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Tree) _treeList.get(index);
  }

  /**
   * Method getTree.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Tree[] getTree() {
    uk.ac.vamsas.objects.core.Tree[] array = new uk.ac.vamsas.objects.core.Tree[0];
    return (uk.ac.vamsas.objects.core.Tree[]) this._treeList.toArray(array);
  }

  /**
   * Method getTreeAsReference.Returns a reference to '_treeList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getTreeAsReference() {
    return this._treeList;
  }

  /**
   * Method getTreeCount.
   * 
   * @return the size of this collection
   */
  public int getTreeCount() {
    return this._treeList.size();
  }

  /**
   * Method hasAligned.
   * 
   * @return true if at least one Aligned has been added
   */
  public boolean hasAligned() {
    return this._has_aligned;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_gapChar != null
        && !org.castor.util.CycleBreaker.startingToCycle(_gapChar)) {
      result = 37 * result + _gapChar.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_gapChar);
    }
    result = 37 * result + (_aligned ? 0 : 1);
    if (_id != null && !org.castor.util.CycleBreaker.startingToCycle(_id)) {
      result = 37 * result + _id.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_id);
    }
    if (_modifiable != null
        && !org.castor.util.CycleBreaker.startingToCycle(_modifiable)) {
      result = 37 * result + _modifiable.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_modifiable);
    }
    if (_alignmentAnnotationList != null
        && !org.castor.util.CycleBreaker
            .startingToCycle(_alignmentAnnotationList)) {
      result = 37 * result + _alignmentAnnotationList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_alignmentAnnotationList);
    }
    if (_treeList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_treeList)) {
      result = 37 * result + _treeList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_treeList);
    }
    if (_alignmentSequenceList != null
        && !org.castor.util.CycleBreaker
            .startingToCycle(_alignmentSequenceList)) {
      result = 37 * result + _alignmentSequenceList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_alignmentSequenceList);
    }
    if (_propertyList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_propertyList)) {
      result = 37 * result + _propertyList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_propertyList);
    }
    if (_provenance != null
        && !org.castor.util.CycleBreaker.startingToCycle(_provenance)) {
      result = 37 * result + _provenance.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_provenance);
    }

    return result;
  }

  /**
   * Returns the value of field 'aligned'.
   * 
   * @return the value of field 'Aligned'.
   */
  public boolean isAligned() {
    return this._aligned;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
   * Method removeAlignmentAnnotation.
   * 
   * @param vAlignmentAnnotation
   * @return true if the object was removed from the collection.
   */
  public boolean removeAlignmentAnnotation(
      final uk.ac.vamsas.objects.core.AlignmentAnnotation vAlignmentAnnotation) {
    boolean removed = _alignmentAnnotationList.remove(vAlignmentAnnotation);
    return removed;
  }

  /**
   * Method removeAlignmentAnnotationAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.AlignmentAnnotation removeAlignmentAnnotationAt(
      final int index) {
    java.lang.Object obj = this._alignmentAnnotationList.remove(index);
    return (uk.ac.vamsas.objects.core.AlignmentAnnotation) obj;
  }

  /**
   * Method removeAlignmentSequence.
   * 
   * @param vAlignmentSequence
   * @return true if the object was removed from the collection.
   */
  public boolean removeAlignmentSequence(
      final uk.ac.vamsas.objects.core.AlignmentSequence vAlignmentSequence) {
    boolean removed = _alignmentSequenceList.remove(vAlignmentSequence);
    return removed;
  }

  /**
   * Method removeAlignmentSequenceAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.AlignmentSequence removeAlignmentSequenceAt(
      final int index) {
    java.lang.Object obj = this._alignmentSequenceList.remove(index);
    return (uk.ac.vamsas.objects.core.AlignmentSequence) obj;
  }

  /**
     */
  public void removeAllAlignmentAnnotation() {
    this._alignmentAnnotationList.clear();
  }

  /**
     */
  public void removeAllAlignmentSequence() {
    this._alignmentSequenceList.clear();
  }

  /**
     */
  public void removeAllProperty() {
    this._propertyList.clear();
  }

  /**
     */
  public void removeAllTree() {
    this._treeList.clear();
  }

  /**
   * Method removeProperty.
   * 
   * @param vProperty
   * @return true if the object was removed from the collection.
   */
  public boolean removeProperty(
      final uk.ac.vamsas.objects.core.Property vProperty) {
    boolean removed = _propertyList.remove(vProperty);
    return removed;
  }

  /**
   * Method removePropertyAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Property removePropertyAt(final int index) {
    java.lang.Object obj = this._propertyList.remove(index);
    return (uk.ac.vamsas.objects.core.Property) obj;
  }

  /**
   * Method removeTree.
   * 
   * @param vTree
   * @return true if the object was removed from the collection.
   */
  public boolean removeTree(final uk.ac.vamsas.objects.core.Tree vTree) {
    boolean removed = _treeList.remove(vTree);
    return removed;
  }

  /**
   * Method removeTreeAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Tree removeTreeAt(final int index) {
    java.lang.Object obj = this._treeList.remove(index);
    return (uk.ac.vamsas.objects.core.Tree) obj;
  }

  /**
   * Sets the value of field 'aligned'.
   * 
   * @param aligned
   *          the value of field 'aligned'.
   */
  public void setAligned(final boolean aligned) {
    this._aligned = aligned;
    this._has_aligned = true;
  }

  /**
   * 
   * 
   * @param index
   * @param vAlignmentAnnotation
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setAlignmentAnnotation(final int index,
      final uk.ac.vamsas.objects.core.AlignmentAnnotation vAlignmentAnnotation)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._alignmentAnnotationList.size()) {
      throw new IndexOutOfBoundsException(
          "setAlignmentAnnotation: Index value '" + index
              + "' not in range [0.."
              + (this._alignmentAnnotationList.size() - 1) + "]");
    }

    this._alignmentAnnotationList.set(index, vAlignmentAnnotation);
  }

  /**
   * 
   * 
   * @param vAlignmentAnnotationArray
   */
  public void setAlignmentAnnotation(
      final uk.ac.vamsas.objects.core.AlignmentAnnotation[] vAlignmentAnnotationArray) {
    // -- copy array
    _alignmentAnnotationList.clear();

    for (int i = 0; i < vAlignmentAnnotationArray.length; i++) {
      this._alignmentAnnotationList.add(vAlignmentAnnotationArray[i]);
    }
  }

  /**
   * Sets the value of '_alignmentAnnotationList' by copying the given Vector.
   * All elements will be checked for type safety.
   * 
   * @param vAlignmentAnnotationList
   *          the Vector to copy.
   */
  public void setAlignmentAnnotation(
      final java.util.Vector vAlignmentAnnotationList) {
    // copy vector
    this._alignmentAnnotationList.clear();

    this._alignmentAnnotationList.addAll(vAlignmentAnnotationList);
  }

  /**
   * Sets the value of '_alignmentAnnotationList' by setting it to the given
   * Vector. No type checking is performed.
   * 
   * @deprecated
   * 
   * @param alignmentAnnotationVector
   *          the Vector to set.
   */
  public void setAlignmentAnnotationAsReference(
      final java.util.Vector alignmentAnnotationVector) {
    this._alignmentAnnotationList = alignmentAnnotationVector;
  }

  /**
   * 
   * 
   * @param index
   * @param vAlignmentSequence
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setAlignmentSequence(final int index,
      final uk.ac.vamsas.objects.core.AlignmentSequence vAlignmentSequence)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._alignmentSequenceList.size()) {
      throw new IndexOutOfBoundsException("setAlignmentSequence: Index value '"
          + index + "' not in range [0.."
          + (this._alignmentSequenceList.size() - 1) + "]");
    }

    this._alignmentSequenceList.set(index, vAlignmentSequence);
  }

  /**
   * 
   * 
   * @param vAlignmentSequenceArray
   */
  public void setAlignmentSequence(
      final uk.ac.vamsas.objects.core.AlignmentSequence[] vAlignmentSequenceArray) {
    // -- copy array
    _alignmentSequenceList.clear();

    for (int i = 0; i < vAlignmentSequenceArray.length; i++) {
      this._alignmentSequenceList.add(vAlignmentSequenceArray[i]);
    }
  }

  /**
   * Sets the value of '_alignmentSequenceList' by copying the given Vector. All
   * elements will be checked for type safety.
   * 
   * @param vAlignmentSequenceList
   *          the Vector to copy.
   */
  public void setAlignmentSequence(final java.util.Vector vAlignmentSequenceList) {
    // copy vector
    this._alignmentSequenceList.clear();

    this._alignmentSequenceList.addAll(vAlignmentSequenceList);
  }

  /**
   * Sets the value of '_alignmentSequenceList' by setting it to the given
   * Vector. No type checking is performed.
   * 
   * @deprecated
   * 
   * @param alignmentSequenceVector
   *          the Vector to set.
   */
  public void setAlignmentSequenceAsReference(
      final java.util.Vector alignmentSequenceVector) {
    this._alignmentSequenceList = alignmentSequenceVector;
  }

  /**
   * Sets the value of field 'gapChar'.
   * 
   * @param gapChar
   *          the value of field 'gapChar'.
   */
  public void setGapChar(final java.lang.String gapChar) {
    this._gapChar = gapChar;
  }

  /**
   * Sets the value of field 'id'. The field 'id' has the following description:
   * Primary Key for vamsas object referencing
   * 
   * 
   * @param id
   *          the value of field 'id'.
   */
  public void setId(final java.lang.String id) {
    this._id = id;
  }

  /**
   * Sets the value of field 'modifiable'.
   * 
   * @param modifiable
   *          the value of field 'modifiable'.
   */
  public void setModifiable(final java.lang.String modifiable) {
    this._modifiable = modifiable;
  }

  /**
   * 
   * 
   * @param index
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setProperty(final int index,
      final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._propertyList.size()) {
      throw new IndexOutOfBoundsException("setProperty: Index value '" + index
          + "' not in range [0.." + (this._propertyList.size() - 1) + "]");
    }

    this._propertyList.set(index, vProperty);
  }

  /**
   * 
   * 
   * @param vPropertyArray
   */
  public void setProperty(
      final uk.ac.vamsas.objects.core.Property[] vPropertyArray) {
    // -- copy array
    _propertyList.clear();

    for (int i = 0; i < vPropertyArray.length; i++) {
      this._propertyList.add(vPropertyArray[i]);
    }
  }

  /**
   * Sets the value of '_propertyList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vPropertyList
   *          the Vector to copy.
   */
  public void setProperty(final java.util.Vector vPropertyList) {
    // copy vector
    this._propertyList.clear();

    this._propertyList.addAll(vPropertyList);
  }

  /**
   * Sets the value of '_propertyList' by setting it to the given Vector. No
   * type checking is performed.
   * 
   * @deprecated
   * 
   * @param propertyVector
   *          the Vector to set.
   */
  public void setPropertyAsReference(final java.util.Vector propertyVector) {
    this._propertyList = propertyVector;
  }

  /**
   * Sets the value of field 'provenance'.
   * 
   * @param provenance
   *          the value of field 'provenance'.
   */
  public void setProvenance(
      final uk.ac.vamsas.objects.core.Provenance provenance) {
    this._provenance = provenance;
  }

  /**
   * 
   * 
   * @param index
   * @param vTree
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setTree(final int index,
      final uk.ac.vamsas.objects.core.Tree vTree)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._treeList.size()) {
      throw new IndexOutOfBoundsException("setTree: Index value '" + index
          + "' not in range [0.." + (this._treeList.size() - 1) + "]");
    }

    this._treeList.set(index, vTree);
  }

  /**
   * 
   * 
   * @param vTreeArray
   */
  public void setTree(final uk.ac.vamsas.objects.core.Tree[] vTreeArray) {
    // -- copy array
    _treeList.clear();

    for (int i = 0; i < vTreeArray.length; i++) {
      this._treeList.add(vTreeArray[i]);
    }
  }

  /**
   * Sets the value of '_treeList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vTreeList
   *          the Vector to copy.
   */
  public void setTree(final java.util.Vector vTreeList) {
    // copy vector
    this._treeList.clear();

    this._treeList.addAll(vTreeList);
  }

  /**
   * Sets the value of '_treeList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param treeVector
   *          the Vector to set.
   */
  public void setTreeAsReference(final java.util.Vector treeVector) {
    this._treeList = treeVector;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.Alignment
   */
  public static uk.ac.vamsas.objects.core.Alignment unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.Alignment) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.Alignment.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
