/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Range.
 * 
 * @version $Revision$ $Date$
 */
public class Range extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Field _seqAStart.
   */
  private int _seqAStart;

  /**
   * keeps track of state for field: _seqAStart
   */
  private boolean _has_seqAStart;

  /**
   * Field _seqAEnd.
   */
  private int _seqAEnd;

  /**
   * keeps track of state for field: _seqAEnd
   */
  private boolean _has_seqAEnd;

  /**
   * Field _seqBStart.
   */
  private int _seqBStart;

  /**
   * keeps track of state for field: _seqBStart
   */
  private boolean _has_seqBStart;

  /**
   * Field _seqBEnd.
   */
  private int _seqBEnd;

  /**
   * keeps track of state for field: _seqBEnd
   */
  private boolean _has_seqBEnd;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public Range() {
    super();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
     */
  public void deleteSeqAEnd() {
    this._has_seqAEnd = false;
  }

  /**
     */
  public void deleteSeqAStart() {
    this._has_seqAStart = false;
  }

  /**
     */
  public void deleteSeqBEnd() {
    this._has_seqBEnd = false;
  }

  /**
     */
  public void deleteSeqBStart() {
    this._has_seqBStart = false;
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof Range) {

      Range temp = (Range) obj;
      if (this._seqAStart != temp._seqAStart)
        return false;
      if (this._has_seqAStart != temp._has_seqAStart)
        return false;
      if (this._seqAEnd != temp._seqAEnd)
        return false;
      if (this._has_seqAEnd != temp._has_seqAEnd)
        return false;
      if (this._seqBStart != temp._seqBStart)
        return false;
      if (this._has_seqBStart != temp._has_seqBStart)
        return false;
      if (this._seqBEnd != temp._seqBEnd)
        return false;
      if (this._has_seqBEnd != temp._has_seqBEnd)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'seqAEnd'.
   * 
   * @return the value of field 'SeqAEnd'.
   */
  public int getSeqAEnd() {
    return this._seqAEnd;
  }

  /**
   * Returns the value of field 'seqAStart'.
   * 
   * @return the value of field 'SeqAStart'.
   */
  public int getSeqAStart() {
    return this._seqAStart;
  }

  /**
   * Returns the value of field 'seqBEnd'.
   * 
   * @return the value of field 'SeqBEnd'.
   */
  public int getSeqBEnd() {
    return this._seqBEnd;
  }

  /**
   * Returns the value of field 'seqBStart'.
   * 
   * @return the value of field 'SeqBStart'.
   */
  public int getSeqBStart() {
    return this._seqBStart;
  }

  /**
   * Method hasSeqAEnd.
   * 
   * @return true if at least one SeqAEnd has been added
   */
  public boolean hasSeqAEnd() {
    return this._has_seqAEnd;
  }

  /**
   * Method hasSeqAStart.
   * 
   * @return true if at least one SeqAStart has been added
   */
  public boolean hasSeqAStart() {
    return this._has_seqAStart;
  }

  /**
   * Method hasSeqBEnd.
   * 
   * @return true if at least one SeqBEnd has been added
   */
  public boolean hasSeqBEnd() {
    return this._has_seqBEnd;
  }

  /**
   * Method hasSeqBStart.
   * 
   * @return true if at least one SeqBStart has been added
   */
  public boolean hasSeqBStart() {
    return this._has_seqBStart;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    result = 37 * result + _seqAStart;
    result = 37 * result + _seqAEnd;
    result = 37 * result + _seqBStart;
    result = 37 * result + _seqBEnd;

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
   * Sets the value of field 'seqAEnd'.
   * 
   * @param seqAEnd
   *          the value of field 'seqAEnd'.
   */
  public void setSeqAEnd(final int seqAEnd) {
    this._seqAEnd = seqAEnd;
    this._has_seqAEnd = true;
  }

  /**
   * Sets the value of field 'seqAStart'.
   * 
   * @param seqAStart
   *          the value of field 'seqAStart'.
   */
  public void setSeqAStart(final int seqAStart) {
    this._seqAStart = seqAStart;
    this._has_seqAStart = true;
  }

  /**
   * Sets the value of field 'seqBEnd'.
   * 
   * @param seqBEnd
   *          the value of field 'seqBEnd'.
   */
  public void setSeqBEnd(final int seqBEnd) {
    this._seqBEnd = seqBEnd;
    this._has_seqBEnd = true;
  }

  /**
   * Sets the value of field 'seqBStart'.
   * 
   * @param seqBStart
   *          the value of field 'seqBStart'.
   */
  public void setSeqBStart(final int seqBStart) {
    this._seqBStart = seqBStart;
    this._has_seqBStart = true;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.Range
   */
  public static uk.ac.vamsas.objects.core.Range unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.Range) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.Range.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
