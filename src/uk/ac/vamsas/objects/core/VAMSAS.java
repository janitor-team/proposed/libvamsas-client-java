/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * contains unassociated trees and a number of analysis sets
 * 
 * 
 * @version $Revision$ $Date: 2007-06-28 14:51:44 +0100 (Thu, 28 Jun 2007)
 *          $
 */
public class VAMSAS extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Primary Key for vamsas object referencing
   */
  private java.lang.String _id;

  /**
   * Field _modifiable.
   */
  private java.lang.String _modifiable;

  /**
   * Field _treeList.
   */
  private java.util.Vector _treeList;

  /**
   * Field _dataSetList.
   */
  private java.util.Vector _dataSetList;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public VAMSAS() {
    super();
    this._treeList = new java.util.Vector();
    this._dataSetList = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vDataSet
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addDataSet(final uk.ac.vamsas.objects.core.DataSet vDataSet)
      throws java.lang.IndexOutOfBoundsException {
    this._dataSetList.addElement(vDataSet);
  }

  /**
   * 
   * 
   * @param index
   * @param vDataSet
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addDataSet(final int index,
      final uk.ac.vamsas.objects.core.DataSet vDataSet)
      throws java.lang.IndexOutOfBoundsException {
    this._dataSetList.add(index, vDataSet);
  }

  /**
   * 
   * 
   * @param vTree
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addTree(final uk.ac.vamsas.objects.core.Tree vTree)
      throws java.lang.IndexOutOfBoundsException {
    this._treeList.addElement(vTree);
  }

  /**
   * 
   * 
   * @param index
   * @param vTree
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addTree(final int index,
      final uk.ac.vamsas.objects.core.Tree vTree)
      throws java.lang.IndexOutOfBoundsException {
    this._treeList.add(index, vTree);
  }

  /**
   * Method enumerateDataSet.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.DataSet elements
   */
  public java.util.Enumeration enumerateDataSet() {
    return this._dataSetList.elements();
  }

  /**
   * Method enumerateTree.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Tree elements
   */
  public java.util.Enumeration enumerateTree() {
    return this._treeList.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof VAMSAS) {

      VAMSAS temp = (VAMSAS) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._id != null) {
        if (temp._id == null)
          return false;
        if (this._id != temp._id) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._id);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._id);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._id.equals(temp._id)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
          }
        }
      } else if (temp._id != null)
        return false;
      if (this._modifiable != null) {
        if (temp._modifiable == null)
          return false;
        if (this._modifiable != temp._modifiable) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._modifiable);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._modifiable);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._modifiable);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._modifiable);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._modifiable.equals(temp._modifiable)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._modifiable);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._modifiable);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._modifiable);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._modifiable);
          }
        }
      } else if (temp._modifiable != null)
        return false;
      if (this._treeList != null) {
        if (temp._treeList == null)
          return false;
        if (this._treeList != temp._treeList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._treeList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._treeList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._treeList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._treeList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._treeList.equals(temp._treeList)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._treeList);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._treeList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._treeList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._treeList);
          }
        }
      } else if (temp._treeList != null)
        return false;
      if (this._dataSetList != null) {
        if (temp._dataSetList == null)
          return false;
        if (this._dataSetList != temp._dataSetList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._dataSetList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._dataSetList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._dataSetList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._dataSetList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._dataSetList.equals(temp._dataSetList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._dataSetList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._dataSetList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._dataSetList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._dataSetList);
          }
        }
      } else if (temp._dataSetList != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Method getDataSet.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.DataSet at the given
   *         index
   */
  public uk.ac.vamsas.objects.core.DataSet getDataSet(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._dataSetList.size()) {
      throw new IndexOutOfBoundsException("getDataSet: Index value '" + index
          + "' not in range [0.." + (this._dataSetList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.DataSet) _dataSetList.get(index);
  }

  /**
   * Method getDataSet.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.DataSet[] getDataSet() {
    uk.ac.vamsas.objects.core.DataSet[] array = new uk.ac.vamsas.objects.core.DataSet[0];
    return (uk.ac.vamsas.objects.core.DataSet[]) this._dataSetList
        .toArray(array);
  }

  /**
   * Method getDataSetAsReference.Returns a reference to '_dataSetList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getDataSetAsReference() {
    return this._dataSetList;
  }

  /**
   * Method getDataSetCount.
   * 
   * @return the size of this collection
   */
  public int getDataSetCount() {
    return this._dataSetList.size();
  }

  /**
   * Returns the value of field 'id'. The field 'id' has the following
   * description: Primary Key for vamsas object referencing
   * 
   * @return the value of field 'Id'.
   */
  public java.lang.String getId() {
    return this._id;
  }

  /**
   * Returns the value of field 'modifiable'.
   * 
   * @return the value of field 'Modifiable'.
   */
  public java.lang.String getModifiable() {
    return this._modifiable;
  }

  /**
   * Method getTree.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Tree at the given index
   */
  public uk.ac.vamsas.objects.core.Tree getTree(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._treeList.size()) {
      throw new IndexOutOfBoundsException("getTree: Index value '" + index
          + "' not in range [0.." + (this._treeList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Tree) _treeList.get(index);
  }

  /**
   * Method getTree.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Tree[] getTree() {
    uk.ac.vamsas.objects.core.Tree[] array = new uk.ac.vamsas.objects.core.Tree[0];
    return (uk.ac.vamsas.objects.core.Tree[]) this._treeList.toArray(array);
  }

  /**
   * Method getTreeAsReference.Returns a reference to '_treeList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getTreeAsReference() {
    return this._treeList;
  }

  /**
   * Method getTreeCount.
   * 
   * @return the size of this collection
   */
  public int getTreeCount() {
    return this._treeList.size();
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_id != null && !org.castor.util.CycleBreaker.startingToCycle(_id)) {
      result = 37 * result + _id.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_id);
    }
    if (_modifiable != null
        && !org.castor.util.CycleBreaker.startingToCycle(_modifiable)) {
      result = 37 * result + _modifiable.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_modifiable);
    }
    if (_treeList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_treeList)) {
      result = 37 * result + _treeList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_treeList);
    }
    if (_dataSetList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_dataSetList)) {
      result = 37 * result + _dataSetList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_dataSetList);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
     */
  public void removeAllDataSet() {
    this._dataSetList.clear();
  }

  /**
     */
  public void removeAllTree() {
    this._treeList.clear();
  }

  /**
   * Method removeDataSet.
   * 
   * @param vDataSet
   * @return true if the object was removed from the collection.
   */
  public boolean removeDataSet(final uk.ac.vamsas.objects.core.DataSet vDataSet) {
    boolean removed = _dataSetList.remove(vDataSet);
    return removed;
  }

  /**
   * Method removeDataSetAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.DataSet removeDataSetAt(final int index) {
    java.lang.Object obj = this._dataSetList.remove(index);
    return (uk.ac.vamsas.objects.core.DataSet) obj;
  }

  /**
   * Method removeTree.
   * 
   * @param vTree
   * @return true if the object was removed from the collection.
   */
  public boolean removeTree(final uk.ac.vamsas.objects.core.Tree vTree) {
    boolean removed = _treeList.remove(vTree);
    return removed;
  }

  /**
   * Method removeTreeAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Tree removeTreeAt(final int index) {
    java.lang.Object obj = this._treeList.remove(index);
    return (uk.ac.vamsas.objects.core.Tree) obj;
  }

  /**
   * 
   * 
   * @param index
   * @param vDataSet
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setDataSet(final int index,
      final uk.ac.vamsas.objects.core.DataSet vDataSet)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._dataSetList.size()) {
      throw new IndexOutOfBoundsException("setDataSet: Index value '" + index
          + "' not in range [0.." + (this._dataSetList.size() - 1) + "]");
    }

    this._dataSetList.set(index, vDataSet);
  }

  /**
   * 
   * 
   * @param vDataSetArray
   */
  public void setDataSet(final uk.ac.vamsas.objects.core.DataSet[] vDataSetArray) {
    // -- copy array
    _dataSetList.clear();

    for (int i = 0; i < vDataSetArray.length; i++) {
      this._dataSetList.add(vDataSetArray[i]);
    }
  }

  /**
   * Sets the value of '_dataSetList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vDataSetList
   *          the Vector to copy.
   */
  public void setDataSet(final java.util.Vector vDataSetList) {
    // copy vector
    this._dataSetList.clear();

    this._dataSetList.addAll(vDataSetList);
  }

  /**
   * Sets the value of '_dataSetList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param dataSetVector
   *          the Vector to set.
   */
  public void setDataSetAsReference(final java.util.Vector dataSetVector) {
    this._dataSetList = dataSetVector;
  }

  /**
   * Sets the value of field 'id'. The field 'id' has the following description:
   * Primary Key for vamsas object referencing
   * 
   * @param id
   *          the value of field 'id'.
   */
  public void setId(final java.lang.String id) {
    this._id = id;
  }

  /**
   * Sets the value of field 'modifiable'.
   * 
   * @param modifiable
   *          the value of field 'modifiable'.
   */
  public void setModifiable(final java.lang.String modifiable) {
    this._modifiable = modifiable;
  }

  /**
   * 
   * 
   * @param index
   * @param vTree
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setTree(final int index,
      final uk.ac.vamsas.objects.core.Tree vTree)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._treeList.size()) {
      throw new IndexOutOfBoundsException("setTree: Index value '" + index
          + "' not in range [0.." + (this._treeList.size() - 1) + "]");
    }

    this._treeList.set(index, vTree);
  }

  /**
   * 
   * 
   * @param vTreeArray
   */
  public void setTree(final uk.ac.vamsas.objects.core.Tree[] vTreeArray) {
    // -- copy array
    _treeList.clear();

    for (int i = 0; i < vTreeArray.length; i++) {
      this._treeList.add(vTreeArray[i]);
    }
  }

  /**
   * Sets the value of '_treeList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vTreeList
   *          the Vector to copy.
   */
  public void setTree(final java.util.Vector vTreeList) {
    // copy vector
    this._treeList.clear();

    this._treeList.addAll(vTreeList);
  }

  /**
   * Sets the value of '_treeList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param treeVector
   *          the Vector to set.
   */
  public void setTreeAsReference(final java.util.Vector treeVector) {
    this._treeList = treeVector;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.VAMSAS
   */
  public static uk.ac.vamsas.objects.core.VAMSAS unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.VAMSAS) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.VAMSAS.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
