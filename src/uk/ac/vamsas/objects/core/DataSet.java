/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class DataSet.
 * 
 * @version $Revision$ $Date: 2007-06-28 14:51:44 +0100 (Thu, 28 Jun 2007)
 *          $
 */
public class DataSet extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Primary Key for vamsas object referencing
   */
  private java.lang.String _id;

  /**
   * Field _sequenceList.
   */
  private java.util.Vector _sequenceList;

  /**
   * Field _sequenceMappingList.
   */
  private java.util.Vector _sequenceMappingList;

  /**
   * Field _dataSetAnnotationsList.
   */
  private java.util.Vector _dataSetAnnotationsList;

  /**
   * Field _alignmentList.
   */
  private java.util.Vector _alignmentList;

  /**
   * Field _treeList.
   */
  private java.util.Vector _treeList;

  /**
   * Field _provenance.
   */
  private uk.ac.vamsas.objects.core.Provenance _provenance;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public DataSet() {
    super();
    this._sequenceList = new java.util.Vector();
    this._sequenceMappingList = new java.util.Vector();
    this._dataSetAnnotationsList = new java.util.Vector();
    this._alignmentList = new java.util.Vector();
    this._treeList = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vAlignment
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addAlignment(final uk.ac.vamsas.objects.core.Alignment vAlignment)
      throws java.lang.IndexOutOfBoundsException {
    this._alignmentList.addElement(vAlignment);
  }

  /**
   * 
   * 
   * @param index
   * @param vAlignment
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addAlignment(final int index,
      final uk.ac.vamsas.objects.core.Alignment vAlignment)
      throws java.lang.IndexOutOfBoundsException {
    this._alignmentList.add(index, vAlignment);
  }

  /**
   * 
   * 
   * @param vDataSetAnnotations
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addDataSetAnnotations(
      final uk.ac.vamsas.objects.core.DataSetAnnotations vDataSetAnnotations)
      throws java.lang.IndexOutOfBoundsException {
    this._dataSetAnnotationsList.addElement(vDataSetAnnotations);
  }

  /**
   * 
   * 
   * @param index
   * @param vDataSetAnnotations
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addDataSetAnnotations(final int index,
      final uk.ac.vamsas.objects.core.DataSetAnnotations vDataSetAnnotations)
      throws java.lang.IndexOutOfBoundsException {
    this._dataSetAnnotationsList.add(index, vDataSetAnnotations);
  }

  /**
   * 
   * 
   * @param vSequence
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addSequence(final uk.ac.vamsas.objects.core.Sequence vSequence)
      throws java.lang.IndexOutOfBoundsException {
    this._sequenceList.addElement(vSequence);
  }

  /**
   * 
   * 
   * @param index
   * @param vSequence
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addSequence(final int index,
      final uk.ac.vamsas.objects.core.Sequence vSequence)
      throws java.lang.IndexOutOfBoundsException {
    this._sequenceList.add(index, vSequence);
  }

  /**
   * 
   * 
   * @param vSequenceMapping
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addSequenceMapping(
      final uk.ac.vamsas.objects.core.SequenceMapping vSequenceMapping)
      throws java.lang.IndexOutOfBoundsException {
    this._sequenceMappingList.addElement(vSequenceMapping);
  }

  /**
   * 
   * 
   * @param index
   * @param vSequenceMapping
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addSequenceMapping(final int index,
      final uk.ac.vamsas.objects.core.SequenceMapping vSequenceMapping)
      throws java.lang.IndexOutOfBoundsException {
    this._sequenceMappingList.add(index, vSequenceMapping);
  }

  /**
   * 
   * 
   * @param vTree
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addTree(final uk.ac.vamsas.objects.core.Tree vTree)
      throws java.lang.IndexOutOfBoundsException {
    this._treeList.addElement(vTree);
  }

  /**
   * 
   * 
   * @param index
   * @param vTree
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addTree(final int index,
      final uk.ac.vamsas.objects.core.Tree vTree)
      throws java.lang.IndexOutOfBoundsException {
    this._treeList.add(index, vTree);
  }

  /**
   * Method enumerateAlignment.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Alignment
   *         elements
   */
  public java.util.Enumeration enumerateAlignment() {
    return this._alignmentList.elements();
  }

  /**
   * Method enumerateDataSetAnnotations.
   * 
   * @return an Enumeration over all
   *         uk.ac.vamsas.objects.core.DataSetAnnotations elements
   */
  public java.util.Enumeration enumerateDataSetAnnotations() {
    return this._dataSetAnnotationsList.elements();
  }

  /**
   * Method enumerateSequence.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Sequence elements
   */
  public java.util.Enumeration enumerateSequence() {
    return this._sequenceList.elements();
  }

  /**
   * Method enumerateSequenceMapping.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.SequenceMapping
   *         elements
   */
  public java.util.Enumeration enumerateSequenceMapping() {
    return this._sequenceMappingList.elements();
  }

  /**
   * Method enumerateTree.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Tree elements
   */
  public java.util.Enumeration enumerateTree() {
    return this._treeList.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof DataSet) {

      DataSet temp = (DataSet) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._id != null) {
        if (temp._id == null)
          return false;
        if (this._id != temp._id) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._id);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._id);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._id.equals(temp._id)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
          }
        }
      } else if (temp._id != null)
        return false;
      if (this._sequenceList != null) {
        if (temp._sequenceList == null)
          return false;
        if (this._sequenceList != temp._sequenceList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._sequenceList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._sequenceList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._sequenceList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._sequenceList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._sequenceList.equals(temp._sequenceList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._sequenceList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._sequenceList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._sequenceList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._sequenceList);
          }
        }
      } else if (temp._sequenceList != null)
        return false;
      if (this._sequenceMappingList != null) {
        if (temp._sequenceMappingList == null)
          return false;
        if (this._sequenceMappingList != temp._sequenceMappingList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._sequenceMappingList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._sequenceMappingList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._sequenceMappingList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._sequenceMappingList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._sequenceMappingList.equals(temp._sequenceMappingList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._sequenceMappingList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._sequenceMappingList);
              return false;
            }
            org.castor.util.CycleBreaker
                .releaseCycleHandle(this._sequenceMappingList);
            org.castor.util.CycleBreaker
                .releaseCycleHandle(temp._sequenceMappingList);
          }
        }
      } else if (temp._sequenceMappingList != null)
        return false;
      if (this._dataSetAnnotationsList != null) {
        if (temp._dataSetAnnotationsList == null)
          return false;
        if (this._dataSetAnnotationsList != temp._dataSetAnnotationsList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._dataSetAnnotationsList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._dataSetAnnotationsList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._dataSetAnnotationsList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._dataSetAnnotationsList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._dataSetAnnotationsList
                .equals(temp._dataSetAnnotationsList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._dataSetAnnotationsList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._dataSetAnnotationsList);
              return false;
            }
            org.castor.util.CycleBreaker
                .releaseCycleHandle(this._dataSetAnnotationsList);
            org.castor.util.CycleBreaker
                .releaseCycleHandle(temp._dataSetAnnotationsList);
          }
        }
      } else if (temp._dataSetAnnotationsList != null)
        return false;
      if (this._alignmentList != null) {
        if (temp._alignmentList == null)
          return false;
        if (this._alignmentList != temp._alignmentList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._alignmentList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._alignmentList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._alignmentList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._alignmentList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._alignmentList.equals(temp._alignmentList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._alignmentList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._alignmentList);
              return false;
            }
            org.castor.util.CycleBreaker
                .releaseCycleHandle(this._alignmentList);
            org.castor.util.CycleBreaker
                .releaseCycleHandle(temp._alignmentList);
          }
        }
      } else if (temp._alignmentList != null)
        return false;
      if (this._treeList != null) {
        if (temp._treeList == null)
          return false;
        if (this._treeList != temp._treeList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._treeList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._treeList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._treeList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._treeList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._treeList.equals(temp._treeList)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._treeList);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._treeList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._treeList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._treeList);
          }
        }
      } else if (temp._treeList != null)
        return false;
      if (this._provenance != null) {
        if (temp._provenance == null)
          return false;
        if (this._provenance != temp._provenance) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._provenance);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._provenance);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._provenance);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._provenance);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._provenance.equals(temp._provenance)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._provenance);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._provenance);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._provenance);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._provenance);
          }
        }
      } else if (temp._provenance != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Method getAlignment.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Alignment at the given
   *         index
   */
  public uk.ac.vamsas.objects.core.Alignment getAlignment(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._alignmentList.size()) {
      throw new IndexOutOfBoundsException("getAlignment: Index value '" + index
          + "' not in range [0.." + (this._alignmentList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Alignment) _alignmentList.get(index);
  }

  /**
   * Method getAlignment.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Alignment[] getAlignment() {
    uk.ac.vamsas.objects.core.Alignment[] array = new uk.ac.vamsas.objects.core.Alignment[0];
    return (uk.ac.vamsas.objects.core.Alignment[]) this._alignmentList
        .toArray(array);
  }

  /**
   * Method getAlignmentAsReference.Returns a reference to '_alignmentList'. No
   * type checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getAlignmentAsReference() {
    return this._alignmentList;
  }

  /**
   * Method getAlignmentCount.
   * 
   * @return the size of this collection
   */
  public int getAlignmentCount() {
    return this._alignmentList.size();
  }

  /**
   * Method getDataSetAnnotations.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.DataSetAnnotations at
   *         the given index
   */
  public uk.ac.vamsas.objects.core.DataSetAnnotations getDataSetAnnotations(
      final int index) throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._dataSetAnnotationsList.size()) {
      throw new IndexOutOfBoundsException(
          "getDataSetAnnotations: Index value '" + index
              + "' not in range [0.."
              + (this._dataSetAnnotationsList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.DataSetAnnotations) _dataSetAnnotationsList
        .get(index);
  }

  /**
   * Method getDataSetAnnotations.Returns the contents of the collection in an
   * Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.DataSetAnnotations[] getDataSetAnnotations() {
    uk.ac.vamsas.objects.core.DataSetAnnotations[] array = new uk.ac.vamsas.objects.core.DataSetAnnotations[0];
    return (uk.ac.vamsas.objects.core.DataSetAnnotations[]) this._dataSetAnnotationsList
        .toArray(array);
  }

  /**
   * Method getDataSetAnnotationsAsReference.Returns a reference to
   * '_dataSetAnnotationsList'. No type checking is performed on any
   * modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getDataSetAnnotationsAsReference() {
    return this._dataSetAnnotationsList;
  }

  /**
   * Method getDataSetAnnotationsCount.
   * 
   * @return the size of this collection
   */
  public int getDataSetAnnotationsCount() {
    return this._dataSetAnnotationsList.size();
  }

  /**
   * Returns the value of field 'id'. The field 'id' has the following
   * description: Primary Key for vamsas object referencing
   * 
   * @return the value of field 'Id'.
   */
  public java.lang.String getId() {
    return this._id;
  }

  /**
   * Returns the value of field 'provenance'.
   * 
   * @return the value of field 'Provenance'.
   */
  public uk.ac.vamsas.objects.core.Provenance getProvenance() {
    return this._provenance;
  }

  /**
   * Method getSequence.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Sequence at the given
   *         index
   */
  public uk.ac.vamsas.objects.core.Sequence getSequence(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._sequenceList.size()) {
      throw new IndexOutOfBoundsException("getSequence: Index value '" + index
          + "' not in range [0.." + (this._sequenceList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Sequence) _sequenceList.get(index);
  }

  /**
   * Method getSequence.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Sequence[] getSequence() {
    uk.ac.vamsas.objects.core.Sequence[] array = new uk.ac.vamsas.objects.core.Sequence[0];
    return (uk.ac.vamsas.objects.core.Sequence[]) this._sequenceList
        .toArray(array);
  }

  /**
   * Method getSequenceAsReference.Returns a reference to '_sequenceList'. No
   * type checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getSequenceAsReference() {
    return this._sequenceList;
  }

  /**
   * Method getSequenceCount.
   * 
   * @return the size of this collection
   */
  public int getSequenceCount() {
    return this._sequenceList.size();
  }

  /**
   * Method getSequenceMapping.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.SequenceMapping at the
   *         given index
   */
  public uk.ac.vamsas.objects.core.SequenceMapping getSequenceMapping(
      final int index) throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._sequenceMappingList.size()) {
      throw new IndexOutOfBoundsException("getSequenceMapping: Index value '"
          + index + "' not in range [0.."
          + (this._sequenceMappingList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.SequenceMapping) _sequenceMappingList
        .get(index);
  }

  /**
   * Method getSequenceMapping.Returns the contents of the collection in an
   * Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.SequenceMapping[] getSequenceMapping() {
    uk.ac.vamsas.objects.core.SequenceMapping[] array = new uk.ac.vamsas.objects.core.SequenceMapping[0];
    return (uk.ac.vamsas.objects.core.SequenceMapping[]) this._sequenceMappingList
        .toArray(array);
  }

  /**
   * Method getSequenceMappingAsReference.Returns a reference to
   * '_sequenceMappingList'. No type checking is performed on any modifications
   * to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getSequenceMappingAsReference() {
    return this._sequenceMappingList;
  }

  /**
   * Method getSequenceMappingCount.
   * 
   * @return the size of this collection
   */
  public int getSequenceMappingCount() {
    return this._sequenceMappingList.size();
  }

  /**
   * Method getTree.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Tree at the given index
   */
  public uk.ac.vamsas.objects.core.Tree getTree(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._treeList.size()) {
      throw new IndexOutOfBoundsException("getTree: Index value '" + index
          + "' not in range [0.." + (this._treeList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Tree) _treeList.get(index);
  }

  /**
   * Method getTree.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Tree[] getTree() {
    uk.ac.vamsas.objects.core.Tree[] array = new uk.ac.vamsas.objects.core.Tree[0];
    return (uk.ac.vamsas.objects.core.Tree[]) this._treeList.toArray(array);
  }

  /**
   * Method getTreeAsReference.Returns a reference to '_treeList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getTreeAsReference() {
    return this._treeList;
  }

  /**
   * Method getTreeCount.
   * 
   * @return the size of this collection
   */
  public int getTreeCount() {
    return this._treeList.size();
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_id != null && !org.castor.util.CycleBreaker.startingToCycle(_id)) {
      result = 37 * result + _id.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_id);
    }
    if (_sequenceList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_sequenceList)) {
      result = 37 * result + _sequenceList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_sequenceList);
    }
    if (_sequenceMappingList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_sequenceMappingList)) {
      result = 37 * result + _sequenceMappingList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_sequenceMappingList);
    }
    if (_dataSetAnnotationsList != null
        && !org.castor.util.CycleBreaker
            .startingToCycle(_dataSetAnnotationsList)) {
      result = 37 * result + _dataSetAnnotationsList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_dataSetAnnotationsList);
    }
    if (_alignmentList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_alignmentList)) {
      result = 37 * result + _alignmentList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_alignmentList);
    }
    if (_treeList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_treeList)) {
      result = 37 * result + _treeList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_treeList);
    }
    if (_provenance != null
        && !org.castor.util.CycleBreaker.startingToCycle(_provenance)) {
      result = 37 * result + _provenance.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_provenance);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
   * Method removeAlignment.
   * 
   * @param vAlignment
   * @return true if the object was removed from the collection.
   */
  public boolean removeAlignment(
      final uk.ac.vamsas.objects.core.Alignment vAlignment) {
    boolean removed = _alignmentList.remove(vAlignment);
    return removed;
  }

  /**
   * Method removeAlignmentAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Alignment removeAlignmentAt(final int index) {
    java.lang.Object obj = this._alignmentList.remove(index);
    return (uk.ac.vamsas.objects.core.Alignment) obj;
  }

  /**
     */
  public void removeAllAlignment() {
    this._alignmentList.clear();
  }

  /**
     */
  public void removeAllDataSetAnnotations() {
    this._dataSetAnnotationsList.clear();
  }

  /**
     */
  public void removeAllSequence() {
    this._sequenceList.clear();
  }

  /**
     */
  public void removeAllSequenceMapping() {
    this._sequenceMappingList.clear();
  }

  /**
     */
  public void removeAllTree() {
    this._treeList.clear();
  }

  /**
   * Method removeDataSetAnnotations.
   * 
   * @param vDataSetAnnotations
   * @return true if the object was removed from the collection.
   */
  public boolean removeDataSetAnnotations(
      final uk.ac.vamsas.objects.core.DataSetAnnotations vDataSetAnnotations) {
    boolean removed = _dataSetAnnotationsList.remove(vDataSetAnnotations);
    return removed;
  }

  /**
   * Method removeDataSetAnnotationsAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.DataSetAnnotations removeDataSetAnnotationsAt(
      final int index) {
    java.lang.Object obj = this._dataSetAnnotationsList.remove(index);
    return (uk.ac.vamsas.objects.core.DataSetAnnotations) obj;
  }

  /**
   * Method removeSequence.
   * 
   * @param vSequence
   * @return true if the object was removed from the collection.
   */
  public boolean removeSequence(
      final uk.ac.vamsas.objects.core.Sequence vSequence) {
    boolean removed = _sequenceList.remove(vSequence);
    return removed;
  }

  /**
   * Method removeSequenceAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Sequence removeSequenceAt(final int index) {
    java.lang.Object obj = this._sequenceList.remove(index);
    return (uk.ac.vamsas.objects.core.Sequence) obj;
  }

  /**
   * Method removeSequenceMapping.
   * 
   * @param vSequenceMapping
   * @return true if the object was removed from the collection.
   */
  public boolean removeSequenceMapping(
      final uk.ac.vamsas.objects.core.SequenceMapping vSequenceMapping) {
    boolean removed = _sequenceMappingList.remove(vSequenceMapping);
    return removed;
  }

  /**
   * Method removeSequenceMappingAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.SequenceMapping removeSequenceMappingAt(
      final int index) {
    java.lang.Object obj = this._sequenceMappingList.remove(index);
    return (uk.ac.vamsas.objects.core.SequenceMapping) obj;
  }

  /**
   * Method removeTree.
   * 
   * @param vTree
   * @return true if the object was removed from the collection.
   */
  public boolean removeTree(final uk.ac.vamsas.objects.core.Tree vTree) {
    boolean removed = _treeList.remove(vTree);
    return removed;
  }

  /**
   * Method removeTreeAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Tree removeTreeAt(final int index) {
    java.lang.Object obj = this._treeList.remove(index);
    return (uk.ac.vamsas.objects.core.Tree) obj;
  }

  /**
   * 
   * 
   * @param index
   * @param vAlignment
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setAlignment(final int index,
      final uk.ac.vamsas.objects.core.Alignment vAlignment)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._alignmentList.size()) {
      throw new IndexOutOfBoundsException("setAlignment: Index value '" + index
          + "' not in range [0.." + (this._alignmentList.size() - 1) + "]");
    }

    this._alignmentList.set(index, vAlignment);
  }

  /**
   * 
   * 
   * @param vAlignmentArray
   */
  public void setAlignment(
      final uk.ac.vamsas.objects.core.Alignment[] vAlignmentArray) {
    // -- copy array
    _alignmentList.clear();

    for (int i = 0; i < vAlignmentArray.length; i++) {
      this._alignmentList.add(vAlignmentArray[i]);
    }
  }

  /**
   * Sets the value of '_alignmentList' by copying the given Vector. All
   * elements will be checked for type safety.
   * 
   * @param vAlignmentList
   *          the Vector to copy.
   */
  public void setAlignment(final java.util.Vector vAlignmentList) {
    // copy vector
    this._alignmentList.clear();

    this._alignmentList.addAll(vAlignmentList);
  }

  /**
   * Sets the value of '_alignmentList' by setting it to the given Vector. No
   * type checking is performed.
   * 
   * @deprecated
   * 
   * @param alignmentVector
   *          the Vector to set.
   */
  public void setAlignmentAsReference(final java.util.Vector alignmentVector) {
    this._alignmentList = alignmentVector;
  }

  /**
   * 
   * 
   * @param index
   * @param vDataSetAnnotations
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setDataSetAnnotations(final int index,
      final uk.ac.vamsas.objects.core.DataSetAnnotations vDataSetAnnotations)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._dataSetAnnotationsList.size()) {
      throw new IndexOutOfBoundsException(
          "setDataSetAnnotations: Index value '" + index
              + "' not in range [0.."
              + (this._dataSetAnnotationsList.size() - 1) + "]");
    }

    this._dataSetAnnotationsList.set(index, vDataSetAnnotations);
  }

  /**
   * 
   * 
   * @param vDataSetAnnotationsArray
   */
  public void setDataSetAnnotations(
      final uk.ac.vamsas.objects.core.DataSetAnnotations[] vDataSetAnnotationsArray) {
    // -- copy array
    _dataSetAnnotationsList.clear();

    for (int i = 0; i < vDataSetAnnotationsArray.length; i++) {
      this._dataSetAnnotationsList.add(vDataSetAnnotationsArray[i]);
    }
  }

  /**
   * Sets the value of '_dataSetAnnotationsList' by copying the given Vector.
   * All elements will be checked for type safety.
   * 
   * @param vDataSetAnnotationsList
   *          the Vector to copy.
   */
  public void setDataSetAnnotations(
      final java.util.Vector vDataSetAnnotationsList) {
    // copy vector
    this._dataSetAnnotationsList.clear();

    this._dataSetAnnotationsList.addAll(vDataSetAnnotationsList);
  }

  /**
   * Sets the value of '_dataSetAnnotationsList' by setting it to the given
   * Vector. No type checking is performed.
   * 
   * @deprecated
   * 
   * @param dataSetAnnotationsVector
   *          the Vector to set.
   */
  public void setDataSetAnnotationsAsReference(
      final java.util.Vector dataSetAnnotationsVector) {
    this._dataSetAnnotationsList = dataSetAnnotationsVector;
  }

  /**
   * Sets the value of field 'id'. The field 'id' has the following description:
   * Primary Key for vamsas object referencing
   * 
   * @param id
   *          the value of field 'id'.
   */
  public void setId(final java.lang.String id) {
    this._id = id;
  }

  /**
   * Sets the value of field 'provenance'.
   * 
   * @param provenance
   *          the value of field 'provenance'.
   */
  public void setProvenance(
      final uk.ac.vamsas.objects.core.Provenance provenance) {
    this._provenance = provenance;
  }

  /**
   * 
   * 
   * @param index
   * @param vSequence
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setSequence(final int index,
      final uk.ac.vamsas.objects.core.Sequence vSequence)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._sequenceList.size()) {
      throw new IndexOutOfBoundsException("setSequence: Index value '" + index
          + "' not in range [0.." + (this._sequenceList.size() - 1) + "]");
    }

    this._sequenceList.set(index, vSequence);
  }

  /**
   * 
   * 
   * @param vSequenceArray
   */
  public void setSequence(
      final uk.ac.vamsas.objects.core.Sequence[] vSequenceArray) {
    // -- copy array
    _sequenceList.clear();

    for (int i = 0; i < vSequenceArray.length; i++) {
      this._sequenceList.add(vSequenceArray[i]);
    }
  }

  /**
   * Sets the value of '_sequenceList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vSequenceList
   *          the Vector to copy.
   */
  public void setSequence(final java.util.Vector vSequenceList) {
    // copy vector
    this._sequenceList.clear();

    this._sequenceList.addAll(vSequenceList);
  }

  /**
   * Sets the value of '_sequenceList' by setting it to the given Vector. No
   * type checking is performed.
   * 
   * @deprecated
   * 
   * @param sequenceVector
   *          the Vector to set.
   */
  public void setSequenceAsReference(final java.util.Vector sequenceVector) {
    this._sequenceList = sequenceVector;
  }

  /**
   * 
   * 
   * @param index
   * @param vSequenceMapping
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setSequenceMapping(final int index,
      final uk.ac.vamsas.objects.core.SequenceMapping vSequenceMapping)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._sequenceMappingList.size()) {
      throw new IndexOutOfBoundsException("setSequenceMapping: Index value '"
          + index + "' not in range [0.."
          + (this._sequenceMappingList.size() - 1) + "]");
    }

    this._sequenceMappingList.set(index, vSequenceMapping);
  }

  /**
   * 
   * 
   * @param vSequenceMappingArray
   */
  public void setSequenceMapping(
      final uk.ac.vamsas.objects.core.SequenceMapping[] vSequenceMappingArray) {
    // -- copy array
    _sequenceMappingList.clear();

    for (int i = 0; i < vSequenceMappingArray.length; i++) {
      this._sequenceMappingList.add(vSequenceMappingArray[i]);
    }
  }

  /**
   * Sets the value of '_sequenceMappingList' by copying the given Vector. All
   * elements will be checked for type safety.
   * 
   * @param vSequenceMappingList
   *          the Vector to copy.
   */
  public void setSequenceMapping(final java.util.Vector vSequenceMappingList) {
    // copy vector
    this._sequenceMappingList.clear();

    this._sequenceMappingList.addAll(vSequenceMappingList);
  }

  /**
   * Sets the value of '_sequenceMappingList' by setting it to the given Vector.
   * No type checking is performed.
   * 
   * @deprecated
   * 
   * @param sequenceMappingVector
   *          the Vector to set.
   */
  public void setSequenceMappingAsReference(
      final java.util.Vector sequenceMappingVector) {
    this._sequenceMappingList = sequenceMappingVector;
  }

  /**
   * 
   * 
   * @param index
   * @param vTree
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setTree(final int index,
      final uk.ac.vamsas.objects.core.Tree vTree)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._treeList.size()) {
      throw new IndexOutOfBoundsException("setTree: Index value '" + index
          + "' not in range [0.." + (this._treeList.size() - 1) + "]");
    }

    this._treeList.set(index, vTree);
  }

  /**
   * 
   * 
   * @param vTreeArray
   */
  public void setTree(final uk.ac.vamsas.objects.core.Tree[] vTreeArray) {
    // -- copy array
    _treeList.clear();

    for (int i = 0; i < vTreeArray.length; i++) {
      this._treeList.add(vTreeArray[i]);
    }
  }

  /**
   * Sets the value of '_treeList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vTreeList
   *          the Vector to copy.
   */
  public void setTree(final java.util.Vector vTreeList) {
    // copy vector
    this._treeList.clear();

    this._treeList.addAll(vTreeList);
  }

  /**
   * Sets the value of '_treeList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param treeVector
   *          the Vector to set.
   */
  public void setTreeAsReference(final java.util.Vector treeVector) {
    this._treeList = treeVector;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.DataSet
   */
  public static uk.ac.vamsas.objects.core.DataSet unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.DataSet) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.DataSet.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
