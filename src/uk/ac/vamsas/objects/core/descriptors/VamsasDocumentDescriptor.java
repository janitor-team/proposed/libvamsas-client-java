/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core.descriptors;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import uk.ac.vamsas.objects.core.VamsasDocument;

/**
 * Class VamsasDocumentDescriptor.
 * 
 * @version $Revision$ $Date$
 */
public class VamsasDocumentDescriptor extends
    org.exolab.castor.xml.util.XMLClassDescriptorImpl {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Field _elementDefinition.
   */
  private boolean _elementDefinition;

  /**
   * Field _nsPrefix.
   */
  private java.lang.String _nsPrefix;

  /**
   * Field _nsURI.
   */
  private java.lang.String _nsURI;

  /**
   * Field _xmlName.
   */
  private java.lang.String _xmlName;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public VamsasDocumentDescriptor() {
    super();
    _nsURI = "http://www.vamsas.ac.uk/schemas/1.0/vamsasDocument";
    _xmlName = "VamsasDocument";
    _elementDefinition = true;

    // -- set grouping compositor
    setCompositorAsSequence();
    org.exolab.castor.xml.util.XMLFieldDescriptorImpl desc = null;
    org.exolab.castor.mapping.FieldHandler handler = null;
    org.exolab.castor.xml.FieldValidator fieldValidator = null;
    // -- initialize attribute descriptors

    // -- initialize element descriptors

    // -- _version
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        java.lang.String.class, "_version", "Version",
        org.exolab.castor.xml.NodeType.Element);
    desc.setImmutable(true);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        VamsasDocument target = (VamsasDocument) object;
        return target.getVersion();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          VamsasDocument target = (VamsasDocument) object;
          target.setVersion((java.lang.String) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return null;
      }
    };
    desc.setHandler(handler);
    desc.setNameSpaceURI("http://www.vamsas.ac.uk/schemas/1.0/vamsasDocument");
    desc.setRequired(true);
    desc.setMultivalued(false);
    addFieldDescriptor(desc);

    // -- validation code for: _version
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    fieldValidator.setMinOccurs(1);
    { // -- local scope
      org.exolab.castor.xml.validators.StringValidator typeValidator;
      typeValidator = new org.exolab.castor.xml.validators.StringValidator();
      fieldValidator.setValidator(typeValidator);
      typeValidator.setWhiteSpace("preserve");
    }
    desc.setValidator(fieldValidator);
    // -- _lockFile
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        uk.ac.vamsas.objects.core.LockFile.class, "_lockFile", "LockFile",
        org.exolab.castor.xml.NodeType.Element);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        VamsasDocument target = (VamsasDocument) object;
        return target.getLockFile();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          VamsasDocument target = (VamsasDocument) object;
          target.setLockFile((uk.ac.vamsas.objects.core.LockFile) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return new uk.ac.vamsas.objects.core.LockFile();
      }
    };
    desc.setHandler(handler);
    desc.setNameSpaceURI("http://www.vamsas.ac.uk/schemas/1.0/vamsasDocument");
    desc.setMultivalued(false);
    addFieldDescriptor(desc);

    // -- validation code for: _lockFile
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    { // -- local scope
    }
    desc.setValidator(fieldValidator);
    // -- _provenance
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        uk.ac.vamsas.objects.core.Provenance.class, "_provenance",
        "Provenance", org.exolab.castor.xml.NodeType.Element);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        VamsasDocument target = (VamsasDocument) object;
        return target.getProvenance();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          VamsasDocument target = (VamsasDocument) object;
          target.setProvenance((uk.ac.vamsas.objects.core.Provenance) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return new uk.ac.vamsas.objects.core.Provenance();
      }
    };
    desc.setHandler(handler);
    desc.setNameSpaceURI("http://www.vamsas.ac.uk/schemas/1.0/vamsasTypes");
    desc.setMultivalued(false);
    addFieldDescriptor(desc);

    // -- validation code for: _provenance
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    { // -- local scope
    }
    desc.setValidator(fieldValidator);
    // -- _VAMSASList
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        uk.ac.vamsas.objects.core.VAMSAS.class, "_VAMSASList", "VAMSAS",
        org.exolab.castor.xml.NodeType.Element);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        VamsasDocument target = (VamsasDocument) object;
        return target.getVAMSAS();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          VamsasDocument target = (VamsasDocument) object;
          target.addVAMSAS((uk.ac.vamsas.objects.core.VAMSAS) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public void resetValue(Object object) throws IllegalStateException,
          IllegalArgumentException {
        try {
          VamsasDocument target = (VamsasDocument) object;
          target.removeAllVAMSAS();
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return new uk.ac.vamsas.objects.core.VAMSAS();
      }
    };
    desc.setHandler(handler);
    desc.setNameSpaceURI("http://www.vamsas.ac.uk/schemas/1.0/vamsasTypes");
    desc.setRequired(true);
    desc.setMultivalued(true);
    addFieldDescriptor(desc);

    // -- validation code for: _VAMSASList
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    fieldValidator.setMinOccurs(1);
    { // -- local scope
    }
    desc.setValidator(fieldValidator);
    // -- _applicationDataList
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        uk.ac.vamsas.objects.core.ApplicationData.class,
        "_applicationDataList", "ApplicationData",
        org.exolab.castor.xml.NodeType.Element);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        VamsasDocument target = (VamsasDocument) object;
        return target.getApplicationData();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          VamsasDocument target = (VamsasDocument) object;
          target
              .addApplicationData((uk.ac.vamsas.objects.core.ApplicationData) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public void resetValue(Object object) throws IllegalStateException,
          IllegalArgumentException {
        try {
          VamsasDocument target = (VamsasDocument) object;
          target.removeAllApplicationData();
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return new uk.ac.vamsas.objects.core.ApplicationData();
      }
    };
    desc.setHandler(handler);
    desc.setNameSpaceURI("http://www.vamsas.ac.uk/schemas/1.0/vamsasTypes");
    desc.setMultivalued(true);
    addFieldDescriptor(desc);

    // -- validation code for: _applicationDataList
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    fieldValidator.setMinOccurs(0);
    { // -- local scope
    }
    desc.setValidator(fieldValidator);
    // -- _attachmentList
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        uk.ac.vamsas.objects.core.Attachment.class, "_attachmentList",
        "Attachment", org.exolab.castor.xml.NodeType.Element);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        VamsasDocument target = (VamsasDocument) object;
        return target.getAttachment();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          VamsasDocument target = (VamsasDocument) object;
          target.addAttachment((uk.ac.vamsas.objects.core.Attachment) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public void resetValue(Object object) throws IllegalStateException,
          IllegalArgumentException {
        try {
          VamsasDocument target = (VamsasDocument) object;
          target.removeAllAttachment();
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return new uk.ac.vamsas.objects.core.Attachment();
      }
    };
    desc.setHandler(handler);
    desc.setNameSpaceURI("http://www.vamsas.ac.uk/schemas/1.0/vamsasTypes");
    desc.setMultivalued(true);
    addFieldDescriptor(desc);

    // -- validation code for: _attachmentList
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    fieldValidator.setMinOccurs(0);
    { // -- local scope
    }
    desc.setValidator(fieldValidator);
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * Method getAccessMode.
   * 
   * @return the access mode specified for this class.
   */
  public org.exolab.castor.mapping.AccessMode getAccessMode() {
    return null;
  }

  /**
   * Method getIdentity.
   * 
   * @return the identity field, null if this class has no identity.
   */
  public org.exolab.castor.mapping.FieldDescriptor getIdentity() {
    return super.getIdentity();
  }

  /**
   * Method getJavaClass.
   * 
   * @return the Java class represented by this descriptor.
   */
  public java.lang.Class getJavaClass() {
    return uk.ac.vamsas.objects.core.VamsasDocument.class;
  }

  /**
   * Method getNameSpacePrefix.
   * 
   * @return the namespace prefix to use when marshaling as XML.
   */
  public java.lang.String getNameSpacePrefix() {
    return _nsPrefix;
  }

  /**
   * Method getNameSpaceURI.
   * 
   * @return the namespace URI used when marshaling and unmarshaling as XML.
   */
  public java.lang.String getNameSpaceURI() {
    return _nsURI;
  }

  /**
   * Method getValidator.
   * 
   * @return a specific validator for the class described by this
   *         ClassDescriptor.
   */
  public org.exolab.castor.xml.TypeValidator getValidator() {
    return this;
  }

  /**
   * Method getXMLName.
   * 
   * @return the XML Name for the Class being described.
   */
  public java.lang.String getXMLName() {
    return _xmlName;
  }

  /**
   * Method isElementDefinition.
   * 
   * @return true if XML schema definition of this Class is that of a global
   *         element or element with anonymous type definition.
   */
  public boolean isElementDefinition() {
    return _elementDefinition;
  }

}
