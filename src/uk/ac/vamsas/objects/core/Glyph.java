/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Discrete symbol - possibly graphically represented
 * 
 * 
 * @version $Revision$ $Date$
 */
public class Glyph extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * internal content storage
   */
  private java.lang.String _content = "";

  /**
   * specifies the symbol dictionary for this glyph - eg utf8 (the default),
   * aasecstr_3 or kd_hydrophobicity - the content is not validated so
   * applications must ensure they gracefully deal with invalid entries here
   */
  private java.lang.String _dict = "utf8";

  // ----------------/
  // - Constructors -/
  // ----------------/

  public Glyph() {
    super();
    setContent("");
    setDict("utf8");
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof Glyph) {

      Glyph temp = (Glyph) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._content != null) {
        if (temp._content == null)
          return false;
        if (this._content != temp._content) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._content);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._content);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._content);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._content);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._content.equals(temp._content)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._content);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._content);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._content);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._content);
          }
        }
      } else if (temp._content != null)
        return false;
      if (this._dict != null) {
        if (temp._dict == null)
          return false;
        if (this._dict != temp._dict) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._dict);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._dict);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._dict);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._dict);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._dict.equals(temp._dict)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._dict);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._dict);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._dict);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._dict);
          }
        }
      } else if (temp._dict != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'content'. The field 'content' has the following
   * description: internal content storage
   * 
   * @return the value of field 'Content'.
   */
  public java.lang.String getContent() {
    return this._content;
  }

  /**
   * Returns the value of field 'dict'. The field 'dict' has the following
   * description: specifies the symbol dictionary for this glyph - eg utf8 (the
   * default), aasecstr_3 or kd_hydrophobicity - the content is not validated so
   * applications must ensure they gracefully deal with invalid entries here
   * 
   * @return the value of field 'Dict'.
   */
  public java.lang.String getDict() {
    return this._dict;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_content != null
        && !org.castor.util.CycleBreaker.startingToCycle(_content)) {
      result = 37 * result + _content.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_content);
    }
    if (_dict != null && !org.castor.util.CycleBreaker.startingToCycle(_dict)) {
      result = 37 * result + _dict.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_dict);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
   * Sets the value of field 'content'. The field 'content' has the following
   * description: internal content storage
   * 
   * @param content
   *          the value of field 'content'.
   */
  public void setContent(final java.lang.String content) {
    this._content = content;
  }

  /**
   * Sets the value of field 'dict'. The field 'dict' has the following
   * description: specifies the symbol dictionary for this glyph - eg utf8 (the
   * default), aasecstr_3 or kd_hydrophobicity - the content is not validated so
   * applications must ensure they gracefully deal with invalid entries here
   * 
   * @param dict
   *          the value of field 'dict'.
   */
  public void setDict(final java.lang.String dict) {
    this._dict = dict;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.Glyph
   */
  public static uk.ac.vamsas.objects.core.Glyph unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.Glyph) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.Glyph.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
