/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class NodeType.
 * 
 * @version $Revision$ $Date$
 */
public class NodeType extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Primary Key for vamsas object referencing
   */
  private java.lang.String _id;

  /**
   * Field _modifiable.
   */
  private java.lang.String _modifiable;

  /**
   * Short name for this node
   */
  private java.lang.String _name;

  /**
   * Descriptive text for this node
   */
  private java.lang.String _description;

  /**
   * Direct associations between this node and any vamsas objects
   */
  private java.util.Vector _vrefList;

  /**
   * Field _propertyList.
   */
  private java.util.Vector _propertyList;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public NodeType() {
    super();
    this._vrefList = new java.util.Vector();
    this._propertyList = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addProperty(final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    this._propertyList.addElement(vProperty);
  }

  /**
   * 
   * 
   * @param index
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addProperty(final int index,
      final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    this._propertyList.add(index, vProperty);
  }

  /**
   * 
   * 
   * @param vVref
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addVref(final uk.ac.vamsas.objects.core.Vref vVref)
      throws java.lang.IndexOutOfBoundsException {
    this._vrefList.addElement(vVref);
  }

  /**
   * 
   * 
   * @param index
   * @param vVref
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addVref(final int index,
      final uk.ac.vamsas.objects.core.Vref vVref)
      throws java.lang.IndexOutOfBoundsException {
    this._vrefList.add(index, vVref);
  }

  /**
   * Method enumerateProperty.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Property elements
   */
  public java.util.Enumeration enumerateProperty() {
    return this._propertyList.elements();
  }

  /**
   * Method enumerateVref.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Vref elements
   */
  public java.util.Enumeration enumerateVref() {
    return this._vrefList.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof NodeType) {

      NodeType temp = (NodeType) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._id != null) {
        if (temp._id == null)
          return false;
        if (this._id != temp._id) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._id);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._id);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._id.equals(temp._id)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
          }
        }
      } else if (temp._id != null)
        return false;
      if (this._modifiable != null) {
        if (temp._modifiable == null)
          return false;
        if (this._modifiable != temp._modifiable) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._modifiable);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._modifiable);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._modifiable);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._modifiable);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._modifiable.equals(temp._modifiable)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._modifiable);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._modifiable);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._modifiable);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._modifiable);
          }
        }
      } else if (temp._modifiable != null)
        return false;
      if (this._name != null) {
        if (temp._name == null)
          return false;
        if (this._name != temp._name) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._name);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._name);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._name);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._name);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._name.equals(temp._name)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._name);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._name);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._name);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._name);
          }
        }
      } else if (temp._name != null)
        return false;
      if (this._description != null) {
        if (temp._description == null)
          return false;
        if (this._description != temp._description) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._description);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._description);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._description);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._description);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._description.equals(temp._description)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._description);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._description);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._description);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._description);
          }
        }
      } else if (temp._description != null)
        return false;
      if (this._vrefList != null) {
        if (temp._vrefList == null)
          return false;
        if (this._vrefList != temp._vrefList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._vrefList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._vrefList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._vrefList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._vrefList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._vrefList.equals(temp._vrefList)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._vrefList);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._vrefList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._vrefList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._vrefList);
          }
        }
      } else if (temp._vrefList != null)
        return false;
      if (this._propertyList != null) {
        if (temp._propertyList == null)
          return false;
        if (this._propertyList != temp._propertyList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._propertyList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._propertyList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._propertyList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._propertyList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._propertyList.equals(temp._propertyList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._propertyList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._propertyList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._propertyList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._propertyList);
          }
        }
      } else if (temp._propertyList != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'description'. The field 'description' has the
   * following description: Descriptive text for this node
   * 
   * @return the value of field 'Description'.
   */
  public java.lang.String getDescription() {
    return this._description;
  }

  /**
   * Returns the value of field 'id'. The field 'id' has the following
   * description: Primary Key for vamsas object referencing
   * 
   * @return the value of field 'Id'.
   */
  public java.lang.String getId() {
    return this._id;
  }

  /**
   * Returns the value of field 'modifiable'.
   * 
   * @return the value of field 'Modifiable'.
   */
  public java.lang.String getModifiable() {
    return this._modifiable;
  }

  /**
   * Returns the value of field 'name'. The field 'name' has the following
   * description: Short name for this node
   * 
   * @return the value of field 'Name'.
   */
  public java.lang.String getName() {
    return this._name;
  }

  /**
   * Method getProperty.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Property at the given
   *         index
   */
  public uk.ac.vamsas.objects.core.Property getProperty(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._propertyList.size()) {
      throw new IndexOutOfBoundsException("getProperty: Index value '" + index
          + "' not in range [0.." + (this._propertyList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Property) _propertyList.get(index);
  }

  /**
   * Method getProperty.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Property[] getProperty() {
    uk.ac.vamsas.objects.core.Property[] array = new uk.ac.vamsas.objects.core.Property[0];
    return (uk.ac.vamsas.objects.core.Property[]) this._propertyList
        .toArray(array);
  }

  /**
   * Method getPropertyAsReference.Returns a reference to '_propertyList'. No
   * type checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getPropertyAsReference() {
    return this._propertyList;
  }

  /**
   * Method getPropertyCount.
   * 
   * @return the size of this collection
   */
  public int getPropertyCount() {
    return this._propertyList.size();
  }

  /**
   * Method getVref.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Vref at the given index
   */
  public uk.ac.vamsas.objects.core.Vref getVref(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._vrefList.size()) {
      throw new IndexOutOfBoundsException("getVref: Index value '" + index
          + "' not in range [0.." + (this._vrefList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Vref) _vrefList.get(index);
  }

  /**
   * Method getVref.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Vref[] getVref() {
    uk.ac.vamsas.objects.core.Vref[] array = new uk.ac.vamsas.objects.core.Vref[0];
    return (uk.ac.vamsas.objects.core.Vref[]) this._vrefList.toArray(array);
  }

  /**
   * Method getVrefAsReference.Returns a reference to '_vrefList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getVrefAsReference() {
    return this._vrefList;
  }

  /**
   * Method getVrefCount.
   * 
   * @return the size of this collection
   */
  public int getVrefCount() {
    return this._vrefList.size();
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_id != null && !org.castor.util.CycleBreaker.startingToCycle(_id)) {
      result = 37 * result + _id.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_id);
    }
    if (_modifiable != null
        && !org.castor.util.CycleBreaker.startingToCycle(_modifiable)) {
      result = 37 * result + _modifiable.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_modifiable);
    }
    if (_name != null && !org.castor.util.CycleBreaker.startingToCycle(_name)) {
      result = 37 * result + _name.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_name);
    }
    if (_description != null
        && !org.castor.util.CycleBreaker.startingToCycle(_description)) {
      result = 37 * result + _description.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_description);
    }
    if (_vrefList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_vrefList)) {
      result = 37 * result + _vrefList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_vrefList);
    }
    if (_propertyList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_propertyList)) {
      result = 37 * result + _propertyList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_propertyList);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
     */
  public void removeAllProperty() {
    this._propertyList.clear();
  }

  /**
     */
  public void removeAllVref() {
    this._vrefList.clear();
  }

  /**
   * Method removeProperty.
   * 
   * @param vProperty
   * @return true if the object was removed from the collection.
   */
  public boolean removeProperty(
      final uk.ac.vamsas.objects.core.Property vProperty) {
    boolean removed = _propertyList.remove(vProperty);
    return removed;
  }

  /**
   * Method removePropertyAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Property removePropertyAt(final int index) {
    java.lang.Object obj = this._propertyList.remove(index);
    return (uk.ac.vamsas.objects.core.Property) obj;
  }

  /**
   * Method removeVref.
   * 
   * @param vVref
   * @return true if the object was removed from the collection.
   */
  public boolean removeVref(final uk.ac.vamsas.objects.core.Vref vVref) {
    boolean removed = _vrefList.remove(vVref);
    return removed;
  }

  /**
   * Method removeVrefAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Vref removeVrefAt(final int index) {
    java.lang.Object obj = this._vrefList.remove(index);
    return (uk.ac.vamsas.objects.core.Vref) obj;
  }

  /**
   * Sets the value of field 'description'. The field 'description' has the
   * following description: Descriptive text for this node
   * 
   * @param description
   *          the value of field 'description'.
   */
  public void setDescription(final java.lang.String description) {
    this._description = description;
  }

  /**
   * Sets the value of field 'id'. The field 'id' has the following description:
   * Primary Key for vamsas object referencing
   * 
   * @param id
   *          the value of field 'id'.
   */
  public void setId(final java.lang.String id) {
    this._id = id;
  }

  /**
   * Sets the value of field 'modifiable'.
   * 
   * @param modifiable
   *          the value of field 'modifiable'.
   */
  public void setModifiable(final java.lang.String modifiable) {
    this._modifiable = modifiable;
  }

  /**
   * Sets the value of field 'name'. The field 'name' has the following
   * description: Short name for this node
   * 
   * @param name
   *          the value of field 'name'.
   */
  public void setName(final java.lang.String name) {
    this._name = name;
  }

  /**
   * 
   * 
   * @param index
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setProperty(final int index,
      final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._propertyList.size()) {
      throw new IndexOutOfBoundsException("setProperty: Index value '" + index
          + "' not in range [0.." + (this._propertyList.size() - 1) + "]");
    }

    this._propertyList.set(index, vProperty);
  }

  /**
   * 
   * 
   * @param vPropertyArray
   */
  public void setProperty(
      final uk.ac.vamsas.objects.core.Property[] vPropertyArray) {
    // -- copy array
    _propertyList.clear();

    for (int i = 0; i < vPropertyArray.length; i++) {
      this._propertyList.add(vPropertyArray[i]);
    }
  }

  /**
   * Sets the value of '_propertyList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vPropertyList
   *          the Vector to copy.
   */
  public void setProperty(final java.util.Vector vPropertyList) {
    // copy vector
    this._propertyList.clear();

    this._propertyList.addAll(vPropertyList);
  }

  /**
   * Sets the value of '_propertyList' by setting it to the given Vector. No
   * type checking is performed.
   * 
   * @deprecated
   * 
   * @param propertyVector
   *          the Vector to set.
   */
  public void setPropertyAsReference(final java.util.Vector propertyVector) {
    this._propertyList = propertyVector;
  }

  /**
   * 
   * 
   * @param index
   * @param vVref
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setVref(final int index,
      final uk.ac.vamsas.objects.core.Vref vVref)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._vrefList.size()) {
      throw new IndexOutOfBoundsException("setVref: Index value '" + index
          + "' not in range [0.." + (this._vrefList.size() - 1) + "]");
    }

    this._vrefList.set(index, vVref);
  }

  /**
   * 
   * 
   * @param vVrefArray
   */
  public void setVref(final uk.ac.vamsas.objects.core.Vref[] vVrefArray) {
    // -- copy array
    _vrefList.clear();

    for (int i = 0; i < vVrefArray.length; i++) {
      this._vrefList.add(vVrefArray[i]);
    }
  }

  /**
   * Sets the value of '_vrefList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vVrefList
   *          the Vector to copy.
   */
  public void setVref(final java.util.Vector vVrefList) {
    // copy vector
    this._vrefList.clear();

    this._vrefList.addAll(vVrefList);
  }

  /**
   * Sets the value of '_vrefList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param vrefVector
   *          the Vector to set.
   */
  public void setVrefAsReference(final java.util.Vector vrefVector) {
    this._vrefList = vrefVector;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.NodeType
   */
  public static uk.ac.vamsas.objects.core.NodeType unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.NodeType) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.NodeType.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
