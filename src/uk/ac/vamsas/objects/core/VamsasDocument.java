/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class VamsasDocument.
 * 
 * @version $Revision$ $Date: 2007-06-28 14:51:44 +0100 (Thu, 28 Jun 2007)
 *          $
 */
public class VamsasDocument extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Vamsas Document Version Number
   */
  private java.lang.String _version;

  /**
   * Field _lockFile.
   */
  private uk.ac.vamsas.objects.core.LockFile _lockFile;

  /**
   * Field _provenance.
   */
  private uk.ac.vamsas.objects.core.Provenance _provenance;

  /**
   * contains unassociated trees and a number of analysis sets
   * 
   */
  private java.util.Vector _VAMSASList;

  /**
   * Field _applicationDataList.
   */
  private java.util.Vector _applicationDataList;

  /**
   * Field _attachmentList.
   */
  private java.util.Vector _attachmentList;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public VamsasDocument() {
    super();
    this._VAMSASList = new java.util.Vector();
    this._applicationDataList = new java.util.Vector();
    this._attachmentList = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vApplicationData
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addApplicationData(
      final uk.ac.vamsas.objects.core.ApplicationData vApplicationData)
      throws java.lang.IndexOutOfBoundsException {
    this._applicationDataList.addElement(vApplicationData);
  }

  /**
   * 
   * 
   * @param index
   * @param vApplicationData
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addApplicationData(final int index,
      final uk.ac.vamsas.objects.core.ApplicationData vApplicationData)
      throws java.lang.IndexOutOfBoundsException {
    this._applicationDataList.add(index, vApplicationData);
  }

  /**
   * 
   * 
   * @param vAttachment
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addAttachment(
      final uk.ac.vamsas.objects.core.Attachment vAttachment)
      throws java.lang.IndexOutOfBoundsException {
    this._attachmentList.addElement(vAttachment);
  }

  /**
   * 
   * 
   * @param index
   * @param vAttachment
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addAttachment(final int index,
      final uk.ac.vamsas.objects.core.Attachment vAttachment)
      throws java.lang.IndexOutOfBoundsException {
    this._attachmentList.add(index, vAttachment);
  }

  /**
   * 
   * 
   * @param vVAMSAS
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addVAMSAS(final uk.ac.vamsas.objects.core.VAMSAS vVAMSAS)
      throws java.lang.IndexOutOfBoundsException {
    this._VAMSASList.addElement(vVAMSAS);
  }

  /**
   * 
   * 
   * @param index
   * @param vVAMSAS
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addVAMSAS(final int index,
      final uk.ac.vamsas.objects.core.VAMSAS vVAMSAS)
      throws java.lang.IndexOutOfBoundsException {
    this._VAMSASList.add(index, vVAMSAS);
  }

  /**
   * Method enumerateApplicationData.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.ApplicationData
   *         elements
   */
  public java.util.Enumeration enumerateApplicationData() {
    return this._applicationDataList.elements();
  }

  /**
   * Method enumerateAttachment.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Attachment
   *         elements
   */
  public java.util.Enumeration enumerateAttachment() {
    return this._attachmentList.elements();
  }

  /**
   * Method enumerateVAMSAS.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.VAMSAS elements
   */
  public java.util.Enumeration enumerateVAMSAS() {
    return this._VAMSASList.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof VamsasDocument) {

      VamsasDocument temp = (VamsasDocument) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._version != null) {
        if (temp._version == null)
          return false;
        if (this._version != temp._version) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._version);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._version);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._version);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._version);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._version.equals(temp._version)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._version);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._version);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._version);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._version);
          }
        }
      } else if (temp._version != null)
        return false;
      if (this._lockFile != null) {
        if (temp._lockFile == null)
          return false;
        if (this._lockFile != temp._lockFile) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._lockFile);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._lockFile);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._lockFile);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._lockFile);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._lockFile.equals(temp._lockFile)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._lockFile);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._lockFile);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._lockFile);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._lockFile);
          }
        }
      } else if (temp._lockFile != null)
        return false;
      if (this._provenance != null) {
        if (temp._provenance == null)
          return false;
        if (this._provenance != temp._provenance) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._provenance);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._provenance);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._provenance);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._provenance);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._provenance.equals(temp._provenance)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._provenance);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._provenance);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._provenance);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._provenance);
          }
        }
      } else if (temp._provenance != null)
        return false;
      if (this._VAMSASList != null) {
        if (temp._VAMSASList == null)
          return false;
        if (this._VAMSASList != temp._VAMSASList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._VAMSASList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._VAMSASList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._VAMSASList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._VAMSASList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._VAMSASList.equals(temp._VAMSASList)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._VAMSASList);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._VAMSASList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._VAMSASList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._VAMSASList);
          }
        }
      } else if (temp._VAMSASList != null)
        return false;
      if (this._applicationDataList != null) {
        if (temp._applicationDataList == null)
          return false;
        if (this._applicationDataList != temp._applicationDataList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._applicationDataList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._applicationDataList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._applicationDataList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._applicationDataList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._applicationDataList.equals(temp._applicationDataList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._applicationDataList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._applicationDataList);
              return false;
            }
            org.castor.util.CycleBreaker
                .releaseCycleHandle(this._applicationDataList);
            org.castor.util.CycleBreaker
                .releaseCycleHandle(temp._applicationDataList);
          }
        }
      } else if (temp._applicationDataList != null)
        return false;
      if (this._attachmentList != null) {
        if (temp._attachmentList == null)
          return false;
        if (this._attachmentList != temp._attachmentList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._attachmentList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._attachmentList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._attachmentList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._attachmentList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._attachmentList.equals(temp._attachmentList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._attachmentList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._attachmentList);
              return false;
            }
            org.castor.util.CycleBreaker
                .releaseCycleHandle(this._attachmentList);
            org.castor.util.CycleBreaker
                .releaseCycleHandle(temp._attachmentList);
          }
        }
      } else if (temp._attachmentList != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Method getApplicationData.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.ApplicationData at the
   *         given index
   */
  public uk.ac.vamsas.objects.core.ApplicationData getApplicationData(
      final int index) throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._applicationDataList.size()) {
      throw new IndexOutOfBoundsException("getApplicationData: Index value '"
          + index + "' not in range [0.."
          + (this._applicationDataList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.ApplicationData) _applicationDataList
        .get(index);
  }

  /**
   * Method getApplicationData.Returns the contents of the collection in an
   * Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.ApplicationData[] getApplicationData() {
    uk.ac.vamsas.objects.core.ApplicationData[] array = new uk.ac.vamsas.objects.core.ApplicationData[0];
    return (uk.ac.vamsas.objects.core.ApplicationData[]) this._applicationDataList
        .toArray(array);
  }

  /**
   * Method getApplicationDataAsReference.Returns a reference to
   * '_applicationDataList'. No type checking is performed on any modifications
   * to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getApplicationDataAsReference() {
    return this._applicationDataList;
  }

  /**
   * Method getApplicationDataCount.
   * 
   * @return the size of this collection
   */
  public int getApplicationDataCount() {
    return this._applicationDataList.size();
  }

  /**
   * Method getAttachment.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Attachment at the given
   *         index
   */
  public uk.ac.vamsas.objects.core.Attachment getAttachment(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._attachmentList.size()) {
      throw new IndexOutOfBoundsException("getAttachment: Index value '"
          + index + "' not in range [0.." + (this._attachmentList.size() - 1)
          + "]");
    }

    return (uk.ac.vamsas.objects.core.Attachment) _attachmentList.get(index);
  }

  /**
   * Method getAttachment.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Attachment[] getAttachment() {
    uk.ac.vamsas.objects.core.Attachment[] array = new uk.ac.vamsas.objects.core.Attachment[0];
    return (uk.ac.vamsas.objects.core.Attachment[]) this._attachmentList
        .toArray(array);
  }

  /**
   * Method getAttachmentAsReference.Returns a reference to '_attachmentList'.
   * No type checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getAttachmentAsReference() {
    return this._attachmentList;
  }

  /**
   * Method getAttachmentCount.
   * 
   * @return the size of this collection
   */
  public int getAttachmentCount() {
    return this._attachmentList.size();
  }

  /**
   * Returns the value of field 'lockFile'.
   * 
   * @return the value of field 'LockFile'.
   */
  public uk.ac.vamsas.objects.core.LockFile getLockFile() {
    return this._lockFile;
  }

  /**
   * Returns the value of field 'provenance'.
   * 
   * @return the value of field 'Provenance'.
   */
  public uk.ac.vamsas.objects.core.Provenance getProvenance() {
    return this._provenance;
  }

  /**
   * Method getVAMSAS.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.VAMSAS at the given
   *         index
   */
  public uk.ac.vamsas.objects.core.VAMSAS getVAMSAS(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._VAMSASList.size()) {
      throw new IndexOutOfBoundsException("getVAMSAS: Index value '" + index
          + "' not in range [0.." + (this._VAMSASList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.VAMSAS) _VAMSASList.get(index);
  }

  /**
   * Method getVAMSAS.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.VAMSAS[] getVAMSAS() {
    uk.ac.vamsas.objects.core.VAMSAS[] array = new uk.ac.vamsas.objects.core.VAMSAS[0];
    return (uk.ac.vamsas.objects.core.VAMSAS[]) this._VAMSASList.toArray(array);
  }

  /**
   * Method getVAMSASAsReference.Returns a reference to '_VAMSASList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getVAMSASAsReference() {
    return this._VAMSASList;
  }

  /**
   * Method getVAMSASCount.
   * 
   * @return the size of this collection
   */
  public int getVAMSASCount() {
    return this._VAMSASList.size();
  }

  /**
   * Returns the value of field 'version'. The field 'version' has the following
   * description: Vamsas Document Version Number
   * 
   * @return the value of field 'Version'.
   */
  public java.lang.String getVersion() {
    return this._version;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_version != null
        && !org.castor.util.CycleBreaker.startingToCycle(_version)) {
      result = 37 * result + _version.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_version);
    }
    if (_lockFile != null
        && !org.castor.util.CycleBreaker.startingToCycle(_lockFile)) {
      result = 37 * result + _lockFile.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_lockFile);
    }
    if (_provenance != null
        && !org.castor.util.CycleBreaker.startingToCycle(_provenance)) {
      result = 37 * result + _provenance.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_provenance);
    }
    if (_VAMSASList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_VAMSASList)) {
      result = 37 * result + _VAMSASList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_VAMSASList);
    }
    if (_applicationDataList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_applicationDataList)) {
      result = 37 * result + _applicationDataList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_applicationDataList);
    }
    if (_attachmentList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_attachmentList)) {
      result = 37 * result + _attachmentList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_attachmentList);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
     */
  public void removeAllApplicationData() {
    this._applicationDataList.clear();
  }

  /**
     */
  public void removeAllAttachment() {
    this._attachmentList.clear();
  }

  /**
     */
  public void removeAllVAMSAS() {
    this._VAMSASList.clear();
  }

  /**
   * Method removeApplicationData.
   * 
   * @param vApplicationData
   * @return true if the object was removed from the collection.
   */
  public boolean removeApplicationData(
      final uk.ac.vamsas.objects.core.ApplicationData vApplicationData) {
    boolean removed = _applicationDataList.remove(vApplicationData);
    return removed;
  }

  /**
   * Method removeApplicationDataAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.ApplicationData removeApplicationDataAt(
      final int index) {
    java.lang.Object obj = this._applicationDataList.remove(index);
    return (uk.ac.vamsas.objects.core.ApplicationData) obj;
  }

  /**
   * Method removeAttachment.
   * 
   * @param vAttachment
   * @return true if the object was removed from the collection.
   */
  public boolean removeAttachment(
      final uk.ac.vamsas.objects.core.Attachment vAttachment) {
    boolean removed = _attachmentList.remove(vAttachment);
    return removed;
  }

  /**
   * Method removeAttachmentAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Attachment removeAttachmentAt(final int index) {
    java.lang.Object obj = this._attachmentList.remove(index);
    return (uk.ac.vamsas.objects.core.Attachment) obj;
  }

  /**
   * Method removeVAMSAS.
   * 
   * @param vVAMSAS
   * @return true if the object was removed from the collection.
   */
  public boolean removeVAMSAS(final uk.ac.vamsas.objects.core.VAMSAS vVAMSAS) {
    boolean removed = _VAMSASList.remove(vVAMSAS);
    return removed;
  }

  /**
   * Method removeVAMSASAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.VAMSAS removeVAMSASAt(final int index) {
    java.lang.Object obj = this._VAMSASList.remove(index);
    return (uk.ac.vamsas.objects.core.VAMSAS) obj;
  }

  /**
   * 
   * 
   * @param index
   * @param vApplicationData
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setApplicationData(final int index,
      final uk.ac.vamsas.objects.core.ApplicationData vApplicationData)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._applicationDataList.size()) {
      throw new IndexOutOfBoundsException("setApplicationData: Index value '"
          + index + "' not in range [0.."
          + (this._applicationDataList.size() - 1) + "]");
    }

    this._applicationDataList.set(index, vApplicationData);
  }

  /**
   * 
   * 
   * @param vApplicationDataArray
   */
  public void setApplicationData(
      final uk.ac.vamsas.objects.core.ApplicationData[] vApplicationDataArray) {
    // -- copy array
    _applicationDataList.clear();

    for (int i = 0; i < vApplicationDataArray.length; i++) {
      this._applicationDataList.add(vApplicationDataArray[i]);
    }
  }

  /**
   * Sets the value of '_applicationDataList' by copying the given Vector. All
   * elements will be checked for type safety.
   * 
   * @param vApplicationDataList
   *          the Vector to copy.
   */
  public void setApplicationData(final java.util.Vector vApplicationDataList) {
    // copy vector
    this._applicationDataList.clear();

    this._applicationDataList.addAll(vApplicationDataList);
  }

  /**
   * Sets the value of '_applicationDataList' by setting it to the given Vector.
   * No type checking is performed.
   * 
   * @deprecated
   * 
   * @param applicationDataVector
   *          the Vector to set.
   */
  public void setApplicationDataAsReference(
      final java.util.Vector applicationDataVector) {
    this._applicationDataList = applicationDataVector;
  }

  /**
   * 
   * 
   * @param index
   * @param vAttachment
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setAttachment(final int index,
      final uk.ac.vamsas.objects.core.Attachment vAttachment)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._attachmentList.size()) {
      throw new IndexOutOfBoundsException("setAttachment: Index value '"
          + index + "' not in range [0.." + (this._attachmentList.size() - 1)
          + "]");
    }

    this._attachmentList.set(index, vAttachment);
  }

  /**
   * 
   * 
   * @param vAttachmentArray
   */
  public void setAttachment(
      final uk.ac.vamsas.objects.core.Attachment[] vAttachmentArray) {
    // -- copy array
    _attachmentList.clear();

    for (int i = 0; i < vAttachmentArray.length; i++) {
      this._attachmentList.add(vAttachmentArray[i]);
    }
  }

  /**
   * Sets the value of '_attachmentList' by copying the given Vector. All
   * elements will be checked for type safety.
   * 
   * @param vAttachmentList
   *          the Vector to copy.
   */
  public void setAttachment(final java.util.Vector vAttachmentList) {
    // copy vector
    this._attachmentList.clear();

    this._attachmentList.addAll(vAttachmentList);
  }

  /**
   * Sets the value of '_attachmentList' by setting it to the given Vector. No
   * type checking is performed.
   * 
   * @deprecated
   * 
   * @param attachmentVector
   *          the Vector to set.
   */
  public void setAttachmentAsReference(final java.util.Vector attachmentVector) {
    this._attachmentList = attachmentVector;
  }

  /**
   * Sets the value of field 'lockFile'.
   * 
   * @param lockFile
   *          the value of field 'lockFile'.
   */
  public void setLockFile(final uk.ac.vamsas.objects.core.LockFile lockFile) {
    this._lockFile = lockFile;
  }

  /**
   * Sets the value of field 'provenance'.
   * 
   * @param provenance
   *          the value of field 'provenance'.
   */
  public void setProvenance(
      final uk.ac.vamsas.objects.core.Provenance provenance) {
    this._provenance = provenance;
  }

  /**
   * 
   * 
   * @param index
   * @param vVAMSAS
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setVAMSAS(final int index,
      final uk.ac.vamsas.objects.core.VAMSAS vVAMSAS)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._VAMSASList.size()) {
      throw new IndexOutOfBoundsException("setVAMSAS: Index value '" + index
          + "' not in range [0.." + (this._VAMSASList.size() - 1) + "]");
    }

    this._VAMSASList.set(index, vVAMSAS);
  }

  /**
   * 
   * 
   * @param vVAMSASArray
   */
  public void setVAMSAS(final uk.ac.vamsas.objects.core.VAMSAS[] vVAMSASArray) {
    // -- copy array
    _VAMSASList.clear();

    for (int i = 0; i < vVAMSASArray.length; i++) {
      this._VAMSASList.add(vVAMSASArray[i]);
    }
  }

  /**
   * Sets the value of '_VAMSASList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vVAMSASList
   *          the Vector to copy.
   */
  public void setVAMSAS(final java.util.Vector vVAMSASList) {
    // copy vector
    this._VAMSASList.clear();

    this._VAMSASList.addAll(vVAMSASList);
  }

  /**
   * Sets the value of '_VAMSASList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param VAMSASVector
   *          the Vector to set.
   */
  public void setVAMSASAsReference(final java.util.Vector VAMSASVector) {
    this._VAMSASList = VAMSASVector;
  }

  /**
   * Sets the value of field 'version'. The field 'version' has the following
   * description: Vamsas Document Version Number
   * 
   * @param version
   *          the value of field 'version'.
   */
  public void setVersion(final java.lang.String version) {
    this._version = version;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.VamsasDocument
   */
  public static uk.ac.vamsas.objects.core.VamsasDocument unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.VamsasDocument) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.VamsasDocument.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
