/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Entry.
 * 
 * @version $Revision$ $Date: 2007-06-28 14:51:44 +0100 (Thu, 28 Jun 2007)
 *          $
 */
public class Entry extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Primary Key for vamsas object referencing
   * 
   */
  private java.lang.String _id;

  /**
   * Who
   */
  private java.lang.String _user;

  /**
   * With which application
   */
  private java.lang.String _app;

  /**
   * Did what
   */
  private java.lang.String _action;

  /**
   * When
   */
  private java.util.Date _date;

  /**
   * additional information
   */
  private java.util.Vector _propertyList;

  /**
   * parameter for the action
   */
  private java.util.Vector _paramList;

  /**
   * bioinformatic objects input to action
   * 
   */
  private java.util.Vector _inputList;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public Entry() {
    super();
    this._propertyList = new java.util.Vector();
    this._paramList = new java.util.Vector();
    this._inputList = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vInput
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addInput(final uk.ac.vamsas.objects.core.Input vInput)
      throws java.lang.IndexOutOfBoundsException {
    this._inputList.addElement(vInput);
  }

  /**
   * 
   * 
   * @param index
   * @param vInput
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addInput(final int index,
      final uk.ac.vamsas.objects.core.Input vInput)
      throws java.lang.IndexOutOfBoundsException {
    this._inputList.add(index, vInput);
  }

  /**
   * 
   * 
   * @param vParam
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addParam(final uk.ac.vamsas.objects.core.Param vParam)
      throws java.lang.IndexOutOfBoundsException {
    this._paramList.addElement(vParam);
  }

  /**
   * 
   * 
   * @param index
   * @param vParam
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addParam(final int index,
      final uk.ac.vamsas.objects.core.Param vParam)
      throws java.lang.IndexOutOfBoundsException {
    this._paramList.add(index, vParam);
  }

  /**
   * 
   * 
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addProperty(final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    this._propertyList.addElement(vProperty);
  }

  /**
   * 
   * 
   * @param index
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addProperty(final int index,
      final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    this._propertyList.add(index, vProperty);
  }

  /**
   * Method enumerateInput.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Input elements
   */
  public java.util.Enumeration enumerateInput() {
    return this._inputList.elements();
  }

  /**
   * Method enumerateParam.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Param elements
   */
  public java.util.Enumeration enumerateParam() {
    return this._paramList.elements();
  }

  /**
   * Method enumerateProperty.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Property elements
   */
  public java.util.Enumeration enumerateProperty() {
    return this._propertyList.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof Entry) {

      Entry temp = (Entry) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._id != null) {
        if (temp._id == null)
          return false;
        if (this._id != temp._id) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._id);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._id);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._id.equals(temp._id)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
          }
        }
      } else if (temp._id != null)
        return false;
      if (this._user != null) {
        if (temp._user == null)
          return false;
        if (this._user != temp._user) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._user);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._user);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._user);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._user);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._user.equals(temp._user)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._user);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._user);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._user);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._user);
          }
        }
      } else if (temp._user != null)
        return false;
      if (this._app != null) {
        if (temp._app == null)
          return false;
        if (this._app != temp._app) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._app);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._app);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._app);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._app);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._app.equals(temp._app)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._app);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._app);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._app);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._app);
          }
        }
      } else if (temp._app != null)
        return false;
      if (this._action != null) {
        if (temp._action == null)
          return false;
        if (this._action != temp._action) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._action);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._action);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._action);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._action);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._action.equals(temp._action)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._action);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._action);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._action);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._action);
          }
        }
      } else if (temp._action != null)
        return false;
      if (this._date != null) {
        if (temp._date == null)
          return false;
        if (this._date != temp._date) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._date);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._date);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._date);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._date);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._date.equals(temp._date)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._date);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._date);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._date);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._date);
          }
        }
      } else if (temp._date != null)
        return false;
      if (this._propertyList != null) {
        if (temp._propertyList == null)
          return false;
        if (this._propertyList != temp._propertyList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._propertyList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._propertyList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._propertyList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._propertyList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._propertyList.equals(temp._propertyList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._propertyList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._propertyList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._propertyList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._propertyList);
          }
        }
      } else if (temp._propertyList != null)
        return false;
      if (this._paramList != null) {
        if (temp._paramList == null)
          return false;
        if (this._paramList != temp._paramList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._paramList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._paramList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._paramList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._paramList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._paramList.equals(temp._paramList)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._paramList);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._paramList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._paramList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._paramList);
          }
        }
      } else if (temp._paramList != null)
        return false;
      if (this._inputList != null) {
        if (temp._inputList == null)
          return false;
        if (this._inputList != temp._inputList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._inputList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._inputList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._inputList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._inputList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._inputList.equals(temp._inputList)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._inputList);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._inputList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._inputList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._inputList);
          }
        }
      } else if (temp._inputList != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'action'. The field 'action' has the following
   * description: Did what
   * 
   * @return the value of field 'Action'.
   */
  public java.lang.String getAction() {
    return this._action;
  }

  /**
   * Returns the value of field 'app'. The field 'app' has the following
   * description: With which application
   * 
   * @return the value of field 'App'.
   */
  public java.lang.String getApp() {
    return this._app;
  }

  /**
   * Returns the value of field 'date'. The field 'date' has the following
   * description: When
   * 
   * @return the value of field 'Date'.
   */
  public java.util.Date getDate() {
    return this._date;
  }

  /**
   * Returns the value of field 'id'. The field 'id' has the following
   * description: Primary Key for vamsas object referencing
   * 
   * 
   * @return the value of field 'Id'.
   */
  public java.lang.String getId() {
    return this._id;
  }

  /**
   * Method getInput.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Input at the given index
   */
  public uk.ac.vamsas.objects.core.Input getInput(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._inputList.size()) {
      throw new IndexOutOfBoundsException("getInput: Index value '" + index
          + "' not in range [0.." + (this._inputList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Input) _inputList.get(index);
  }

  /**
   * Method getInput.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Input[] getInput() {
    uk.ac.vamsas.objects.core.Input[] array = new uk.ac.vamsas.objects.core.Input[0];
    return (uk.ac.vamsas.objects.core.Input[]) this._inputList.toArray(array);
  }

  /**
   * Method getInputAsReference.Returns a reference to '_inputList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getInputAsReference() {
    return this._inputList;
  }

  /**
   * Method getInputCount.
   * 
   * @return the size of this collection
   */
  public int getInputCount() {
    return this._inputList.size();
  }

  /**
   * Method getParam.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Param at the given index
   */
  public uk.ac.vamsas.objects.core.Param getParam(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._paramList.size()) {
      throw new IndexOutOfBoundsException("getParam: Index value '" + index
          + "' not in range [0.." + (this._paramList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Param) _paramList.get(index);
  }

  /**
   * Method getParam.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Param[] getParam() {
    uk.ac.vamsas.objects.core.Param[] array = new uk.ac.vamsas.objects.core.Param[0];
    return (uk.ac.vamsas.objects.core.Param[]) this._paramList.toArray(array);
  }

  /**
   * Method getParamAsReference.Returns a reference to '_paramList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getParamAsReference() {
    return this._paramList;
  }

  /**
   * Method getParamCount.
   * 
   * @return the size of this collection
   */
  public int getParamCount() {
    return this._paramList.size();
  }

  /**
   * Method getProperty.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Property at the given
   *         index
   */
  public uk.ac.vamsas.objects.core.Property getProperty(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._propertyList.size()) {
      throw new IndexOutOfBoundsException("getProperty: Index value '" + index
          + "' not in range [0.." + (this._propertyList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Property) _propertyList.get(index);
  }

  /**
   * Method getProperty.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Property[] getProperty() {
    uk.ac.vamsas.objects.core.Property[] array = new uk.ac.vamsas.objects.core.Property[0];
    return (uk.ac.vamsas.objects.core.Property[]) this._propertyList
        .toArray(array);
  }

  /**
   * Method getPropertyAsReference.Returns a reference to '_propertyList'. No
   * type checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getPropertyAsReference() {
    return this._propertyList;
  }

  /**
   * Method getPropertyCount.
   * 
   * @return the size of this collection
   */
  public int getPropertyCount() {
    return this._propertyList.size();
  }

  /**
   * Returns the value of field 'user'. The field 'user' has the following
   * description: Who
   * 
   * @return the value of field 'User'.
   */
  public java.lang.String getUser() {
    return this._user;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_id != null && !org.castor.util.CycleBreaker.startingToCycle(_id)) {
      result = 37 * result + _id.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_id);
    }
    if (_user != null && !org.castor.util.CycleBreaker.startingToCycle(_user)) {
      result = 37 * result + _user.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_user);
    }
    if (_app != null && !org.castor.util.CycleBreaker.startingToCycle(_app)) {
      result = 37 * result + _app.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_app);
    }
    if (_action != null
        && !org.castor.util.CycleBreaker.startingToCycle(_action)) {
      result = 37 * result + _action.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_action);
    }
    if (_date != null && !org.castor.util.CycleBreaker.startingToCycle(_date)) {
      result = 37 * result + _date.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_date);
    }
    if (_propertyList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_propertyList)) {
      result = 37 * result + _propertyList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_propertyList);
    }
    if (_paramList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_paramList)) {
      result = 37 * result + _paramList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_paramList);
    }
    if (_inputList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_inputList)) {
      result = 37 * result + _inputList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_inputList);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
     */
  public void removeAllInput() {
    this._inputList.clear();
  }

  /**
     */
  public void removeAllParam() {
    this._paramList.clear();
  }

  /**
     */
  public void removeAllProperty() {
    this._propertyList.clear();
  }

  /**
   * Method removeInput.
   * 
   * @param vInput
   * @return true if the object was removed from the collection.
   */
  public boolean removeInput(final uk.ac.vamsas.objects.core.Input vInput) {
    boolean removed = _inputList.remove(vInput);
    return removed;
  }

  /**
   * Method removeInputAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Input removeInputAt(final int index) {
    java.lang.Object obj = this._inputList.remove(index);
    return (uk.ac.vamsas.objects.core.Input) obj;
  }

  /**
   * Method removeParam.
   * 
   * @param vParam
   * @return true if the object was removed from the collection.
   */
  public boolean removeParam(final uk.ac.vamsas.objects.core.Param vParam) {
    boolean removed = _paramList.remove(vParam);
    return removed;
  }

  /**
   * Method removeParamAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Param removeParamAt(final int index) {
    java.lang.Object obj = this._paramList.remove(index);
    return (uk.ac.vamsas.objects.core.Param) obj;
  }

  /**
   * Method removeProperty.
   * 
   * @param vProperty
   * @return true if the object was removed from the collection.
   */
  public boolean removeProperty(
      final uk.ac.vamsas.objects.core.Property vProperty) {
    boolean removed = _propertyList.remove(vProperty);
    return removed;
  }

  /**
   * Method removePropertyAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Property removePropertyAt(final int index) {
    java.lang.Object obj = this._propertyList.remove(index);
    return (uk.ac.vamsas.objects.core.Property) obj;
  }

  /**
   * Sets the value of field 'action'. The field 'action' has the following
   * description: Did what
   * 
   * @param action
   *          the value of field 'action'.
   */
  public void setAction(final java.lang.String action) {
    this._action = action;
  }

  /**
   * Sets the value of field 'app'. The field 'app' has the following
   * description: With which application
   * 
   * @param app
   *          the value of field 'app'.
   */
  public void setApp(final java.lang.String app) {
    this._app = app;
  }

  /**
   * Sets the value of field 'date'. The field 'date' has the following
   * description: When
   * 
   * @param date
   *          the value of field 'date'.
   */
  public void setDate(final java.util.Date date) {
    this._date = date;
  }

  /**
   * Sets the value of field 'id'. The field 'id' has the following description:
   * Primary Key for vamsas object referencing
   * 
   * 
   * @param id
   *          the value of field 'id'.
   */
  public void setId(final java.lang.String id) {
    this._id = id;
  }

  /**
   * 
   * 
   * @param index
   * @param vInput
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setInput(final int index,
      final uk.ac.vamsas.objects.core.Input vInput)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._inputList.size()) {
      throw new IndexOutOfBoundsException("setInput: Index value '" + index
          + "' not in range [0.." + (this._inputList.size() - 1) + "]");
    }

    this._inputList.set(index, vInput);
  }

  /**
   * 
   * 
   * @param vInputArray
   */
  public void setInput(final uk.ac.vamsas.objects.core.Input[] vInputArray) {
    // -- copy array
    _inputList.clear();

    for (int i = 0; i < vInputArray.length; i++) {
      this._inputList.add(vInputArray[i]);
    }
  }

  /**
   * Sets the value of '_inputList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vInputList
   *          the Vector to copy.
   */
  public void setInput(final java.util.Vector vInputList) {
    // copy vector
    this._inputList.clear();

    this._inputList.addAll(vInputList);
  }

  /**
   * Sets the value of '_inputList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param inputVector
   *          the Vector to set.
   */
  public void setInputAsReference(final java.util.Vector inputVector) {
    this._inputList = inputVector;
  }

  /**
   * 
   * 
   * @param index
   * @param vParam
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setParam(final int index,
      final uk.ac.vamsas.objects.core.Param vParam)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._paramList.size()) {
      throw new IndexOutOfBoundsException("setParam: Index value '" + index
          + "' not in range [0.." + (this._paramList.size() - 1) + "]");
    }

    this._paramList.set(index, vParam);
  }

  /**
   * 
   * 
   * @param vParamArray
   */
  public void setParam(final uk.ac.vamsas.objects.core.Param[] vParamArray) {
    // -- copy array
    _paramList.clear();

    for (int i = 0; i < vParamArray.length; i++) {
      this._paramList.add(vParamArray[i]);
    }
  }

  /**
   * Sets the value of '_paramList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vParamList
   *          the Vector to copy.
   */
  public void setParam(final java.util.Vector vParamList) {
    // copy vector
    this._paramList.clear();

    this._paramList.addAll(vParamList);
  }

  /**
   * Sets the value of '_paramList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param paramVector
   *          the Vector to set.
   */
  public void setParamAsReference(final java.util.Vector paramVector) {
    this._paramList = paramVector;
  }

  /**
   * 
   * 
   * @param index
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setProperty(final int index,
      final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._propertyList.size()) {
      throw new IndexOutOfBoundsException("setProperty: Index value '" + index
          + "' not in range [0.." + (this._propertyList.size() - 1) + "]");
    }

    this._propertyList.set(index, vProperty);
  }

  /**
   * 
   * 
   * @param vPropertyArray
   */
  public void setProperty(
      final uk.ac.vamsas.objects.core.Property[] vPropertyArray) {
    // -- copy array
    _propertyList.clear();

    for (int i = 0; i < vPropertyArray.length; i++) {
      this._propertyList.add(vPropertyArray[i]);
    }
  }

  /**
   * Sets the value of '_propertyList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vPropertyList
   *          the Vector to copy.
   */
  public void setProperty(final java.util.Vector vPropertyList) {
    // copy vector
    this._propertyList.clear();

    this._propertyList.addAll(vPropertyList);
  }

  /**
   * Sets the value of '_propertyList' by setting it to the given Vector. No
   * type checking is performed.
   * 
   * @deprecated
   * 
   * @param propertyVector
   *          the Vector to set.
   */
  public void setPropertyAsReference(final java.util.Vector propertyVector) {
    this._propertyList = propertyVector;
  }

  /**
   * Sets the value of field 'user'. The field 'user' has the following
   * description: Who
   * 
   * @param user
   *          the value of field 'user'.
   */
  public void setUser(final java.lang.String user) {
    this._user = user;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.Entry
   */
  public static uk.ac.vamsas.objects.core.Entry unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.Entry) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.Entry.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
