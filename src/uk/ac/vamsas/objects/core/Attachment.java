/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Attachment.
 * 
 * @version $Revision$ $Date$
 */
public class Attachment extends uk.ac.vamsas.objects.core.AppData implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * true implies data will be decompresses with Zip before presenting to
   * application
   */
  private boolean _compressed = false;

  /**
   * keeps track of state for field: _compressed
   */
  private boolean _has_compressed;

  /**
   * Type of arbitrary data - TODO: decide format - use (extended) MIME types ?
   */
  private java.lang.String _type;

  /**
   * Object the arbitrary data is associated with
   * 
   */
  private java.lang.Object _objectref;

  /**
   * Primary Key for vamsas object referencing
   * 
   */
  private java.lang.String _id;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public Attachment() {
    super();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
     */
  public void deleteCompressed() {
    this._has_compressed = false;
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof Attachment) {

      Attachment temp = (Attachment) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._compressed != temp._compressed)
        return false;
      if (this._has_compressed != temp._has_compressed)
        return false;
      if (this._type != null) {
        if (temp._type == null)
          return false;
        if (this._type != temp._type) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._type);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._type);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._type);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._type);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._type.equals(temp._type)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._type);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._type);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._type);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._type);
          }
        }
      } else if (temp._type != null)
        return false;
      if (this._objectref != null) {
        if (temp._objectref == null)
          return false;
        if (this._objectref != temp._objectref) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._objectref);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._objectref);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._objectref);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._objectref);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._objectref.equals(temp._objectref)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._objectref);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._objectref);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._objectref);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._objectref);
          }
        }
      } else if (temp._objectref != null)
        return false;
      if (this._id != null) {
        if (temp._id == null)
          return false;
        if (this._id != temp._id) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._id);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._id);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._id.equals(temp._id)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
          }
        }
      } else if (temp._id != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'compressed'. The field 'compressed' has the
   * following description: true implies data will be decompresses with Zip
   * before presenting to application
   * 
   * @return the value of field 'Compressed'.
   */
  public boolean getCompressed() {
    return this._compressed;
  }

  /**
   * Returns the value of field 'id'. The field 'id' has the following
   * description: Primary Key for vamsas object referencing
   * 
   * 
   * @return the value of field 'Id'.
   */
  public java.lang.String getId() {
    return this._id;
  }

  /**
   * Returns the value of field 'objectref'. The field 'objectref' has the
   * following description: Object the arbitrary data is associated with
   * 
   * 
   * @return the value of field 'Objectref'.
   */
  public java.lang.Object getObjectref() {
    return this._objectref;
  }

  /**
   * Returns the value of field 'type'. The field 'type' has the following
   * description: Type of arbitrary data - TODO: decide format - use (extended)
   * MIME types ?
   * 
   * @return the value of field 'Type'.
   */
  public java.lang.String getType() {
    return this._type;
  }

  /**
   * Method hasCompressed.
   * 
   * @return true if at least one Compressed has been added
   */
  public boolean hasCompressed() {
    return this._has_compressed;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    result = 37 * result + (_compressed ? 0 : 1);
    if (_type != null && !org.castor.util.CycleBreaker.startingToCycle(_type)) {
      result = 37 * result + _type.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_type);
    }
    if (_objectref != null
        && !org.castor.util.CycleBreaker.startingToCycle(_objectref)) {
      result = 37 * result + _objectref.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_objectref);
    }
    if (_id != null && !org.castor.util.CycleBreaker.startingToCycle(_id)) {
      result = 37 * result + _id.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_id);
    }

    return result;
  }

  /**
   * Returns the value of field 'compressed'. The field 'compressed' has the
   * following description: true implies data will be decompresses with Zip
   * before presenting to application
   * 
   * @return the value of field 'Compressed'.
   */
  public boolean isCompressed() {
    return this._compressed;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
   * Sets the value of field 'compressed'. The field 'compressed' has the
   * following description: true implies data will be decompresses with Zip
   * before presenting to application
   * 
   * @param compressed
   *          the value of field 'compressed'.
   */
  public void setCompressed(final boolean compressed) {
    this._compressed = compressed;
    this._has_compressed = true;
  }

  /**
   * Sets the value of field 'id'. The field 'id' has the following description:
   * Primary Key for vamsas object referencing
   * 
   * 
   * @param id
   *          the value of field 'id'.
   */
  public void setId(final java.lang.String id) {
    this._id = id;
  }

  /**
   * Sets the value of field 'objectref'. The field 'objectref' has the
   * following description: Object the arbitrary data is associated with
   * 
   * 
   * @param objectref
   *          the value of field 'objectref'.
   */
  public void setObjectref(final java.lang.Object objectref) {
    this._objectref = objectref;
  }

  /**
   * Sets the value of field 'type'. The field 'type' has the following
   * description: Type of arbitrary data - TODO: decide format - use (extended)
   * MIME types ?
   * 
   * @param type
   *          the value of field 'type'.
   */
  public void setType(final java.lang.String type) {
    this._type = type;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.AppData
   */
  public static uk.ac.vamsas.objects.core.AppData unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.AppData) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.Attachment.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
