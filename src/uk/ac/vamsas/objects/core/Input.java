/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Input.
 * 
 * @version $Revision$ $Date$
 */
public class Input extends uk.ac.vamsas.objects.core.RangeType implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Field _name.
   */
  private java.lang.String _name;

  /**
   * Reference Frame for rangeType specfication
   * 
   */
  private java.util.Vector _objRef;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public Input() {
    super();
    this._objRef = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vObjRef
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addObjRef(final java.lang.Object vObjRef)
      throws java.lang.IndexOutOfBoundsException {
    this._objRef.addElement(vObjRef);
  }

  /**
   * 
   * 
   * @param index
   * @param vObjRef
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addObjRef(final int index, final java.lang.Object vObjRef)
      throws java.lang.IndexOutOfBoundsException {
    this._objRef.add(index, vObjRef);
  }

  /**
   * Method enumerateObjRef.
   * 
   * @return an Enumeration over all java.lang.Object elements
   */
  public java.util.Enumeration enumerateObjRef() {
    return this._objRef.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof Input) {

      Input temp = (Input) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._name != null) {
        if (temp._name == null)
          return false;
        if (this._name != temp._name) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._name);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._name);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._name);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._name);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._name.equals(temp._name)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._name);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._name);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._name);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._name);
          }
        }
      } else if (temp._name != null)
        return false;
      if (this._objRef != null) {
        if (temp._objRef == null)
          return false;
        if (this._objRef != temp._objRef) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._objRef);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._objRef);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._objRef);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._objRef);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._objRef.equals(temp._objRef)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._objRef);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._objRef);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._objRef);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._objRef);
          }
        }
      } else if (temp._objRef != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'name'.
   * 
   * @return the value of field 'Name'.
   */
  public java.lang.String getName() {
    return this._name;
  }

  /**
   * Method getObjRef.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the java.lang.Object at the given index
   */
  public java.lang.Object getObjRef(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._objRef.size()) {
      throw new IndexOutOfBoundsException("getObjRef: Index value '" + index
          + "' not in range [0.." + (this._objRef.size() - 1) + "]");
    }

    return _objRef.get(index);
  }

  /**
   * Method getObjRef.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public java.lang.Object[] getObjRef() {
    java.lang.Object[] array = new java.lang.Object[0];
    return (java.lang.Object[]) this._objRef.toArray(array);
  }

  /**
   * Method getObjRefAsReference.Returns a reference to '_objRef'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getObjRefAsReference() {
    return this._objRef;
  }

  /**
   * Method getObjRefCount.
   * 
   * @return the size of this collection
   */
  public int getObjRefCount() {
    return this._objRef.size();
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_name != null && !org.castor.util.CycleBreaker.startingToCycle(_name)) {
      result = 37 * result + _name.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_name);
    }
    if (_objRef != null
        && !org.castor.util.CycleBreaker.startingToCycle(_objRef)) {
      result = 37 * result + _objRef.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_objRef);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
     */
  public void removeAllObjRef() {
    this._objRef.clear();
  }

  /**
   * Method removeObjRef.
   * 
   * @param vObjRef
   * @return true if the object was removed from the collection.
   */
  public boolean removeObjRef(final java.lang.Object vObjRef) {
    boolean removed = _objRef.remove(vObjRef);
    return removed;
  }

  /**
   * Method removeObjRefAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public java.lang.Object removeObjRefAt(final int index) {
    java.lang.Object obj = this._objRef.remove(index);
    return obj;
  }

  /**
   * Sets the value of field 'name'.
   * 
   * @param name
   *          the value of field 'name'.
   */
  public void setName(final java.lang.String name) {
    this._name = name;
  }

  /**
   * 
   * 
   * @param index
   * @param vObjRef
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setObjRef(final int index, final java.lang.Object vObjRef)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._objRef.size()) {
      throw new IndexOutOfBoundsException("setObjRef: Index value '" + index
          + "' not in range [0.." + (this._objRef.size() - 1) + "]");
    }

    this._objRef.set(index, vObjRef);
  }

  /**
   * 
   * 
   * @param vObjRefArray
   */
  public void setObjRef(final java.lang.Object[] vObjRefArray) {
    // -- copy array
    _objRef.clear();

    for (int i = 0; i < vObjRefArray.length; i++) {
      this._objRef.add(vObjRefArray[i]);
    }
  }

  /**
   * Sets the value of '_objRef' by copying the given Vector. All elements will
   * be checked for type safety.
   * 
   * @param vObjRefList
   *          the Vector to copy.
   */
  public void setObjRef(final java.util.Vector vObjRefList) {
    // copy vector
    this._objRef.clear();

    this._objRef.addAll(vObjRefList);
  }

  /**
   * Sets the value of '_objRef' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param objRefVector
   *          the Vector to set.
   */
  public void setObjRefAsReference(final java.util.Vector objRefVector) {
    this._objRef = objRefVector;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.RangeType
   */
  public static uk.ac.vamsas.objects.core.RangeType unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.RangeType) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.Input.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
