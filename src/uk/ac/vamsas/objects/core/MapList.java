/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class MapList.
 * 
 * @version $Revision$ $Date$
 */
public class MapList extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Offset to first position in dataset sequence record that start position on
   * 'onto' maps to
   */
  private long _from = 0;

  /**
   * keeps track of state for field: _from
   */
  private boolean _has_from;

  /**
   * Offset to last position in dataset sequence record that end position on
   * 'onto' maps to
   */
  private long _to = 0;

  /**
   * keeps track of state for field: _to
   */
  private boolean _has_to;

  /**
   * Offset to first position in database entry that first (or offset) position
   * in sequence maps to
   */
  private long _start;

  /**
   * keeps track of state for field: _start
   */
  private boolean _has_start;

  /**
   * Offset to last position in database entry that last (offset) position in
   * sequence maps to
   */
  private long _end;

  /**
   * keeps track of state for field: _end
   */
  private boolean _has_end;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public MapList() {
    super();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
     */
  public void deleteEnd() {
    this._has_end = false;
  }

  /**
     */
  public void deleteFrom() {
    this._has_from = false;
  }

  /**
     */
  public void deleteStart() {
    this._has_start = false;
  }

  /**
     */
  public void deleteTo() {
    this._has_to = false;
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof MapList) {

      MapList temp = (MapList) obj;
      if (this._from != temp._from)
        return false;
      if (this._has_from != temp._has_from)
        return false;
      if (this._to != temp._to)
        return false;
      if (this._has_to != temp._has_to)
        return false;
      if (this._start != temp._start)
        return false;
      if (this._has_start != temp._has_start)
        return false;
      if (this._end != temp._end)
        return false;
      if (this._has_end != temp._has_end)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'end'. The field 'end' has the following
   * description: Offset to last position in database entry that last (offset)
   * position in sequence maps to
   * 
   * @return the value of field 'End'.
   */
  public long getEnd() {
    return this._end;
  }

  /**
   * Returns the value of field 'from'. The field 'from' has the following
   * description: Offset to first position in dataset sequence record that start
   * position on 'onto' maps to
   * 
   * @return the value of field 'From'.
   */
  public long getFrom() {
    return this._from;
  }

  /**
   * Returns the value of field 'start'. The field 'start' has the following
   * description: Offset to first position in database entry that first (or
   * offset) position in sequence maps to
   * 
   * @return the value of field 'Start'.
   */
  public long getStart() {
    return this._start;
  }

  /**
   * Returns the value of field 'to'. The field 'to' has the following
   * description: Offset to last position in dataset sequence record that end
   * position on 'onto' maps to
   * 
   * @return the value of field 'To'.
   */
  public long getTo() {
    return this._to;
  }

  /**
   * Method hasEnd.
   * 
   * @return true if at least one End has been added
   */
  public boolean hasEnd() {
    return this._has_end;
  }

  /**
   * Method hasFrom.
   * 
   * @return true if at least one From has been added
   */
  public boolean hasFrom() {
    return this._has_from;
  }

  /**
   * Method hasStart.
   * 
   * @return true if at least one Start has been added
   */
  public boolean hasStart() {
    return this._has_start;
  }

  /**
   * Method hasTo.
   * 
   * @return true if at least one To has been added
   */
  public boolean hasTo() {
    return this._has_to;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    result = 37 * result + (int) (_from ^ (_from >>> 32));
    result = 37 * result + (int) (_to ^ (_to >>> 32));
    result = 37 * result + (int) (_start ^ (_start >>> 32));
    result = 37 * result + (int) (_end ^ (_end >>> 32));

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
   * Sets the value of field 'end'. The field 'end' has the following
   * description: Offset to last position in database entry that last (offset)
   * position in sequence maps to
   * 
   * @param end
   *          the value of field 'end'.
   */
  public void setEnd(final long end) {
    this._end = end;
    this._has_end = true;
  }

  /**
   * Sets the value of field 'from'. The field 'from' has the following
   * description: Offset to first position in dataset sequence record that start
   * position on 'onto' maps to
   * 
   * @param from
   *          the value of field 'from'.
   */
  public void setFrom(final long from) {
    this._from = from;
    this._has_from = true;
  }

  /**
   * Sets the value of field 'start'. The field 'start' has the following
   * description: Offset to first position in database entry that first (or
   * offset) position in sequence maps to
   * 
   * @param start
   *          the value of field 'start'.
   */
  public void setStart(final long start) {
    this._start = start;
    this._has_start = true;
  }

  /**
   * Sets the value of field 'to'. The field 'to' has the following description:
   * Offset to last position in dataset sequence record that end position on
   * 'onto' maps to
   * 
   * @param to
   *          the value of field 'to'.
   */
  public void setTo(final long to) {
    this._to = to;
    this._has_to = true;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.MapList
   */
  public static uk.ac.vamsas.objects.core.MapList unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.MapList) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.MapList.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
