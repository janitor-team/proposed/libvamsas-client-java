/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class AppData.
 * 
 * @version $Revision$ $Date$
 */
public class AppData extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Internal choice value storage
   */
  private java.lang.Object _choiceValue;

  /**
   * Field _data.
   */
  private byte[] _data;

  /**
   * Field _dataReference.
   */
  private java.lang.String _dataReference;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public AppData() {
    super();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof AppData) {

      AppData temp = (AppData) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._choiceValue != null) {
        if (temp._choiceValue == null)
          return false;
        if (this._choiceValue != temp._choiceValue) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._choiceValue);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._choiceValue);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._choiceValue);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._choiceValue);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._choiceValue.equals(temp._choiceValue)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._choiceValue);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._choiceValue);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._choiceValue);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._choiceValue);
          }
        }
      } else if (temp._choiceValue != null)
        return false;
      if (this._data != null) {
        if (temp._data == null)
          return false;
        if (this._data != temp._data) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._data);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._data);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._data);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._data);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!java.util.Arrays.equals(this._data, temp._data)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._data);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._data);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._data);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._data);
          }
        }
      } else if (temp._data != null)
        return false;
      if (this._dataReference != null) {
        if (temp._dataReference == null)
          return false;
        if (this._dataReference != temp._dataReference) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._dataReference);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._dataReference);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._dataReference);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._dataReference);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._dataReference.equals(temp._dataReference)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._dataReference);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._dataReference);
              return false;
            }
            org.castor.util.CycleBreaker
                .releaseCycleHandle(this._dataReference);
            org.castor.util.CycleBreaker
                .releaseCycleHandle(temp._dataReference);
          }
        }
      } else if (temp._dataReference != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'choiceValue'. The field 'choiceValue' has the
   * following description: Internal choice value storage
   * 
   * @return the value of field 'ChoiceValue'.
   */
  public java.lang.Object getChoiceValue() {
    return this._choiceValue;
  }

  /**
   * Returns the value of field 'data'.
   * 
   * @return the value of field 'Data'.
   */
  public byte[] getData() {
    return this._data;
  }

  /**
   * Returns the value of field 'dataReference'.
   * 
   * @return the value of field 'DataReference'.
   */
  public java.lang.String getDataReference() {
    return this._dataReference;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_choiceValue != null
        && !org.castor.util.CycleBreaker.startingToCycle(_choiceValue)) {
      result = 37 * result + _choiceValue.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_choiceValue);
    }
    if (_data != null && !org.castor.util.CycleBreaker.startingToCycle(_data)) {
      result = 37 * result + _data.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_data);
    }
    if (_dataReference != null
        && !org.castor.util.CycleBreaker.startingToCycle(_dataReference)) {
      result = 37 * result + _dataReference.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_dataReference);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
   * Sets the value of field 'data'.
   * 
   * @param data
   *          the value of field 'data'.
   */
  public void setData(final byte[] data) {
    this._data = data;
    this._choiceValue = data;
  }

  /**
   * Sets the value of field 'dataReference'.
   * 
   * @param dataReference
   *          the value of field 'dataReference'.
   */
  public void setDataReference(final java.lang.String dataReference) {
    this._dataReference = dataReference;
    this._choiceValue = dataReference;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.AppData
   */
  public static uk.ac.vamsas.objects.core.AppData unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.AppData) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.AppData.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
