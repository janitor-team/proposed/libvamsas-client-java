/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Provenance.
 * 
 * @version $Revision$ $Date: 2007-06-28 14:51:44 +0100 (Thu, 28 Jun 2007)
 *          $
 */
public class Provenance extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Field _entryList.
   */
  private java.util.Vector _entryList;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public Provenance() {
    super();
    this._entryList = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vEntry
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addEntry(final uk.ac.vamsas.objects.core.Entry vEntry)
      throws java.lang.IndexOutOfBoundsException {
    this._entryList.addElement(vEntry);
  }

  /**
   * 
   * 
   * @param index
   * @param vEntry
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addEntry(final int index,
      final uk.ac.vamsas.objects.core.Entry vEntry)
      throws java.lang.IndexOutOfBoundsException {
    this._entryList.add(index, vEntry);
  }

  /**
   * Method enumerateEntry.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Entry elements
   */
  public java.util.Enumeration enumerateEntry() {
    return this._entryList.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof Provenance) {

      Provenance temp = (Provenance) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._entryList != null) {
        if (temp._entryList == null)
          return false;
        if (this._entryList != temp._entryList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._entryList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._entryList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._entryList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._entryList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._entryList.equals(temp._entryList)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._entryList);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._entryList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._entryList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._entryList);
          }
        }
      } else if (temp._entryList != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Method getEntry.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Entry at the given index
   */
  public uk.ac.vamsas.objects.core.Entry getEntry(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._entryList.size()) {
      throw new IndexOutOfBoundsException("getEntry: Index value '" + index
          + "' not in range [0.." + (this._entryList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Entry) _entryList.get(index);
  }

  /**
   * Method getEntry.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Entry[] getEntry() {
    uk.ac.vamsas.objects.core.Entry[] array = new uk.ac.vamsas.objects.core.Entry[0];
    return (uk.ac.vamsas.objects.core.Entry[]) this._entryList.toArray(array);
  }

  /**
   * Method getEntryAsReference.Returns a reference to '_entryList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getEntryAsReference() {
    return this._entryList;
  }

  /**
   * Method getEntryCount.
   * 
   * @return the size of this collection
   */
  public int getEntryCount() {
    return this._entryList.size();
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_entryList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_entryList)) {
      result = 37 * result + _entryList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_entryList);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
     */
  public void removeAllEntry() {
    this._entryList.clear();
  }

  /**
   * Method removeEntry.
   * 
   * @param vEntry
   * @return true if the object was removed from the collection.
   */
  public boolean removeEntry(final uk.ac.vamsas.objects.core.Entry vEntry) {
    boolean removed = _entryList.remove(vEntry);
    return removed;
  }

  /**
   * Method removeEntryAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Entry removeEntryAt(final int index) {
    java.lang.Object obj = this._entryList.remove(index);
    return (uk.ac.vamsas.objects.core.Entry) obj;
  }

  /**
   * 
   * 
   * @param index
   * @param vEntry
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setEntry(final int index,
      final uk.ac.vamsas.objects.core.Entry vEntry)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._entryList.size()) {
      throw new IndexOutOfBoundsException("setEntry: Index value '" + index
          + "' not in range [0.." + (this._entryList.size() - 1) + "]");
    }

    this._entryList.set(index, vEntry);
  }

  /**
   * 
   * 
   * @param vEntryArray
   */
  public void setEntry(final uk.ac.vamsas.objects.core.Entry[] vEntryArray) {
    // -- copy array
    _entryList.clear();

    for (int i = 0; i < vEntryArray.length; i++) {
      this._entryList.add(vEntryArray[i]);
    }
  }

  /**
   * Sets the value of '_entryList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vEntryList
   *          the Vector to copy.
   */
  public void setEntry(final java.util.Vector vEntryList) {
    // copy vector
    this._entryList.clear();

    this._entryList.addAll(vEntryList);
  }

  /**
   * Sets the value of '_entryList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param entryVector
   *          the Vector to set.
   */
  public void setEntryAsReference(final java.util.Vector entryVector) {
    this._entryList = entryVector;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.Provenance
   */
  public static uk.ac.vamsas.objects.core.Provenance unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.Provenance) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.Provenance.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
