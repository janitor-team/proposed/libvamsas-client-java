/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * This is annotation over the coordinate frame defined by all the columns in
 * the alignment.
 * 
 * 
 * @version $Revision$ $Date$
 */
public class AlignmentAnnotation extends
    uk.ac.vamsas.objects.core.RangeAnnotation implements java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * TODO: decide if this flag is redundant - when true it would suggest that
   * the annotationElement values together form a graph
   */
  private boolean _graph;

  /**
   * keeps track of state for field: _graph
   */
  private boolean _has_graph;

  /**
   * annotation is associated with a range on a particular group of alignment
   * sequences
   */
  private java.util.Vector _seqrefs;

  /**
   * Field _provenance.
   */
  private uk.ac.vamsas.objects.core.Provenance _provenance;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public AlignmentAnnotation() {
    super();
    this._seqrefs = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vSeqrefs
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addSeqrefs(final java.lang.Object vSeqrefs)
      throws java.lang.IndexOutOfBoundsException {
    this._seqrefs.addElement(vSeqrefs);
  }

  /**
   * 
   * 
   * @param index
   * @param vSeqrefs
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addSeqrefs(final int index, final java.lang.Object vSeqrefs)
      throws java.lang.IndexOutOfBoundsException {
    this._seqrefs.add(index, vSeqrefs);
  }

  /**
     */
  public void deleteGraph() {
    this._has_graph = false;
  }

  /**
   * Method enumerateSeqrefs.
   * 
   * @return an Enumeration over all java.lang.Object elements
   */
  public java.util.Enumeration enumerateSeqrefs() {
    return this._seqrefs.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof AlignmentAnnotation) {

      AlignmentAnnotation temp = (AlignmentAnnotation) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._graph != temp._graph)
        return false;
      if (this._has_graph != temp._has_graph)
        return false;
      if (this._seqrefs != null) {
        if (temp._seqrefs == null)
          return false;
        if (this._seqrefs != temp._seqrefs) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._seqrefs);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._seqrefs);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._seqrefs);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._seqrefs);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._seqrefs.equals(temp._seqrefs)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._seqrefs);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._seqrefs);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._seqrefs);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._seqrefs);
          }
        }
      } else if (temp._seqrefs != null)
        return false;
      if (this._provenance != null) {
        if (temp._provenance == null)
          return false;
        if (this._provenance != temp._provenance) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._provenance);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._provenance);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._provenance);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._provenance);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._provenance.equals(temp._provenance)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._provenance);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._provenance);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._provenance);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._provenance);
          }
        }
      } else if (temp._provenance != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'graph'. The field 'graph' has the following
   * description: TODO: decide if this flag is redundant - when true it would
   * suggest that the annotationElement values together form a graph
   * 
   * @return the value of field 'Graph'.
   */
  public boolean getGraph() {
    return this._graph;
  }

  /**
   * Returns the value of field 'provenance'.
   * 
   * @return the value of field 'Provenance'.
   */
  public uk.ac.vamsas.objects.core.Provenance getProvenance() {
    return this._provenance;
  }

  /**
   * Method getSeqrefs.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the java.lang.Object at the given index
   */
  public java.lang.Object getSeqrefs(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._seqrefs.size()) {
      throw new IndexOutOfBoundsException("getSeqrefs: Index value '" + index
          + "' not in range [0.." + (this._seqrefs.size() - 1) + "]");
    }

    return _seqrefs.get(index);
  }

  /**
   * Method getSeqrefs.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public java.lang.Object[] getSeqrefs() {
    java.lang.Object[] array = new java.lang.Object[0];
    return (java.lang.Object[]) this._seqrefs.toArray(array);
  }

  /**
   * Method getSeqrefsAsReference.Returns a reference to '_seqrefs'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getSeqrefsAsReference() {
    return this._seqrefs;
  }

  /**
   * Method getSeqrefsCount.
   * 
   * @return the size of this collection
   */
  public int getSeqrefsCount() {
    return this._seqrefs.size();
  }

  /**
   * Method hasGraph.
   * 
   * @return true if at least one Graph has been added
   */
  public boolean hasGraph() {
    return this._has_graph;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    result = 37 * result + (_graph ? 0 : 1);
    if (_seqrefs != null
        && !org.castor.util.CycleBreaker.startingToCycle(_seqrefs)) {
      result = 37 * result + _seqrefs.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_seqrefs);
    }
    if (_provenance != null
        && !org.castor.util.CycleBreaker.startingToCycle(_provenance)) {
      result = 37 * result + _provenance.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_provenance);
    }

    return result;
  }

  /**
   * Returns the value of field 'graph'. The field 'graph' has the following
   * description: TODO: decide if this flag is redundant - when true it would
   * suggest that the annotationElement values together form a graph
   * 
   * @return the value of field 'Graph'.
   */
  public boolean isGraph() {
    return this._graph;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
     */
  public void removeAllSeqrefs() {
    this._seqrefs.clear();
  }

  /**
   * Method removeSeqrefs.
   * 
   * @param vSeqrefs
   * @return true if the object was removed from the collection.
   */
  public boolean removeSeqrefs(final java.lang.Object vSeqrefs) {
    boolean removed = _seqrefs.remove(vSeqrefs);
    return removed;
  }

  /**
   * Method removeSeqrefsAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public java.lang.Object removeSeqrefsAt(final int index) {
    java.lang.Object obj = this._seqrefs.remove(index);
    return obj;
  }

  /**
   * Sets the value of field 'graph'. The field 'graph' has the following
   * description: TODO: decide if this flag is redundant - when true it would
   * suggest that the annotationElement values together form a graph
   * 
   * @param graph
   *          the value of field 'graph'.
   */
  public void setGraph(final boolean graph) {
    this._graph = graph;
    this._has_graph = true;
  }

  /**
   * Sets the value of field 'provenance'.
   * 
   * @param provenance
   *          the value of field 'provenance'.
   */
  public void setProvenance(
      final uk.ac.vamsas.objects.core.Provenance provenance) {
    this._provenance = provenance;
  }

  /**
   * 
   * 
   * @param index
   * @param vSeqrefs
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setSeqrefs(final int index, final java.lang.Object vSeqrefs)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._seqrefs.size()) {
      throw new IndexOutOfBoundsException("setSeqrefs: Index value '" + index
          + "' not in range [0.." + (this._seqrefs.size() - 1) + "]");
    }

    this._seqrefs.set(index, vSeqrefs);
  }

  /**
   * 
   * 
   * @param vSeqrefsArray
   */
  public void setSeqrefs(final java.lang.Object[] vSeqrefsArray) {
    // -- copy array
    _seqrefs.clear();

    for (int i = 0; i < vSeqrefsArray.length; i++) {
      this._seqrefs.add(vSeqrefsArray[i]);
    }
  }

  /**
   * Sets the value of '_seqrefs' by copying the given Vector. All elements will
   * be checked for type safety.
   * 
   * @param vSeqrefsList
   *          the Vector to copy.
   */
  public void setSeqrefs(final java.util.Vector vSeqrefsList) {
    // copy vector
    this._seqrefs.clear();

    this._seqrefs.addAll(vSeqrefsList);
  }

  /**
   * Sets the value of '_seqrefs' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param seqrefsVector
   *          the Vector to set.
   */
  public void setSeqrefsAsReference(final java.util.Vector seqrefsVector) {
    this._seqrefs = seqrefsVector;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.RangeType
   */
  public static uk.ac.vamsas.objects.core.RangeType unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.RangeType) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.AlignmentAnnotation.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
