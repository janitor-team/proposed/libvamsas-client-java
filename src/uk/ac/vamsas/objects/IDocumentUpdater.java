/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects;

import uk.ac.vamsas.objects.core.*;

public interface IDocumentUpdater {
  public void update(Alignment vobj);

  public void update(AlignmentAnnotation vobj);

  public void update(AlignmentSequence vobj);

  public void update(AlignmentSequenceAnnotation vobj);

  public void update(AnnotationElement vobj);

  public void update(AppData vobj);

  public void update(ApplicationData vobj);

  public void update(Common vobj);

  public void update(DataSet vobj);

  public void update(DataSetAnnotations vobj);

  public void update(DbRef vobj);

  public void update(Entry vobj);

  public void update(Glyph vobj);

  public void update(Input vobj);

  public void update(Instance vobj);

  public void update(Link vobj);

  public void update(LockFile vobj);

  public void update(Map vobj);

  // TODO: replace with mapRangeType handler public void update(MapList vobj) {}

  public void update(SequenceMapping vobj);

  public void update(Newick vobj);

  public void update(Param vobj);

  public void update(Pos vobj);

  public void update(Property vobj);

  public void update(Provenance vobj);

  public void update(RangeAnnotation vobj);

  public void update(RangeType vobj);

  public void update(Score vobj);

  public void update(Seg vobj);

  public void update(Sequence vobj);

  public void update(SequenceType vobj);

  public void update(Tree vobj);

  public void update(User vobj);

  public void update(VAMSAS vobj);

  public void update(VamsasDocument vobj);
}
