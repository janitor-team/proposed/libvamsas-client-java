/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.utils.document;

import java.util.Hashtable;

/**
 * enumerates versions for the VamsasDocument.Version string provides version
 * comparison methods TODO: LATER: associate schema versions with these strings
 */
public class VersionEntries {
  public static final String ALPHA_VERSION = "alpha";

  public static final String BETA_VERSION = "beta";

  protected static Hashtable versions;
  static {
    versions = new Hashtable();
    // integers represent version hierarchy - 0 precedes 1
    versions.put(ALPHA_VERSION, new Integer(0));
    versions.put(BETA_VERSION, new Integer(1));
  }

  // TODO: LATER: decide on best pattern for enumeration classes (ie - need an
  // ordered list of versions, and validator, plus explicit enum-strings)
  public static boolean isVersion(String vstring) {
    return versions.containsKey(vstring);
  }

  /**
   * returns 0 if levels are equivalent, 1 if higher is valid and higher, 2 if
   * lower is valid and higher -1 if both levels are invalid
   * 
   * @param higher
   * @param lower
   * @return
   */
  public static int compare(String higher, String lower) {
    int v_1 = versions.containsKey(higher) ? ((Integer) versions.get(higher))
        .intValue() : -1;
    int v_2 = versions.containsKey(lower) ? ((Integer) versions.get(lower))
        .intValue() : -1;
    int comp = v_1 < v_2 ? 2 : v_1 == v_2 ? 0 : 1;
    return (comp == 0) ? (v_1 != -1 ? 0 : -1) : comp;
  }

  /**
   * @return the latest version that this vamsas library supports
   */
  public static String latestVersion() {
    return BETA_VERSION;
  }

}
