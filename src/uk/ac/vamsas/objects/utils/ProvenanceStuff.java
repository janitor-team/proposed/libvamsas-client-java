/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.utils;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.vamsas.objects.core.Entry;
import uk.ac.vamsas.objects.core.Provenance;

public class ProvenanceStuff {

  /**
   * stuff for making and doing things with provenance objects.
   */
  static Log log = LogFactory.getLog(ProvenanceStuff.class);

  /**
   * @param app
   *          TODO
   * @param action
   *          text for action entry
   * @return new Provenance entry for ArchiveWriter created docs. TODO: Verify
   *         and move to SimpleClient class for provenance handling
   */
  public static Entry newProvenanceEntry(String app, String user, String action) {
    log.debug("Adding ProvenanceEntry(" + user + "," + action + ")");
    Entry e = new Entry();
    e.setApp(app);
    e.setAction(action);
    e.setUser(user);
    e.setDate(new Date());
    return e;
  }

  public static Provenance newProvenance(Entry entry) {
    Provenance list = new Provenance();
    list.addEntry(entry);
    return list;
  }

  public static Provenance newProvenance(String user, String action) {
    return newProvenance(ProvenanceStuff.newProvenanceEntry(
        "vamsasApp:ExampleVamsasClient/alpha", user, action));
  }

  public static Provenance newProvenance(String app, String user, String action) {
    return newProvenance(ProvenanceStuff.newProvenanceEntry(app, user, action));
  }

}
