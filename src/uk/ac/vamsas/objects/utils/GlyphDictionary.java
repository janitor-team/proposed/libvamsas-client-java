/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.utils;

/**
 * dict attribute values for glyph symbol sets found in
 * uk.ac.vamsas.objects.core.AnnotationElement TODO: add validators and
 * multilength symbols.
 * 
 * @author JimP
 * 
 */
public class GlyphDictionary {
  /**
   * standard H, E, or C three state secondary structure assignment.
   */
  static final public String PROTEIN_SS_3STATE = "aasecstr_3"; // HE, blank or C

  /**
   * default glyph type attribute indicates a UTF8 character
   */
  static final public String DEFAULT = "utf8";

  /**
   * kyte and doolittle hydrophobicity TODO: specify this glyph set.
   */
  static final public String PROTEIN_HD_HYDRO = "kd_hydrophobicity";
}
