/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.utils.trees;

import uk.ac.vamsas.client.Vobject;
import uk.ac.vamsas.client.VorbaId;

public class SequenceNode extends BinaryNode {
  /** DOCUMENT ME!! */
  public float dist;

  /** DOCUMENT ME!! */
  public boolean dummy = false;

  private boolean placeholder = false;

  /**
   * Creates a new SequenceNode object.
   */
  public SequenceNode() {
    super();
  }

  /**
   * Creates a new SequenceNode object.
   * 
   * @param val
   *          DOCUMENT ME!
   * @param parent
   *          DOCUMENT ME!
   * @param dist
   *          DOCUMENT ME!
   * @param name
   *          DOCUMENT ME!
   */
  public SequenceNode(Vobject val, SequenceNode parent, float dist, String name) {
    super(val, parent, name);
    this.dist = dist;
  }

  /**
   * Creates a new SequenceNode object.
   * 
   * @param val
   *          DOCUMENT ME!
   * @param parent
   *          DOCUMENT ME!
   * @param name
   *          DOCUMENT ME!
   * @param dist
   *          DOCUMENT ME!
   * @param bootstrap
   *          DOCUMENT ME!
   * @param dummy
   *          DOCUMENT ME!
   */
  public SequenceNode(Vobject val, SequenceNode parent, String name,
      float dist, int bootstrap, boolean dummy) {
    super(val, parent, name);
    this.dist = dist;
    this.bootstrap = bootstrap;
    this.dummy = dummy;
  }

  /**
   * @param dummy
   *          true if node is created for the representation of polytomous trees
   */
  public boolean isDummy() {
    return dummy;
  }

  /*
   * @param placeholder is true if the sequence referred to in the element node
   * is not actually present in the associated alignment
   */
  public boolean isPlaceholder() {
    return placeholder;
  }

  /**
   * DOCUMENT ME!
   * 
   * @param newstate
   *          DOCUMENT ME!
   * 
   * @return DOCUMENT ME!
   */
  public boolean setDummy(boolean newstate) {
    boolean oldstate = dummy;
    dummy = newstate;

    return oldstate;
  }

  /**
   * DOCUMENT ME!
   * 
   * @param Placeholder
   *          DOCUMENT ME!
   */
  public void setPlaceholder(boolean Placeholder) {
    this.placeholder = Placeholder;
  }

  /**
   * ascends the tree but doesn't stop until a non-dummy node is discovered.
   * This will probably break if the tree is a mixture of BinaryNodes and
   * SequenceNodes.
   */
  public SequenceNode AscendTree() {
    SequenceNode c = this;

    do {
      c = (SequenceNode) c.parent();
    } while ((c != null) && c.dummy);

    return c;
  }

}
