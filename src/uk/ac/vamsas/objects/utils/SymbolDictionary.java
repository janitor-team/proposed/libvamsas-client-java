/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.utils;

public class SymbolDictionary {
  /**
   * defines standard names and properties for vamsas sequence dictionaries
   */
  static final public String STANDARD_AA = "info:iubmb.org/aminoacids"; // strict
                                                                        // 1
                                                                        // letter
                                                                        // code

  static final public String STANDARD_NA = "info:iubmb.org/nucleosides";// strict
                                                                        // 1
                                                                        // letter
                                                                        // code
                                                                        // (do
                                                                        // not
                                                                        // allow
                                                                        // arbitrary
                                                                        // rare
                                                                        // nucleosides)
  /**
   * TODO: Vamsas Dictionary properties interface an interface for dictionary
   * provides : validation for a string symbolwidth (or symbol next/previous)
   * mappings to certain other dictionaries (one2three, etc) gap-character test
   * 
   */

}
