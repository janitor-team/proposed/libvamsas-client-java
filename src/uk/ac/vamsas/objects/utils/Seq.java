/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.utils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.regex.Pattern;

import uk.ac.vamsas.objects.core.AlignmentSequence;
import uk.ac.vamsas.objects.core.Sequence;
import uk.ac.vamsas.objects.core.SequenceType;

/**
 * @author jimp
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class Seq {

  public static void write_PirSeq(OutputStream os, SequenceType seq, int wid)
      throws IOException {
    BufferedWriter pir_out = new BufferedWriter(new OutputStreamWriter(os));
    pir_out.write(">P1;" + seq.getName() + "\n");
    int width = (wid < 1) ? 80 : wid;
    for (int j = 0, k = seq.getSequence().length(); j < k; j += width)
      if (j + width < k)
        pir_out.write(seq.getSequence().substring(j, j + width) + "\n");
      else
        pir_out.write(seq.getSequence().substring(j) + "\n");
    pir_out.flush();
  }

  public static void write_FastaSeq(OutputStream os, SequenceType seq)
      throws IOException {
    BufferedWriter fasta_out = new BufferedWriter(new OutputStreamWriter(os));
    fasta_out.write(">" + seq.getName() + "\n");
    fasta_out.write(seq.getSequence() + "\n");
    fasta_out.flush();
  }

  public static void write_FastaSeq(OutputStream os, SequenceType seq, int wid)
      throws IOException {
    BufferedWriter fasta_out = new BufferedWriter(new OutputStreamWriter(os));
    fasta_out.write(">" + seq.getName() + "\n");
    int width = (wid < 1) ? 80 : wid;
    for (int j = 0, k = seq.getSequence().length(); j < k; j += width)
      if (j + width < k)
        fasta_out.write(seq.getSequence().substring(j, j + width) + "\n");
      else
        fasta_out.write(seq.getSequence().substring(j) + "\n");
    fasta_out.flush();
  }

  /**
   *validate a SequenceType Vobject as an info:iubmb.org/aminoacid SequenceType
   * This version resolves references to Sequence objects from AlignmentSequence
   * TODO: Define info: urn for dictionary string (could also be regex of valid
   * characters!)
   * 
   * @param s
   * @param dict
   *          TODO
   * @return true if a valid amino acid sequence Vobject
   */
  private static boolean valid_aadictionary_string(String s, String dict) {
    if (s == null)
      return false;
    // validate against dictionary
    // TODO generalise to resolve dictionary against info: urn for dictionary
    // type
    Pattern aa_repl = Pattern.compile("[ARNDCQEGHILKMFPSTWYVUX]+",
        Pattern.CASE_INSENSITIVE);
    String remnants = aa_repl.matcher(s).replaceAll("");
    return !remnants.matches("//S+");
  }

  public static Sequence newSequence(String Name, String Sequence,
      String Dictionary, int start, int end) {
    // TODO: make hierarchy reflecting the SeqType Vobject.
    Sequence seq = new Sequence();
    seq.setDictionary(Dictionary);
    seq.setName(Name);
    seq.setSequence(Sequence);
    seq.setStart(start);
    if (start <= end) {
      if ((end - start) != Sequence.length())
        seq.setEnd(start + Sequence.length());
    } else {
      // reverse topology mapping. TODO: VAMSAS: decide if allowed to do
      // start>end on Sequence Vobject
      if ((start - end) != Sequence.length())
        seq.setEnd(end + Sequence.length());
    }
    return seq;
  }

  public static AlignmentSequence newAlignmentSequence(String name,
      String alSequence, Sequence refseq, long start, long end) {
    if (refseq != null) {
      AlignmentSequence asq = new AlignmentSequence();
      asq.setName(name);
      asq.setSequence(alSequence);
      asq.setRefid(refseq);
      if (end > refseq.getEnd() || end < start || end == -1)
        end = refseq.getEnd();
      asq.setEnd(end);
      if (start < refseq.getStart())
        start = refseq.getStart();
      asq.setStart(start);
      return asq;
    }
    return null;
  }

  public static boolean is_valid_aa_seq(SequenceType s) {
    Sequence q;
    boolean validref = false;
    if (s instanceof Sequence) {
      q = (Sequence) s;
      if (q.getDictionary() != null

      && q.getDictionary().length() > 0
          || !q.getDictionary().equals(SymbolDictionary.STANDARD_AA))
        return false;
      return valid_aadictionary_string(q.getSequence(),
          SymbolDictionary.STANDARD_AA);
    }

    // follow references
    if (s instanceof AlignmentSequence) {
      Object w = (((AlignmentSequence) s).getRefid());
      if (w != null && w != s && w instanceof SequenceType)
        return is_valid_aa_seq((SequenceType) w)
            && valid_aadictionary_string(((AlignmentSequence) s).getSequence(),
                SymbolDictionary.STANDARD_AA);
    }

    return false;
  }
}
