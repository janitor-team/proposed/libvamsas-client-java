/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.utils;

import uk.ac.vamsas.objects.core.*;
import uk.ac.vamsas.objects.utils.document.VersionEntries;

/**
 * various vamsas-client independent helpers for creating and manipulating the
 * vamsasDocument Vobject
 * 
 * @author jimp
 * 
 */
public class DocumentStuff {
  public static VamsasDocument newVamsasDocument(VAMSAS root[], String version) {
    return newVamsasDocument(root, ProvenanceStuff.newProvenance(
        "AUTO:org.vamsas.DocumentStuff.newVamsasDocument",
        "Vamsas Document created"), version);
  }

  public static VamsasDocument newVamsasDocument(VAMSAS root[]) {
    return newVamsasDocument(root, ProvenanceStuff.newProvenance(
        "AUTO:org.vamsas.DocumentStuff.newVamsasDocument",
        "Vamsas Document created"), VersionEntries.latestVersion());
  }

  public static VamsasDocument newVamsasDocument(VAMSAS root[], Provenance p,
      String version) {
    VamsasDocument doc = new VamsasDocument();
    for (int r = 0; r < root.length; r++) {
      doc.addVAMSAS(root[r]);
    }
    doc.setProvenance(p);
    doc.setVersion(version);
    return doc;
  }
}
