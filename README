The VAMSAS Client
=================

This is version 0.2 of the LGPLed Java implementation of the 
VAMSAS framework for bioinformatics application data exchange interoperation. 
VAMSAS is a pseudo-acronym, and is short for 'Visualization and Analysis 
of Molecular Sequences, Alignments and Structures'. 
The client is the product of an eScience research project funded by the
UK Biotechnology and Biological Sciences Research Council which
aimed to enable interoperation between visualization and analysis tools
that deal with DNA, protein sequences, alignments, and molecular structures.

The framework model is language independent - but this 
java prototype was developed for incorporation in the 
three sister programs (Jalview, TOPALi and AstexViewer@EBI-MSD)
involved in the vamsas project (http://www.vamsas.ac.uk).

Directory structure and Build Notes
-----------------------------------
Java source lives under src, runtime dependencies under lib and resources,
compile time dependencies (which need to be added to the ant build classpath) 
reside in tools, and license details for dependencies are described in CONTRIB.
Ant 1.7 has been verified to work with the provided build.xml.

API and schema documentation can be found under the docs directory, 
along with a variety of (not entirely up to date) notes made during 
the development of the code. The schemas directory contains the vamsas document
XML model, plus a couple of legacy XML documents. The demo directory also contains 
some older demonstration data. Finally, the test directory is made conspicuous via
its emptiness (sorry!). 

A documented demonstration client (uk.ac.vamsas.test.ExampleApplication) based
on the Jalview/VAMSAS control logic shows how the VAMSAS client code is intended 
to be used, but it does not yet demonstrate all aspects of the interchange model.
   
For more information, please contact me, or any of the other developers 
directly, or via their program's own email discussion list.

Changes since version 0.1
-------------------------
* Session Name parameter to decouple vamsas document source from session URN
* VAMSAS data selection event 
* 1.5 and 1.4 build options
* SAXTreeViewer
* minor bug and information message fixes.
* legacy import of vamsas 0.1 sessions

The SAXTreeViewer was added to the uk.ac.vamsas.test.document package - it is
a simple debug tool which will monitor a specific VAMSAS document archive, and
display its contents as a tree which is updated after any modifications by other
VAMSAS clients.  This class relies on the java 1.5 runtime, so a build1.4 


Jim Procter.
18th February 2010.

The other VAMSAS coders: Dominik Lindner, Pierre Marguerite, Iain Milne, Andrew Waterhouse.
The VAMSAS project researchers: Geoff Barton, David Martin, David Marshall, Tom Oldfield, Frank Wright.
